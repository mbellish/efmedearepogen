﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EFMedeaRepoGen
{
    partial class RepoTemplate
    {
        private Type genType;
        private Type itype;
        public RepoTemplate(Type typeToGen, Type itype)
        {
            this.genType = typeToGen;
            this.itype = itype;
        }

        public static string PascalCase(string toCase)
        {
            string toReturn = "";
            string[] split = toCase.Split('_');
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i] == "t")
                    continue;
                if (split[i].Length > 1)
                    toReturn += split[i].Substring(0, 1).ToUpper() + split[i].Substring(1, split[i].Length - 1);
                else
                    toReturn += split[i].ToUpper();
            }
            return toReturn;
        }

        public static string PrettyCase(string toCase)
        {
            string toReturn = "";
            string[] split = toCase.Split('_');
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i] == "t")
                    continue;
                if (split[i].Length > 1)
                    toReturn += split[i].Substring(0, 1).ToUpper() + split[i].Substring(1, split[i].Length - 1);
                else
                    toReturn += split[i].ToUpper();

                toReturn += " ";
            }
            return toReturn.TrimEnd(' ');
        }

        public static string CamelCase(string toCase)
        {
            string toReturn = "";
            string[] split = toCase.Split('_');
            toReturn = split[0];

            for (int i = 1; i < split.Length; i++)
            {
                if (split[i] == "t")
                    continue;
                if (split[i].Length > 1)
                    toReturn += split[i].Substring(0, 1).ToUpper() + split[i].Substring(1, split[i].Length - 1);
                else
                    toReturn += split[i].ToUpper();

                toReturn += " ";
            }
            return toReturn.TrimEnd(' ');
        }

        private List<String> GetKeys()
        {
            List<String> toReturn = new List<string>();

            toReturn.AddRange(genType.GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), true).Length > 0).Select(n => n.Name));
            
            return toReturn;
        }

        private string GetParamKeys()
        {
            
            string toReturn = string.Join(',',(genType.GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), true).Length > 0).Select(n => n.PropertyType.Name + " " + PascalCase(n.Name))));

            
            return toReturn.TrimEnd('\'');
        }

        private String GetKeyToPascalKeyEqual(string secondary)
        {
            if (GetKeys().Count() == 0)
                return "";
            string toReturn = "";
            foreach(var key in GetKeys())
            {
                toReturn += "p." + key + " == " + secondary + PascalCase(key) + " && ";
            }
            return toReturn.Substring(0, toReturn.Length - 4);
        }
        private string GetPascalKeyList(string seperator)
        {
            return string.Join(seperator, GetKeys().Select(p => PascalCase(p)));
        }

        private List<string> GetAdditionalMethods()
        {
            List<string> toReturn = new List<string>();
            var keys = GetPascalKeyList(string.Empty);

            foreach(var method in itype.GetMethods())
            {
                if (method.Name == "GetAll" || method.Name == "Insert" || method.Name =="Delete" || method.Name == "Update" || method.Name == "GetBy"+keys)
                    continue;
                string methodSignature = String.Format("public {0} {1}(", method.ReturnType.ToGenericTypeString(), method.Name);
                foreach(var param in method.GetParameters())
                {
                    methodSignature += param.ParameterType.ToGenericTypeString() + " " + param.Name + ", ";
                }
                methodSignature = methodSignature.TrimEnd(',', ' ');
                methodSignature += ") {";
                toReturn.Add(methodSignature);
            }
            
            return toReturn;
        }
    }
}
