
// -----------------------------------------------------------------------
// <copyright file="InvoiceErrorRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Error repository.
    /// </summary>
    public class InvoiceErrorRepository : IInvoiceErrorRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceErrorRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceErrorRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceErrorModel> GetAll()
        {
            return this.context.invoice_error_t.Select(Mapper.Map<InvoiceErrorModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceError by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Invoice Error.</returns>
        public InvoiceErrorModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<InvoiceErrorModel>(this.context.invoice_error_t.Where(p => p.row_nb == RowNb));
        }
       	    public List<InvoiceErrorModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceErrorModel> GetByInvoiceNbInvForeFl(String invoiceNb, String invForeFl) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceErrorModel model)
        {
            this.context.invoice_error_t.Remove(Mapper.Map<invoice_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceErrorModel model)
        {
            var record = this.context.invoice_error_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<InvoiceErrorModel, invoice_error_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceErrorModel model)
        {
            this.context.invoice_error_t.Add(Mapper.Map<invoice_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}