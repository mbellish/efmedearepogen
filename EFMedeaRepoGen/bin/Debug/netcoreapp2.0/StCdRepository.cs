
// -----------------------------------------------------------------------
// <copyright file="StCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The St Cd repository.
    /// </summary>
    public class StCdRepository : IStCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="StCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public StCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<StCdModel> GetAll()
        {
            return this.context.st_cd_t.Select(Mapper.Map<StCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a StCd by StCd
        /// </summary>
		/// <param name="st_cd">The st_cd of the entity.</param>        
        /// <returns>A St Cd.</returns>
        public StCdModel GetByStCd(String st_cd)
        {
            return Mapper.Map<StCdModel>(this.context.st_cd_t.Where(p => p.st_cd == StCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(StCdModel model)
        {
            this.context.st_cd_t.Remove(Mapper.Map<st_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(StCdModel model)
        {
            var record = this.context.st_cd_t.Where(p => p.st_cd == model.StCd).FirstOrDefault();
            Mapper.Map<StCdModel, st_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(StCdModel model)
        {
            this.context.st_cd_t.Add(Mapper.Map<st_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}