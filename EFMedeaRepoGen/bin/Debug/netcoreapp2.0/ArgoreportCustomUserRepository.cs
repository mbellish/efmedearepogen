
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomUserRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom User repository.
    /// </summary>
    public class ArgoreportCustomUserRepository : IArgoreportCustomUserRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomUserRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomUserRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomUserModel> GetAll()
        {
            return this.context.argoreport_custom_user_t.Select(Mapper.Map<ArgoreportCustomUserModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomUser by ArgoreportCustomId UserId
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="user_id">The user_id of the entity.</param>        
        /// <returns>A Argoreport Custom User.</returns>
        public ArgoreportCustomUserModel GetByArgoreportCustomIdUserId(String argoreport_custom_id,String user_id)
        {
            return Mapper.Map<ArgoreportCustomUserModel>(this.context.argoreport_custom_user_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.user_id == UserId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomUserModel model)
        {
            this.context.argoreport_custom_user_t.Remove(Mapper.Map<argoreport_custom_user_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomUserModel model)
        {
            var record = this.context.argoreport_custom_user_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.user_id == model.UserId).FirstOrDefault();
            Mapper.Map<ArgoreportCustomUserModel, argoreport_custom_user_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomUserModel model)
        {
            this.context.argoreport_custom_user_t.Add(Mapper.Map<argoreport_custom_user_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}