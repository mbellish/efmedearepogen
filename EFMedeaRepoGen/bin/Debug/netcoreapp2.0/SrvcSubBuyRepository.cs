
// -----------------------------------------------------------------------
// <copyright file="SrvcSubBuyRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Srvc Sub Buy repository.
    /// </summary>
    public class SrvcSubBuyRepository : ISrvcSubBuyRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SrvcSubBuyRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SrvcSubBuyRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SrvcSubBuyModel> GetAll()
        {
            return this.context.srvc_sub_buy_t.Select(Mapper.Map<SrvcSubBuyModel>).ToList();
        }

		        /// <summary>
        /// Gets a SrvcSubBuy by ProgSrvcNm SysId SubDt SubCd ALaCarteRetailRate
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="a_la_carte_retail_rate">The a_la_carte_retail_rate of the entity.</param>        
        /// <returns>A Srvc Sub Buy.</returns>
        public SrvcSubBuyModel GetByProgSrvcNmSysIdSubDtSubCdALaCarteRetailRate(String prog_srvc_nm,String sys_id,DateTime sub_dt,String sub_cd,Decimal a_la_carte_retail_rate)
        {
            return Mapper.Map<SrvcSubBuyModel>(this.context.srvc_sub_buy_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.sys_id == SysId && p.sub_dt == SubDt && p.sub_cd == SubCd && p.a_la_carte_retail_rate == ALaCarteRetailRate));
        }
       	    public List<SrvcSubBuyModel> GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SrvcSubBuyModel model)
        {
            this.context.srvc_sub_buy_t.Remove(Mapper.Map<srvc_sub_buy_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SrvcSubBuyModel model)
        {
            var record = this.context.srvc_sub_buy_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.sys_id == model.SysId && p.sub_dt == model.SubDt && p.sub_cd == model.SubCd && p.a_la_carte_retail_rate == model.ALaCarteRetailRate).FirstOrDefault();
            Mapper.Map<SrvcSubBuyModel, srvc_sub_buy_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SrvcSubBuyModel model)
        {
            this.context.srvc_sub_buy_t.Add(Mapper.Map<srvc_sub_buy_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}