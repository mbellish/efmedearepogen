
// -----------------------------------------------------------------------
// <copyright file="ContractRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract repository.
    /// </summary>
    public class ContractRepository : IContractRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractModel> GetAll()
        {
            return this.context.contract_t.Select(Mapper.Map<ContractModel>).ToList();
        }

		        /// <summary>
        /// Gets a Contract by ContractNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>        
        /// <returns>A Contract.</returns>
        public ContractModel GetByContractNb(String contract_nb)
        {
            return Mapper.Map<ContractModel>(this.context.contract_t.Where(p => p.contract_nb == ContractNb));
        }
       	    public List<ContractModel> GetByParentContractNb(String parentContractNb) {
			throw new NotImplementedException();
		}
		public List<ContractModel> GetActiveParentContractsWithActiveBillingParty() {
			throw new NotImplementedException();
		}
		public List<ContractModel> GetContractsWithoutEmailContact() {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractModel model)
        {
            this.context.contract_t.Remove(Mapper.Map<contract_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractModel model)
        {
            var record = this.context.contract_t.Where(p => p.contract_nb == model.ContractNb).FirstOrDefault();
            Mapper.Map<ContractModel, contract_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractModel model)
        {
            this.context.contract_t.Add(Mapper.Map<contract_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}