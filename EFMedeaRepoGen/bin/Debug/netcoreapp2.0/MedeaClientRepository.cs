
// -----------------------------------------------------------------------
// <copyright file="MedeaClientRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Medea Client repository.
    /// </summary>
    public class MedeaClientRepository : IMedeaClientRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MedeaClientRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MedeaClientRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MedeaClientModel> GetAll()
        {
            return this.context.medea_client_t.Select(Mapper.Map<MedeaClientModel>).ToList();
        }

		        /// <summary>
        /// Gets a MedeaClient by ClientCd
        /// </summary>
		/// <param name="client_cd">The client_cd of the entity.</param>        
        /// <returns>A Medea Client.</returns>
        public MedeaClientModel GetByClientCd(String client_cd)
        {
            return Mapper.Map<MedeaClientModel>(this.context.medea_client_t.Where(p => p.client_cd == ClientCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MedeaClientModel model)
        {
            this.context.medea_client_t.Remove(Mapper.Map<medea_client_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MedeaClientModel model)
        {
            var record = this.context.medea_client_t.Where(p => p.client_cd == model.ClientCd).FirstOrDefault();
            Mapper.Map<MedeaClientModel, medea_client_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MedeaClientModel model)
        {
            this.context.medea_client_t.Add(Mapper.Map<medea_client_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}