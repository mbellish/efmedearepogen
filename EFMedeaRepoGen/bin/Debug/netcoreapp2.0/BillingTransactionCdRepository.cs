
// -----------------------------------------------------------------------
// <copyright file="BillingTransactionCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Billing Transaction Cd repository.
    /// </summary>
    public class BillingTransactionCdRepository : IBillingTransactionCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillingTransactionCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BillingTransactionCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BillingTransactionCdModel> GetAll()
        {
            return this.context.billing_transaction_cd_t.Select(Mapper.Map<BillingTransactionCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a BillingTransactionCd by BillingTransactionCd
        /// </summary>
		/// <param name="billing_transaction_cd">The billing_transaction_cd of the entity.</param>        
        /// <returns>A Billing Transaction Cd.</returns>
        public BillingTransactionCdModel GetByBillingTransactionCd(String billing_transaction_cd)
        {
            return Mapper.Map<BillingTransactionCdModel>(this.context.billing_transaction_cd_t.Where(p => p.billing_transaction_cd == BillingTransactionCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BillingTransactionCdModel model)
        {
            this.context.billing_transaction_cd_t.Remove(Mapper.Map<billing_transaction_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BillingTransactionCdModel model)
        {
            var record = this.context.billing_transaction_cd_t.Where(p => p.billing_transaction_cd == model.BillingTransactionCd).FirstOrDefault();
            Mapper.Map<BillingTransactionCdModel, billing_transaction_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BillingTransactionCdModel model)
        {
            this.context.billing_transaction_cd_t.Add(Mapper.Map<billing_transaction_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}