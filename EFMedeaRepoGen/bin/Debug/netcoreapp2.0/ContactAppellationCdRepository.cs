
// -----------------------------------------------------------------------
// <copyright file="ContactAppellationCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact Appellation Cd repository.
    /// </summary>
    public class ContactAppellationCdRepository : IContactAppellationCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactAppellationCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactAppellationCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactAppellationCdModel> GetAll()
        {
            return this.context.contact_appellation_cd_t.Select(Mapper.Map<ContactAppellationCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactAppellationCd by AppellationCd
        /// </summary>
		/// <param name="appellation_cd">The appellation_cd of the entity.</param>        
        /// <returns>A Contact Appellation Cd.</returns>
        public ContactAppellationCdModel GetByAppellationCd(String appellation_cd)
        {
            return Mapper.Map<ContactAppellationCdModel>(this.context.contact_appellation_cd_t.Where(p => p.appellation_cd == AppellationCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactAppellationCdModel model)
        {
            this.context.contact_appellation_cd_t.Remove(Mapper.Map<contact_appellation_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactAppellationCdModel model)
        {
            var record = this.context.contact_appellation_cd_t.Where(p => p.appellation_cd == model.AppellationCd).FirstOrDefault();
            Mapper.Map<ContactAppellationCdModel, contact_appellation_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactAppellationCdModel model)
        {
            this.context.contact_appellation_cd_t.Add(Mapper.Map<contact_appellation_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}