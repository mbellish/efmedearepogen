
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Tax repository.
    /// </summary>
    public class PreviewInvoiceTaxRepository : IPreviewInvoiceTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceTaxModel> GetAll()
        {
            return this.context.preview_invoice_tax_t.Select(Mapper.Map<PreviewInvoiceTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceTax by InvoiceNb InvoiceRecNb SysInvoiceRecNb TaxCd
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>/// <param name="sys_invoice_rec_nb">The sys_invoice_rec_nb of the entity.</param>/// <param name="tax_cd">The tax_cd of the entity.</param>        
        /// <returns>A Preview Invoice Tax.</returns>
        public PreviewInvoiceTaxModel GetByInvoiceNbInvoiceRecNbSysInvoiceRecNbTaxCd(Int32 invoice_nb,String invoice_rec_nb,String sys_invoice_rec_nb,String tax_cd)
        {
            return Mapper.Map<PreviewInvoiceTaxModel>(this.context.preview_invoice_tax_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb && p.sys_invoice_rec_nb == SysInvoiceRecNb && p.tax_cd == TaxCd));
        }
       	    public List<PreviewInvoiceTaxModel> GetByInvoiceNb(Int32 invoiceNb) {
			throw new NotImplementedException();
		}
		public List<PreviewInvoiceTaxModel> GetByInvoiceNbInvoiceRecNb(Int32 invoiceNb, String invoiceRecNb) {
			throw new NotImplementedException();
		}
		public List<PreviewInvoiceTaxModel> GetByInvoiceNbSysInvoiceRecNb(Int32 invoiceNb, String sysInvoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceTaxModel model)
        {
            this.context.preview_invoice_tax_t.Remove(Mapper.Map<preview_invoice_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceTaxModel model)
        {
            var record = this.context.preview_invoice_tax_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb && p.sys_invoice_rec_nb == model.SysInvoiceRecNb && p.tax_cd == model.TaxCd).FirstOrDefault();
            Mapper.Map<PreviewInvoiceTaxModel, preview_invoice_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceTaxModel model)
        {
            this.context.preview_invoice_tax_t.Add(Mapper.Map<preview_invoice_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}