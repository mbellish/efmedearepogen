
// -----------------------------------------------------------------------
// <copyright file="BasicRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Basic Rate repository.
    /// </summary>
    public class BasicRateRepository : IBasicRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BasicRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BasicRateModel> GetAll()
        {
            return this.context.basic_rate_t.Select(Mapper.Map<BasicRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a BasicRate by StartDt SubCd PenetratePercent ContractNb CarriageId
        /// </summary>
		/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="penetrate_percent">The penetrate_percent of the entity.</param>/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>        
        /// <returns>A Basic Rate.</returns>
        public BasicRateModel GetByStartDtSubCdPenetratePercentContractNbCarriageId(DateTime start_dt,String sub_cd,Double penetrate_percent,String contract_nb,String carriage_id)
        {
            return Mapper.Map<BasicRateModel>(this.context.basic_rate_t.Where(p => p.start_dt == StartDt && p.sub_cd == SubCd && p.penetrate_percent == PenetratePercent && p.contract_nb == ContractNb && p.carriage_id == CarriageId));
        }
       	    public List<BasicRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<BasicRateModel> GetByContractNbCarriageId(String contractNb, String carriageID) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BasicRateModel model)
        {
            this.context.basic_rate_t.Remove(Mapper.Map<basic_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BasicRateModel model)
        {
            var record = this.context.basic_rate_t.Where(p => p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.penetrate_percent == model.PenetratePercent && p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId).FirstOrDefault();
            Mapper.Map<BasicRateModel, basic_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BasicRateModel model)
        {
            this.context.basic_rate_t.Add(Mapper.Map<basic_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}