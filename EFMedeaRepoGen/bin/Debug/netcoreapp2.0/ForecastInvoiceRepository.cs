
// -----------------------------------------------------------------------
// <copyright file="ForecastInvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Invoice repository.
    /// </summary>
    public class ForecastInvoiceRepository : IForecastInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastInvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastInvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastInvoiceModel> GetAll()
        {
            return this.context.forecast_invoice_t.Select(Mapper.Map<ForecastInvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastInvoice by InvoiceNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Forecast Invoice.</returns>
        public ForecastInvoiceModel GetByInvoiceNb(String invoice_nb)
        {
            return Mapper.Map<ForecastInvoiceModel>(this.context.forecast_invoice_t.Where(p => p.invoice_nb == InvoiceNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastInvoiceModel model)
        {
            this.context.forecast_invoice_t.Remove(Mapper.Map<forecast_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastInvoiceModel model)
        {
            var record = this.context.forecast_invoice_t.Where(p => p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<ForecastInvoiceModel, forecast_invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastInvoiceModel model)
        {
            this.context.forecast_invoice_t.Add(Mapper.Map<forecast_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}