
// -----------------------------------------------------------------------
// <copyright file="MsoRegionCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Region Comment repository.
    /// </summary>
    public class MsoRegionCommentRepository : IMsoRegionCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoRegionCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoRegionCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoRegionCommentModel> GetAll()
        {
            return this.context.mso_region_comment_t.Select(Mapper.Map<MsoRegionCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoRegionComment by CommentNb MsoNb MsoRegionNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="mso_region_nb">The mso_region_nb of the entity.</param>        
        /// <returns>A Mso Region Comment.</returns>
        public MsoRegionCommentModel GetByCommentNbMsoNbMsoRegionNb(String comment_nb,String mso_nb,String mso_region_nb)
        {
            return Mapper.Map<MsoRegionCommentModel>(this.context.mso_region_comment_t.Where(p => p.comment_nb == CommentNb && p.mso_nb == MsoNb && p.mso_region_nb == MsoRegionNb));
        }
       	    public List<MsoRegionCommentModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoRegionCommentModel> GetByMsoNbMsoRegionNb(String msoNb, String msoRegionNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoRegionCommentModel model)
        {
            this.context.mso_region_comment_t.Remove(Mapper.Map<mso_region_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoRegionCommentModel model)
        {
            var record = this.context.mso_region_comment_t.Where(p => p.comment_nb == model.CommentNb && p.mso_nb == model.MsoNb && p.mso_region_nb == model.MsoRegionNb).FirstOrDefault();
            Mapper.Map<MsoRegionCommentModel, mso_region_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoRegionCommentModel model)
        {
            this.context.mso_region_comment_t.Add(Mapper.Map<mso_region_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}