
// -----------------------------------------------------------------------
// <copyright file="SysHeadendSrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Headend Srvc repository.
    /// </summary>
    public class SysHeadendSrvcRepository : ISysHeadendSrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysHeadendSrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysHeadendSrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysHeadendSrvcModel> GetAll()
        {
            return this.context.sys_headend_srvc_t.Select(Mapper.Map<SysHeadendSrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysHeadendSrvc by SysId ReceiverId
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="receiver_id">The receiver_id of the entity.</param>        
        /// <returns>A Sys Headend Srvc.</returns>
        public SysHeadendSrvcModel GetBySysIdReceiverId(String sys_id,Int32 receiver_id)
        {
            return Mapper.Map<SysHeadendSrvcModel>(this.context.sys_headend_srvc_t.Where(p => p.sys_id == SysId && p.receiver_id == ReceiverId));
        }
       	    public List<SysHeadendSrvcModel> GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysHeadendSrvcModel model)
        {
            this.context.sys_headend_srvc_t.Remove(Mapper.Map<sys_headend_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysHeadendSrvcModel model)
        {
            var record = this.context.sys_headend_srvc_t.Where(p => p.sys_id == model.SysId && p.receiver_id == model.ReceiverId).FirstOrDefault();
            Mapper.Map<SysHeadendSrvcModel, sys_headend_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysHeadendSrvcModel model)
        {
            this.context.sys_headend_srvc_t.Add(Mapper.Map<sys_headend_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}