
// -----------------------------------------------------------------------
// <copyright file="MsoClusterRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster repository.
    /// </summary>
    public class MsoClusterRepository : IMsoClusterRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterModel> GetAll()
        {
            return this.context.mso_cluster_t.Select(Mapper.Map<MsoClusterModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoCluster by MsoNb ClusterNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>        
        /// <returns>A Mso Cluster.</returns>
        public MsoClusterModel GetByMsoNbClusterNb(String mso_nb,String cluster_nb)
        {
            return Mapper.Map<MsoClusterModel>(this.context.mso_cluster_t.Where(p => p.mso_nb == MsoNb && p.cluster_nb == ClusterNb));
        }
       	    public List<MsoClusterModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByDelivCntryCd(String delivCntryCd) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByDelivSt(String delivSt) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByMsoNbDistrictNb(String msoNb, String districtNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByMsoNbMsoDivisionNb(String msoNb, String msoDivisionNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetByMsoNbMsoRegionNb(String msoNb, String msoRegionNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterModel> GetBySt(String st) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterModel model)
        {
            this.context.mso_cluster_t.Remove(Mapper.Map<mso_cluster_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterModel model)
        {
            var record = this.context.mso_cluster_t.Where(p => p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb).FirstOrDefault();
            Mapper.Map<MsoClusterModel, mso_cluster_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterModel model)
        {
            this.context.mso_cluster_t.Add(Mapper.Map<mso_cluster_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}