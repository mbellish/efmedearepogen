
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogRevenueAccrueLatestInvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log Revenue Accrue Latest Invoice repository.
    /// </summary>
    public class InvoiceRecLogRevenueAccrueLatestInvoiceRepository : IInvoiceRecLogRevenueAccrueLatestInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogRevenueAccrueLatestInvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogRevenueAccrueLatestInvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogRevenueAccrueLatestInvoiceModel> GetAll()
        {
            return this.context.invoice_rec_log_revenue_accrue_latest_invoice_t.Select(Mapper.Map<InvoiceRecLogRevenueAccrueLatestInvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLogRevenueAccrueLatestInvoice by InvoiceRecLogId InvoiceNb
        /// </summary>
		/// <param name="invoice_rec_log_id">The invoice_rec_log_id of the entity.</param>/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Invoice Rec Log Revenue Accrue Latest Invoice.</returns>
        public InvoiceRecLogRevenueAccrueLatestInvoiceModel GetByInvoiceRecLogIdInvoiceNb(Int32 invoice_rec_log_id,String invoice_nb)
        {
            return Mapper.Map<InvoiceRecLogRevenueAccrueLatestInvoiceModel>(this.context.invoice_rec_log_revenue_accrue_latest_invoice_t.Where(p => p.invoice_rec_log_id == InvoiceRecLogId && p.invoice_nb == InvoiceNb));
        }
       	    public List<InvoiceRecLogRevenueAccrueLatestInvoiceModel> GetByInvoiceRecLogId(Int32 invoiceRecLogId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogRevenueAccrueLatestInvoiceModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_latest_invoice_t.Remove(Mapper.Map<invoice_rec_log_revenue_accrue_latest_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogRevenueAccrueLatestInvoiceModel model)
        {
            var record = this.context.invoice_rec_log_revenue_accrue_latest_invoice_t.Where(p => p.invoice_rec_log_id == model.InvoiceRecLogId && p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<InvoiceRecLogRevenueAccrueLatestInvoiceModel, invoice_rec_log_revenue_accrue_latest_invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogRevenueAccrueLatestInvoiceModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_latest_invoice_t.Add(Mapper.Map<invoice_rec_log_revenue_accrue_latest_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}