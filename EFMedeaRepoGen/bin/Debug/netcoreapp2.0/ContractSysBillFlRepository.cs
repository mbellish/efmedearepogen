
// -----------------------------------------------------------------------
// <copyright file="ContractSysBillFlRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Sys Bill Fl repository.
    /// </summary>
    public class ContractSysBillFlRepository : IContractSysBillFlRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractSysBillFlRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractSysBillFlRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractSysBillFlModel> GetAll()
        {
            return this.context.contract_sys_bill_fl_t.Select(Mapper.Map<ContractSysBillFlModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractSysBillFl by ContractNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>        
        /// <returns>A Contract Sys Bill Fl.</returns>
        public ContractSysBillFlModel GetByContractNb(String contract_nb)
        {
            return Mapper.Map<ContractSysBillFlModel>(this.context.contract_sys_bill_fl_t.Where(p => p.contract_nb == ContractNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractSysBillFlModel model)
        {
            this.context.contract_sys_bill_fl_t.Remove(Mapper.Map<contract_sys_bill_fl_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractSysBillFlModel model)
        {
            var record = this.context.contract_sys_bill_fl_t.Where(p => p.contract_nb == model.ContractNb).FirstOrDefault();
            Mapper.Map<ContractSysBillFlModel, contract_sys_bill_fl_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractSysBillFlModel model)
        {
            this.context.contract_sys_bill_fl_t.Add(Mapper.Map<contract_sys_bill_fl_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}