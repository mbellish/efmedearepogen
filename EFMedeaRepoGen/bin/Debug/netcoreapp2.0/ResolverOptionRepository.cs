
// -----------------------------------------------------------------------
// <copyright file="ResolverOptionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Resolver Option repository.
    /// </summary>
    public class ResolverOptionRepository : IResolverOptionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverOptionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ResolverOptionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ResolverOptionModel> GetAll()
        {
            return this.context.resolver_option_t.Select(Mapper.Map<ResolverOptionModel>).ToList();
        }

		        /// <summary>
        /// Gets a ResolverOption by ResolverOption ErrorNb
        /// </summary>
		/// <param name="resolver_option">The resolver_option of the entity.</param>/// <param name="error_nb">The error_nb of the entity.</param>        
        /// <returns>A Resolver Option.</returns>
        public ResolverOptionModel GetByResolverOptionErrorNb(String resolver_option,Int32 error_nb)
        {
            return Mapper.Map<ResolverOptionModel>(this.context.resolver_option_t.Where(p => p.resolver_option == ResolverOption && p.error_nb == ErrorNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ResolverOptionModel model)
        {
            this.context.resolver_option_t.Remove(Mapper.Map<resolver_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ResolverOptionModel model)
        {
            var record = this.context.resolver_option_t.Where(p => p.resolver_option == model.ResolverOption && p.error_nb == model.ErrorNb).FirstOrDefault();
            Mapper.Map<ResolverOptionModel, resolver_option_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ResolverOptionModel model)
        {
            this.context.resolver_option_t.Add(Mapper.Map<resolver_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}