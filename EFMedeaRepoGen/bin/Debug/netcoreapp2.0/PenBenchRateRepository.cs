
// -----------------------------------------------------------------------
// <copyright file="PenBenchRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Pen Bench Rate repository.
    /// </summary>
    public class PenBenchRateRepository : IPenBenchRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PenBenchRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PenBenchRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PenBenchRateModel> GetAll()
        {
            return this.context.pen_bench_rate_t.Select(Mapper.Map<PenBenchRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a PenBenchRate by PenBenchRateId
        /// </summary>
		/// <param name="pen_bench_rate_id">The pen_bench_rate_id of the entity.</param>        
        /// <returns>A Pen Bench Rate.</returns>
        public PenBenchRateModel GetByPenBenchRateId(Int32 pen_bench_rate_id)
        {
            return Mapper.Map<PenBenchRateModel>(this.context.pen_bench_rate_t.Where(p => p.pen_bench_rate_id == PenBenchRateId));
        }
       	    public List<PenBenchRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<PenBenchRateModel> GetByContractNbCarriageId(String contractNb, String carriageID) {
			throw new NotImplementedException();
		}
		public PenBenchRateModel GetByContractNbCarriageIdStartDtSubCdPenetratePercentStartSubCnt(String contractNb, String carriageID, DateTime startDt, String subCd, Double penetrationPercent, Decimal startSubCnt) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PenBenchRateModel model)
        {
            this.context.pen_bench_rate_t.Remove(Mapper.Map<pen_bench_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PenBenchRateModel model)
        {
            var record = this.context.pen_bench_rate_t.Where(p => p.pen_bench_rate_id == model.PenBenchRateId).FirstOrDefault();
            Mapper.Map<PenBenchRateModel, pen_bench_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PenBenchRateModel model)
        {
            this.context.pen_bench_rate_t.Add(Mapper.Map<pen_bench_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}