
// -----------------------------------------------------------------------
// <copyright file="SysSrvcRetailRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc Retail Rate repository.
    /// </summary>
    public class SysSrvcRetailRateRepository : ISysSrvcRetailRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcRetailRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcRetailRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcRetailRateModel> GetAll()
        {
            return this.context.sys_srvc_retail_rate_t.Select(Mapper.Map<SysSrvcRetailRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvcRetailRate by ProgSrvcNm SysId ALaCarteRetailRate SubCd
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="a_la_carte_retail_rate">The a_la_carte_retail_rate of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A Sys Srvc Retail Rate.</returns>
        public SysSrvcRetailRateModel GetByProgSrvcNmSysIdALaCarteRetailRateSubCd(String prog_srvc_nm,String sys_id,Decimal a_la_carte_retail_rate,String sub_cd)
        {
            return Mapper.Map<SysSrvcRetailRateModel>(this.context.sys_srvc_retail_rate_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.sys_id == SysId && p.a_la_carte_retail_rate == ALaCarteRetailRate && p.sub_cd == SubCd));
        }
       	    public List<SysSrvcRetailRateModel> GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcRetailRateModel model)
        {
            this.context.sys_srvc_retail_rate_t.Remove(Mapper.Map<sys_srvc_retail_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcRetailRateModel model)
        {
            var record = this.context.sys_srvc_retail_rate_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.sys_id == model.SysId && p.a_la_carte_retail_rate == model.ALaCarteRetailRate && p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<SysSrvcRetailRateModel, sys_srvc_retail_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcRetailRateModel model)
        {
            this.context.sys_srvc_retail_rate_t.Add(Mapper.Map<sys_srvc_retail_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}