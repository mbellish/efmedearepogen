
// -----------------------------------------------------------------------
// <copyright file="ContractHistoryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract History repository.
    /// </summary>
    public class ContractHistoryRepository : IContractHistoryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractHistoryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractHistoryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractHistoryModel> GetAll()
        {
            return this.context.contract_history_t.Select(Mapper.Map<ContractHistoryModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractHistory by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Contract History.</returns>
        public ContractHistoryModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<ContractHistoryModel>(this.context.contract_history_t.Where(p => p.row_nb == RowNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractHistoryModel model)
        {
            this.context.contract_history_t.Remove(Mapper.Map<contract_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractHistoryModel model)
        {
            var record = this.context.contract_history_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<ContractHistoryModel, contract_history_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractHistoryModel model)
        {
            this.context.contract_history_t.Add(Mapper.Map<contract_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}