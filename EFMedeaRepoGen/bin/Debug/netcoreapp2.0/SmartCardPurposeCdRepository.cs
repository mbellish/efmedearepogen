
// -----------------------------------------------------------------------
// <copyright file="SmartCardPurposeCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Smart Card Purpose Cd repository.
    /// </summary>
    public class SmartCardPurposeCdRepository : ISmartCardPurposeCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartCardPurposeCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SmartCardPurposeCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SmartCardPurposeCdModel> GetAll()
        {
            return this.context.smart_card_purpose_cd_t.Select(Mapper.Map<SmartCardPurposeCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a SmartCardPurposeCd by SmartCardPurposeCd
        /// </summary>
		/// <param name="smart_card_purpose_cd">The smart_card_purpose_cd of the entity.</param>        
        /// <returns>A Smart Card Purpose Cd.</returns>
        public SmartCardPurposeCdModel GetBySmartCardPurposeCd(String smart_card_purpose_cd)
        {
            return Mapper.Map<SmartCardPurposeCdModel>(this.context.smart_card_purpose_cd_t.Where(p => p.smart_card_purpose_cd == SmartCardPurposeCd));
        }
       	    public SmartCardPurposeCdModel GetBysmartCardPurposeCd(Int32 smartCardPurposeCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SmartCardPurposeCdModel model)
        {
            this.context.smart_card_purpose_cd_t.Remove(Mapper.Map<smart_card_purpose_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SmartCardPurposeCdModel model)
        {
            var record = this.context.smart_card_purpose_cd_t.Where(p => p.smart_card_purpose_cd == model.SmartCardPurposeCd).FirstOrDefault();
            Mapper.Map<SmartCardPurposeCdModel, smart_card_purpose_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SmartCardPurposeCdModel model)
        {
            this.context.smart_card_purpose_cd_t.Add(Mapper.Map<smart_card_purpose_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}