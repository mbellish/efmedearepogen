
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogRevenueAccrueAverageInvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log Revenue Accrue Average Invoice repository.
    /// </summary>
    public class InvoiceRecLogRevenueAccrueAverageInvoiceRepository : IInvoiceRecLogRevenueAccrueAverageInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogRevenueAccrueAverageInvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogRevenueAccrueAverageInvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogRevenueAccrueAverageInvoiceModel> GetAll()
        {
            return this.context.invoice_rec_log_revenue_accrue_average_invoice_t.Select(Mapper.Map<InvoiceRecLogRevenueAccrueAverageInvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLogRevenueAccrueAverageInvoice by InvoiceRecLogRevenueAccrueAverageId InvoiceNb
        /// </summary>
		/// <param name="invoice_rec_log_revenue_accrue_average_id">The invoice_rec_log_revenue_accrue_average_id of the entity.</param>/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Invoice Rec Log Revenue Accrue Average Invoice.</returns>
        public InvoiceRecLogRevenueAccrueAverageInvoiceModel GetByInvoiceRecLogRevenueAccrueAverageIdInvoiceNb(Int32 invoice_rec_log_revenue_accrue_average_id,String invoice_nb)
        {
            return Mapper.Map<InvoiceRecLogRevenueAccrueAverageInvoiceModel>(this.context.invoice_rec_log_revenue_accrue_average_invoice_t.Where(p => p.invoice_rec_log_revenue_accrue_average_id == InvoiceRecLogRevenueAccrueAverageId && p.invoice_nb == InvoiceNb));
        }
       	    public List<InvoiceRecLogRevenueAccrueAverageInvoiceModel> GetByInvoiceRecLogRevenueAccrueAverageId(Int32 id) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogRevenueAccrueAverageInvoiceModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_average_invoice_t.Remove(Mapper.Map<invoice_rec_log_revenue_accrue_average_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogRevenueAccrueAverageInvoiceModel model)
        {
            var record = this.context.invoice_rec_log_revenue_accrue_average_invoice_t.Where(p => p.invoice_rec_log_revenue_accrue_average_id == model.InvoiceRecLogRevenueAccrueAverageId && p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<InvoiceRecLogRevenueAccrueAverageInvoiceModel, invoice_rec_log_revenue_accrue_average_invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogRevenueAccrueAverageInvoiceModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_average_invoice_t.Add(Mapper.Map<invoice_rec_log_revenue_accrue_average_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}