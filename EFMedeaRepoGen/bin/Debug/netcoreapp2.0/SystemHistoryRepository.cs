
// -----------------------------------------------------------------------
// <copyright file="SystemHistoryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The System History repository.
    /// </summary>
    public class SystemHistoryRepository : ISystemHistoryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemHistoryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SystemHistoryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SystemHistoryModel> GetAll()
        {
            return this.context.system_history_t.Select(Mapper.Map<SystemHistoryModel>).ToList();
        }

		        /// <summary>
        /// Gets a SystemHistory by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A System History.</returns>
        public SystemHistoryModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<SystemHistoryModel>(this.context.system_history_t.Where(p => p.row_nb == RowNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SystemHistoryModel model)
        {
            this.context.system_history_t.Remove(Mapper.Map<system_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SystemHistoryModel model)
        {
            var record = this.context.system_history_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<SystemHistoryModel, system_history_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SystemHistoryModel model)
        {
            this.context.system_history_t.Add(Mapper.Map<system_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}