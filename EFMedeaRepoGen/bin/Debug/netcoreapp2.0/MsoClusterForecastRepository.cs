
// -----------------------------------------------------------------------
// <copyright file="MsoClusterForecastRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster Forecast repository.
    /// </summary>
    public class MsoClusterForecastRepository : IMsoClusterForecastRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterForecastRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterForecastRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterForecastModel> GetAll()
        {
            return this.context.mso_cluster_forecast_t.Select(Mapper.Map<MsoClusterForecastModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoClusterForecast by MsoNb ClusterNb ProgSrvcNm
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>        
        /// <returns>A Mso Cluster Forecast.</returns>
        public MsoClusterForecastModel GetByMsoNbClusterNbProgSrvcNm(String mso_nb,String cluster_nb,String prog_srvc_nm)
        {
            return Mapper.Map<MsoClusterForecastModel>(this.context.mso_cluster_forecast_t.Where(p => p.mso_nb == MsoNb && p.cluster_nb == ClusterNb && p.prog_srvc_nm == ProgSrvcNm));
        }
       	    public List<MsoClusterForecastModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterForecastModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterForecastModel model)
        {
            this.context.mso_cluster_forecast_t.Remove(Mapper.Map<mso_cluster_forecast_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterForecastModel model)
        {
            var record = this.context.mso_cluster_forecast_t.Where(p => p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb && p.prog_srvc_nm == model.ProgSrvcNm).FirstOrDefault();
            Mapper.Map<MsoClusterForecastModel, mso_cluster_forecast_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterForecastModel model)
        {
            this.context.mso_cluster_forecast_t.Add(Mapper.Map<mso_cluster_forecast_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}