
// -----------------------------------------------------------------------
// <copyright file="HeadendBillingDataRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Headend Billing Data repository.
    /// </summary>
    public class HeadendBillingDataRepository : IHeadendBillingDataRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="HeadendBillingDataRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public HeadendBillingDataRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<HeadendBillingDataModel> GetAll()
        {
            return this.context.headend_billing_data_t.Select(Mapper.Map<HeadendBillingDataModel>).ToList();
        }

		        /// <summary>
        /// Gets a HeadendBillingData by HeadendNb
        /// </summary>
		/// <param name="headend_nb">The headend_nb of the entity.</param>        
        /// <returns>A Headend Billing Data.</returns>
        public HeadendBillingDataModel GetByHeadendNb(String headend_nb)
        {
            return Mapper.Map<HeadendBillingDataModel>(this.context.headend_billing_data_t.Where(p => p.headend_nb == HeadendNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(HeadendBillingDataModel model)
        {
            this.context.headend_billing_data_t.Remove(Mapper.Map<headend_billing_data_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(HeadendBillingDataModel model)
        {
            var record = this.context.headend_billing_data_t.Where(p => p.headend_nb == model.HeadendNb).FirstOrDefault();
            Mapper.Map<HeadendBillingDataModel, headend_billing_data_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(HeadendBillingDataModel model)
        {
            this.context.headend_billing_data_t.Add(Mapper.Map<headend_billing_data_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}