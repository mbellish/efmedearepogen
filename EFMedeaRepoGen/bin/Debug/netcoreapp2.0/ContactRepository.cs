
// -----------------------------------------------------------------------
// <copyright file="ContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact repository.
    /// </summary>
    public class ContactRepository : IContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactModel> GetAll()
        {
            return this.context.contact_t.Select(Mapper.Map<ContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a Contact by ContactNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>        
        /// <returns>A Contact.</returns>
        public ContactModel GetByContactNb(String contact_nb)
        {
            return Mapper.Map<ContactModel>(this.context.contact_t.Where(p => p.contact_nb == ContactNb));
        }
       	    public List<ContactModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<ContactModel> GetByDeptCd(String deptCd) {
			throw new NotImplementedException();
		}
		public List<ContactModel> GetByLastNm(String lastNm) {
			throw new NotImplementedException();
		}
		public List<ContactModel> GetBySt(String st) {
			throw new NotImplementedException();
		}
		public List<ContactModel> GetByTitleCd(String titleCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactModel model)
        {
            this.context.contact_t.Remove(Mapper.Map<contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactModel model)
        {
            var record = this.context.contact_t.Where(p => p.contact_nb == model.ContactNb).FirstOrDefault();
            Mapper.Map<ContactModel, contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactModel model)
        {
            this.context.contact_t.Add(Mapper.Map<contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}