
// -----------------------------------------------------------------------
// <copyright file="MsoClusterUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster User Def repository.
    /// </summary>
    public class MsoClusterUserDefRepository : IMsoClusterUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterUserDefModel> GetAll()
        {
            return this.context.mso_cluster_user_def_t.Select(Mapper.Map<MsoClusterUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoClusterUserDef by MsoNb ClusterNb UserDefCd RowNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Mso Cluster User Def.</returns>
        public MsoClusterUserDefModel GetByMsoNbClusterNbUserDefCdRowNb(String mso_nb,String cluster_nb,String user_def_cd,Int32 row_nb)
        {
            return Mapper.Map<MsoClusterUserDefModel>(this.context.mso_cluster_user_def_t.Where(p => p.mso_nb == MsoNb && p.cluster_nb == ClusterNb && p.user_def_cd == UserDefCd && p.row_nb == RowNb));
        }
       	    public List<MsoClusterUserDefModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterUserDefModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		public MsoClusterUserDefModel GetByRowNbMsoNbClusterNbUserDefCd(Int32 rowNb, String msoNb, String clusterNb, String userDefCd) {
			throw new NotImplementedException();
		}
		public List<MsoClusterUserDefModel> GetByUserDefCdDropDownId(Nullable<Int32> userDefCdDropDownId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterUserDefModel model)
        {
            this.context.mso_cluster_user_def_t.Remove(Mapper.Map<mso_cluster_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterUserDefModel model)
        {
            var record = this.context.mso_cluster_user_def_t.Where(p => p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb && p.user_def_cd == model.UserDefCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<MsoClusterUserDefModel, mso_cluster_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterUserDefModel model)
        {
            this.context.mso_cluster_user_def_t.Add(Mapper.Map<mso_cluster_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}