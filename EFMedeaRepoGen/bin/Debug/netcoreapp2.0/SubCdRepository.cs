
// -----------------------------------------------------------------------
// <copyright file="SubCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sub Cd repository.
    /// </summary>
    public class SubCdRepository : ISubCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SubCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SubCdModel> GetAll()
        {
            return this.context.sub_cd_t.Select(Mapper.Map<SubCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a SubCd by SubCd
        /// </summary>
		/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A Sub Cd.</returns>
        public SubCdModel GetBySubCd(String sub_cd)
        {
            return Mapper.Map<SubCdModel>(this.context.sub_cd_t.Where(p => p.sub_cd == SubCd));
        }
       	    public List<SubCdModel> GetByProgSrvcNmFromSubSrvc(String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SubCdModel model)
        {
            this.context.sub_cd_t.Remove(Mapper.Map<sub_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SubCdModel model)
        {
            var record = this.context.sub_cd_t.Where(p => p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<SubCdModel, sub_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SubCdModel model)
        {
            this.context.sub_cd_t.Add(Mapper.Map<sub_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}