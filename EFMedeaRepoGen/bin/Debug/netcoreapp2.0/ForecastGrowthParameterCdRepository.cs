
// -----------------------------------------------------------------------
// <copyright file="ForecastGrowthParameterCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Growth Parameter Cd repository.
    /// </summary>
    public class ForecastGrowthParameterCdRepository : IForecastGrowthParameterCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastGrowthParameterCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastGrowthParameterCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastGrowthParameterCdModel> GetAll()
        {
            return this.context.forecast_growth_parameter_cd_t.Select(Mapper.Map<ForecastGrowthParameterCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastGrowthParameterCd by ForecastGrowthParameterCd
        /// </summary>
		/// <param name="forecast_growth_parameter_cd">The forecast_growth_parameter_cd of the entity.</param>        
        /// <returns>A Forecast Growth Parameter Cd.</returns>
        public ForecastGrowthParameterCdModel GetByForecastGrowthParameterCd(String forecast_growth_parameter_cd)
        {
            return Mapper.Map<ForecastGrowthParameterCdModel>(this.context.forecast_growth_parameter_cd_t.Where(p => p.forecast_growth_parameter_cd == ForecastGrowthParameterCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastGrowthParameterCdModel model)
        {
            this.context.forecast_growth_parameter_cd_t.Remove(Mapper.Map<forecast_growth_parameter_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastGrowthParameterCdModel model)
        {
            var record = this.context.forecast_growth_parameter_cd_t.Where(p => p.forecast_growth_parameter_cd == model.ForecastGrowthParameterCd).FirstOrDefault();
            Mapper.Map<ForecastGrowthParameterCdModel, forecast_growth_parameter_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastGrowthParameterCdModel model)
        {
            this.context.forecast_growth_parameter_cd_t.Add(Mapper.Map<forecast_growth_parameter_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}