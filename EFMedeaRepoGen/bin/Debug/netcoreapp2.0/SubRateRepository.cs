
// -----------------------------------------------------------------------
// <copyright file="SubRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sub Rate repository.
    /// </summary>
    public class SubRateRepository : ISubRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SubRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SubRateModel> GetAll()
        {
            return this.context.sub_rate_t.Select(Mapper.Map<SubRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a SubRate by SubRateId
        /// </summary>
		/// <param name="sub_rate_id">The sub_rate_id of the entity.</param>        
        /// <returns>A Sub Rate.</returns>
        public SubRateModel GetBySubRateId(Int32 sub_rate_id)
        {
            return Mapper.Map<SubRateModel>(this.context.sub_rate_t.Where(p => p.sub_rate_id == SubRateId));
        }
       	    public IList<SubRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SubRateModel model)
        {
            this.context.sub_rate_t.Remove(Mapper.Map<sub_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SubRateModel model)
        {
            var record = this.context.sub_rate_t.Where(p => p.sub_rate_id == model.SubRateId).FirstOrDefault();
            Mapper.Map<SubRateModel, sub_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SubRateModel model)
        {
            this.context.sub_rate_t.Add(Mapper.Map<sub_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}