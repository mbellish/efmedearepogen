
// -----------------------------------------------------------------------
// <copyright file="EquipmentSpecsRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Specs repository.
    /// </summary>
    public class EquipmentSpecsRepository : IEquipmentSpecsRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentSpecsRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentSpecsRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentSpecsModel> GetAll()
        {
            return this.context.equipment_specs_t.Select(Mapper.Map<EquipmentSpecsModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentSpecs by EquipmentSpecsId
        /// </summary>
		/// <param name="equipment_specs_id">The equipment_specs_id of the entity.</param>        
        /// <returns>A Equipment Specs.</returns>
        public EquipmentSpecsModel GetByEquipmentSpecsId(Int32 equipment_specs_id)
        {
            return Mapper.Map<EquipmentSpecsModel>(this.context.equipment_specs_t.Where(p => p.equipment_specs_id == EquipmentSpecsId));
        }
       	    public List<EquipmentSpecsModel> GetByEquipCd(String equipCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentSpecsModel model)
        {
            this.context.equipment_specs_t.Remove(Mapper.Map<equipment_specs_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentSpecsModel model)
        {
            var record = this.context.equipment_specs_t.Where(p => p.equipment_specs_id == model.EquipmentSpecsId).FirstOrDefault();
            Mapper.Map<EquipmentSpecsModel, equipment_specs_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentSpecsModel model)
        {
            this.context.equipment_specs_t.Add(Mapper.Map<equipment_specs_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}