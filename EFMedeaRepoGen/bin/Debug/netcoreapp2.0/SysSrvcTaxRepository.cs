
// -----------------------------------------------------------------------
// <copyright file="SysSrvcTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc Tax repository.
    /// </summary>
    public class SysSrvcTaxRepository : ISysSrvcTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcTaxModel> GetAll()
        {
            return this.context.sys_srvc_tax_t.Select(Mapper.Map<SysSrvcTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvcTax by SysId ProgSrvcNm TaxCd
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="tax_cd">The tax_cd of the entity.</param>        
        /// <returns>A Sys Srvc Tax.</returns>
        public SysSrvcTaxModel GetBySysIdProgSrvcNmTaxCd(String sys_id,String prog_srvc_nm,String tax_cd)
        {
            return Mapper.Map<SysSrvcTaxModel>(this.context.sys_srvc_tax_t.Where(p => p.sys_id == SysId && p.prog_srvc_nm == ProgSrvcNm && p.tax_cd == TaxCd));
        }
       	    public List<SysSrvcTaxModel> GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcTaxModel model)
        {
            this.context.sys_srvc_tax_t.Remove(Mapper.Map<sys_srvc_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcTaxModel model)
        {
            var record = this.context.sys_srvc_tax_t.Where(p => p.sys_id == model.SysId && p.prog_srvc_nm == model.ProgSrvcNm && p.tax_cd == model.TaxCd).FirstOrDefault();
            Mapper.Map<SysSrvcTaxModel, sys_srvc_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcTaxModel model)
        {
            this.context.sys_srvc_tax_t.Add(Mapper.Map<sys_srvc_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}