
// -----------------------------------------------------------------------
// <copyright file="ContractCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Cd repository.
    /// </summary>
    public class ContractCdRepository : IContractCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCdModel> GetAll()
        {
            return this.context.contract_cd_t.Select(Mapper.Map<ContractCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCd by ContractCd
        /// </summary>
		/// <param name="contract_cd">The contract_cd of the entity.</param>        
        /// <returns>A Contract Cd.</returns>
        public ContractCdModel GetByContractCd(String contract_cd)
        {
            return Mapper.Map<ContractCdModel>(this.context.contract_cd_t.Where(p => p.contract_cd == ContractCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCdModel model)
        {
            this.context.contract_cd_t.Remove(Mapper.Map<contract_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCdModel model)
        {
            var record = this.context.contract_cd_t.Where(p => p.contract_cd == model.ContractCd).FirstOrDefault();
            Mapper.Map<ContractCdModel, contract_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCdModel model)
        {
            this.context.contract_cd_t.Add(Mapper.Map<contract_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}