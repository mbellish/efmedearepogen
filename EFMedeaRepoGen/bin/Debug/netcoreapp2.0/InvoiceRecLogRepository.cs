
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log repository.
    /// </summary>
    public class InvoiceRecLogRepository : IInvoiceRecLogRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogModel> GetAll()
        {
            return this.context.invoice_rec_log_t.Select(Mapper.Map<InvoiceRecLogModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLog by InvoiceRecLogId
        /// </summary>
		/// <param name="invoice_rec_log_id">The invoice_rec_log_id of the entity.</param>        
        /// <returns>A Invoice Rec Log.</returns>
        public InvoiceRecLogModel GetByInvoiceRecLogId(Int32 invoice_rec_log_id)
        {
            return Mapper.Map<InvoiceRecLogModel>(this.context.invoice_rec_log_t.Where(p => p.invoice_rec_log_id == InvoiceRecLogId));
        }
       	    public List<InvoiceRecLogModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogModel model)
        {
            this.context.invoice_rec_log_t.Remove(Mapper.Map<invoice_rec_log_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogModel model)
        {
            var record = this.context.invoice_rec_log_t.Where(p => p.invoice_rec_log_id == model.InvoiceRecLogId).FirstOrDefault();
            Mapper.Map<InvoiceRecLogModel, invoice_rec_log_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogModel model)
        {
            this.context.invoice_rec_log_t.Add(Mapper.Map<invoice_rec_log_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}