
// -----------------------------------------------------------------------
// <copyright file="ForecastContractTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Contract Tax repository.
    /// </summary>
    public class ForecastContractTaxRepository : IForecastContractTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastContractTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastContractTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastContractTaxModel> GetAll()
        {
            return this.context.forecast_contract_tax_t.Select(Mapper.Map<ForecastContractTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastContractTax by ForecastContractTaxId
        /// </summary>
		/// <param name="forecast_contract_tax_id">The forecast_contract_tax_id of the entity.</param>        
        /// <returns>A Forecast Contract Tax.</returns>
        public ForecastContractTaxModel GetByForecastContractTaxId(Int32 forecast_contract_tax_id)
        {
            return Mapper.Map<ForecastContractTaxModel>(this.context.forecast_contract_tax_t.Where(p => p.forecast_contract_tax_id == ForecastContractTaxId));
        }
       	    public IList<ForecastContractTaxModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastContractTaxModel model)
        {
            this.context.forecast_contract_tax_t.Remove(Mapper.Map<forecast_contract_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastContractTaxModel model)
        {
            var record = this.context.forecast_contract_tax_t.Where(p => p.forecast_contract_tax_id == model.ForecastContractTaxId).FirstOrDefault();
            Mapper.Map<ForecastContractTaxModel, forecast_contract_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastContractTaxModel model)
        {
            this.context.forecast_contract_tax_t.Add(Mapper.Map<forecast_contract_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}