
// -----------------------------------------------------------------------
// <copyright file="MsoClusterPromoRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster Promo repository.
    /// </summary>
    public class MsoClusterPromoRepository : IMsoClusterPromoRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterPromoRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterPromoRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterPromoModel> GetAll()
        {
            return this.context.mso_cluster_promo_t.Select(Mapper.Map<MsoClusterPromoModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoClusterPromo by MsoNb ClusterNb PromoId
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>/// <param name="promo_id">The promo_id of the entity.</param>        
        /// <returns>A Mso Cluster Promo.</returns>
        public MsoClusterPromoModel GetByMsoNbClusterNbPromoId(String mso_nb,String cluster_nb,String promo_id)
        {
            return Mapper.Map<MsoClusterPromoModel>(this.context.mso_cluster_promo_t.Where(p => p.mso_nb == MsoNb && p.cluster_nb == ClusterNb && p.promo_id == PromoId));
        }
       	    public List<MsoClusterPromoModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterPromoModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterPromoModel model)
        {
            this.context.mso_cluster_promo_t.Remove(Mapper.Map<mso_cluster_promo_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterPromoModel model)
        {
            var record = this.context.mso_cluster_promo_t.Where(p => p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb && p.promo_id == model.PromoId).FirstOrDefault();
            Mapper.Map<MsoClusterPromoModel, mso_cluster_promo_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterPromoModel model)
        {
            this.context.mso_cluster_promo_t.Add(Mapper.Map<mso_cluster_promo_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}