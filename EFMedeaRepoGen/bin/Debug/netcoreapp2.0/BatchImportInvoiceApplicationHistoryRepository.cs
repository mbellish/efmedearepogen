
// -----------------------------------------------------------------------
// <copyright file="BatchImportInvoiceApplicationHistoryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Batch Import Invoice Application History repository.
    /// </summary>
    public class BatchImportInvoiceApplicationHistoryRepository : IBatchImportInvoiceApplicationHistoryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchImportInvoiceApplicationHistoryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BatchImportInvoiceApplicationHistoryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BatchImportInvoiceApplicationHistoryModel> GetAll()
        {
            return this.context.batch_import_invoice_application_history_t.Select(Mapper.Map<BatchImportInvoiceApplicationHistoryModel>).ToList();
        }

		        /// <summary>
        /// Gets a BatchImportInvoiceApplicationHistory by HistoryId
        /// </summary>
		/// <param name="history_id">The history_id of the entity.</param>        
        /// <returns>A Batch Import Invoice Application History.</returns>
        public BatchImportInvoiceApplicationHistoryModel GetByHistoryId(Int32 history_id)
        {
            return Mapper.Map<BatchImportInvoiceApplicationHistoryModel>(this.context.batch_import_invoice_application_history_t.Where(p => p.history_id == HistoryId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BatchImportInvoiceApplicationHistoryModel model)
        {
            this.context.batch_import_invoice_application_history_t.Remove(Mapper.Map<batch_import_invoice_application_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BatchImportInvoiceApplicationHistoryModel model)
        {
            var record = this.context.batch_import_invoice_application_history_t.Where(p => p.history_id == model.HistoryId).FirstOrDefault();
            Mapper.Map<BatchImportInvoiceApplicationHistoryModel, batch_import_invoice_application_history_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BatchImportInvoiceApplicationHistoryModel model)
        {
            this.context.batch_import_invoice_application_history_t.Add(Mapper.Map<batch_import_invoice_application_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}