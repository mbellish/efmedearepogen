
// -----------------------------------------------------------------------
// <copyright file="MasterAgreementContractRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Master Agreement Contract repository.
    /// </summary>
    public class MasterAgreementContractRepository : IMasterAgreementContractRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterAgreementContractRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MasterAgreementContractRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MasterAgreementContractModel> GetAll()
        {
            return this.context.master_agreement_contract_t.Select(Mapper.Map<MasterAgreementContractModel>).ToList();
        }

		        /// <summary>
        /// Gets a MasterAgreementContract by MasterAgreementContractId
        /// </summary>
		/// <param name="master_agreement_contract_id">The master_agreement_contract_id of the entity.</param>        
        /// <returns>A Master Agreement Contract.</returns>
        public MasterAgreementContractModel GetByMasterAgreementContractId(Int32 master_agreement_contract_id)
        {
            return Mapper.Map<MasterAgreementContractModel>(this.context.master_agreement_contract_t.Where(p => p.master_agreement_contract_id == MasterAgreementContractId));
        }
       	    public MasterAgreementContractModel GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementContractModel> GetByMasterAgreementId(String masterAgreementId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MasterAgreementContractModel model)
        {
            this.context.master_agreement_contract_t.Remove(Mapper.Map<master_agreement_contract_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MasterAgreementContractModel model)
        {
            var record = this.context.master_agreement_contract_t.Where(p => p.master_agreement_contract_id == model.MasterAgreementContractId).FirstOrDefault();
            Mapper.Map<MasterAgreementContractModel, master_agreement_contract_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MasterAgreementContractModel model)
        {
            this.context.master_agreement_contract_t.Add(Mapper.Map<master_agreement_contract_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}