
// -----------------------------------------------------------------------
// <copyright file="EquipmentSrvcBlackoutTierRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Srvc Blackout Tier repository.
    /// </summary>
    public class EquipmentSrvcBlackoutTierRepository : IEquipmentSrvcBlackoutTierRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentSrvcBlackoutTierRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentSrvcBlackoutTierRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentSrvcBlackoutTierModel> GetAll()
        {
            return this.context.equipment_srvc_blackout_tier_t.Select(Mapper.Map<EquipmentSrvcBlackoutTierModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentSrvcBlackoutTier by BlackoutCd ProgSrvcNm Tier
        /// </summary>
		/// <param name="blackout_cd">The blackout_cd of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="tier">The tier of the entity.</param>        
        /// <returns>A Equipment Srvc Blackout Tier.</returns>
        public EquipmentSrvcBlackoutTierModel GetByBlackoutCdProgSrvcNmTier(String blackout_cd,String prog_srvc_nm,Int32 tier)
        {
            return Mapper.Map<EquipmentSrvcBlackoutTierModel>(this.context.equipment_srvc_blackout_tier_t.Where(p => p.blackout_cd == BlackoutCd && p.prog_srvc_nm == ProgSrvcNm && p.tier == Tier));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentSrvcBlackoutTierModel model)
        {
            this.context.equipment_srvc_blackout_tier_t.Remove(Mapper.Map<equipment_srvc_blackout_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentSrvcBlackoutTierModel model)
        {
            var record = this.context.equipment_srvc_blackout_tier_t.Where(p => p.blackout_cd == model.BlackoutCd && p.prog_srvc_nm == model.ProgSrvcNm && p.tier == model.Tier).FirstOrDefault();
            Mapper.Map<EquipmentSrvcBlackoutTierModel, equipment_srvc_blackout_tier_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentSrvcBlackoutTierModel model)
        {
            this.context.equipment_srvc_blackout_tier_t.Add(Mapper.Map<equipment_srvc_blackout_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}