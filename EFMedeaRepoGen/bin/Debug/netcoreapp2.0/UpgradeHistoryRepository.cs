
// -----------------------------------------------------------------------
// <copyright file="UpgradeHistoryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Upgrade History repository.
    /// </summary>
    public class UpgradeHistoryRepository : IUpgradeHistoryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpgradeHistoryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UpgradeHistoryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UpgradeHistoryModel> GetAll()
        {
            return this.context.upgrade_history_t.Select(Mapper.Map<UpgradeHistoryModel>).ToList();
        }

		        /// <summary>
        /// Gets a UpgradeHistory by Product InstallDt
        /// </summary>
		/// <param name="product">The product of the entity.</param>/// <param name="install_dt">The install_dt of the entity.</param>        
        /// <returns>A Upgrade History.</returns>
        public UpgradeHistoryModel GetByProductInstallDt(String product,DateTime install_dt)
        {
            return Mapper.Map<UpgradeHistoryModel>(this.context.upgrade_history_t.Where(p => p.product == Product && p.install_dt == InstallDt));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UpgradeHistoryModel model)
        {
            this.context.upgrade_history_t.Remove(Mapper.Map<upgrade_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UpgradeHistoryModel model)
        {
            var record = this.context.upgrade_history_t.Where(p => p.product == model.Product && p.install_dt == model.InstallDt).FirstOrDefault();
            Mapper.Map<UpgradeHistoryModel, upgrade_history_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UpgradeHistoryModel model)
        {
            this.context.upgrade_history_t.Add(Mapper.Map<upgrade_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}