
// -----------------------------------------------------------------------
// <copyright file="ContractCarriageRiderRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Carriage Rider repository.
    /// </summary>
    public class ContractCarriageRiderRepository : IContractCarriageRiderRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCarriageRiderRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCarriageRiderRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCarriageRiderModel> GetAll()
        {
            return this.context.contract_carriage_rider_t.Select(Mapper.Map<ContractCarriageRiderModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCarriageRider by ContractNb CarriageId LinkContractNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="link_contract_nb">The link_contract_nb of the entity.</param>        
        /// <returns>A Contract Carriage Rider.</returns>
        public ContractCarriageRiderModel GetByContractNbCarriageIdLinkContractNb(String contract_nb,String carriage_id,String link_contract_nb)
        {
            return Mapper.Map<ContractCarriageRiderModel>(this.context.contract_carriage_rider_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.link_contract_nb == LinkContractNb));
        }
       	    public List<ContractCarriageRiderModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ContractCarriageRiderModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCarriageRiderModel model)
        {
            this.context.contract_carriage_rider_t.Remove(Mapper.Map<contract_carriage_rider_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCarriageRiderModel model)
        {
            var record = this.context.contract_carriage_rider_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.link_contract_nb == model.LinkContractNb).FirstOrDefault();
            Mapper.Map<ContractCarriageRiderModel, contract_carriage_rider_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCarriageRiderModel model)
        {
            this.context.contract_carriage_rider_t.Add(Mapper.Map<contract_carriage_rider_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}