
// -----------------------------------------------------------------------
// <copyright file="SysSrvcConRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc Con repository.
    /// </summary>
    public class SysSrvcConRepository : ISysSrvcConRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcConRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcConRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcConModel> GetAll()
        {
            return this.context.sys_srvc_con_t.Select(Mapper.Map<SysSrvcConModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvcCon by ProgSrvcNm SysId
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Sys Srvc Con.</returns>
        public SysSrvcConModel GetByProgSrvcNmSysId(String prog_srvc_nm,String sys_id)
        {
            return Mapper.Map<SysSrvcConModel>(this.context.sys_srvc_con_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.sys_id == SysId));
        }
       	    public List<SysSrvcConModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcConModel model)
        {
            this.context.sys_srvc_con_t.Remove(Mapper.Map<sys_srvc_con_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcConModel model)
        {
            var record = this.context.sys_srvc_con_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<SysSrvcConModel, sys_srvc_con_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcConModel model)
        {
            this.context.sys_srvc_con_t.Add(Mapper.Map<sys_srvc_con_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}