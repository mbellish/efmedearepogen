
// -----------------------------------------------------------------------
// <copyright file="ContractSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Sub repository.
    /// </summary>
    public class ContractSubRepository : IContractSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractSubModel> GetAll()
        {
            return this.context.contract_sub_t.Select(Mapper.Map<ContractSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractSub by ContractNb SubDt
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>        
        /// <returns>A Contract Sub.</returns>
        public ContractSubModel GetByContractNbSubDt(String contract_nb,DateTime sub_dt)
        {
            return Mapper.Map<ContractSubModel>(this.context.contract_sub_t.Where(p => p.contract_nb == ContractNb && p.sub_dt == SubDt));
        }
       	    public IList<ContractSubModel> GetBasicSubsForContractBySubDt(String contractNb, DateTime subDt) {
			throw new NotImplementedException();
		}
		public ContractSubModel GetLatestSubsAsOfBillingPeriod(String contractNb, DateTime subDt) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractSubModel model)
        {
            this.context.contract_sub_t.Remove(Mapper.Map<contract_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractSubModel model)
        {
            var record = this.context.contract_sub_t.Where(p => p.contract_nb == model.ContractNb && p.sub_dt == model.SubDt).FirstOrDefault();
            Mapper.Map<ContractSubModel, contract_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractSubModel model)
        {
            this.context.contract_sub_t.Add(Mapper.Map<contract_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}