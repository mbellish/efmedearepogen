
// -----------------------------------------------------------------------
// <copyright file="AggregatePenRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Aggregate Pen Rate repository.
    /// </summary>
    public class AggregatePenRateRepository : IAggregatePenRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AggregatePenRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public AggregatePenRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<AggregatePenRateModel> GetAll()
        {
            return this.context.aggregate_pen_rate_t.Select(Mapper.Map<AggregatePenRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a AggregatePenRate by AggregatePenRateId
        /// </summary>
		/// <param name="aggregate_pen_rate_id">The aggregate_pen_rate_id of the entity.</param>        
        /// <returns>A Aggregate Pen Rate.</returns>
        public AggregatePenRateModel GetByAggregatePenRateId(String aggregate_pen_rate_id)
        {
            return Mapper.Map<AggregatePenRateModel>(this.context.aggregate_pen_rate_t.Where(p => p.aggregate_pen_rate_id == AggregatePenRateId));
        }
       	    public IList<AggregatePenRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public IList<AggregatePenRateModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		public AggregatePenRateModel GetByContractNbCarriageIdStartDtPenetrationSysSubCd(String contractNb, String carriageId, DateTime startDt, Double penetration, String subCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(AggregatePenRateModel model)
        {
            this.context.aggregate_pen_rate_t.Remove(Mapper.Map<aggregate_pen_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(AggregatePenRateModel model)
        {
            var record = this.context.aggregate_pen_rate_t.Where(p => p.aggregate_pen_rate_id == model.AggregatePenRateId).FirstOrDefault();
            Mapper.Map<AggregatePenRateModel, aggregate_pen_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(AggregatePenRateModel model)
        {
            this.context.aggregate_pen_rate_t.Add(Mapper.Map<aggregate_pen_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}