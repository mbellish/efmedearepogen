
// -----------------------------------------------------------------------
// <copyright file="MsoDistrictRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso District repository.
    /// </summary>
    public class MsoDistrictRepository : IMsoDistrictRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDistrictRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDistrictRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDistrictModel> GetAll()
        {
            return this.context.mso_district_t.Select(Mapper.Map<MsoDistrictModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDistrict by MsoNb DistrictNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="district_nb">The district_nb of the entity.</param>        
        /// <returns>A Mso District.</returns>
        public MsoDistrictModel GetByMsoNbDistrictNb(String mso_nb,String district_nb)
        {
            return Mapper.Map<MsoDistrictModel>(this.context.mso_district_t.Where(p => p.mso_nb == MsoNb && p.district_nb == DistrictNb));
        }
       	    public List<MsoDistrictModel> GetByDistrictNb(String districtNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictModel> GetByMsoNbMsoDivisionNb(String msoNb, String msoDivisionNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictModel> GetByMsoNbMsoRegionNb(String msoNb, String msoRegionNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDistrictModel model)
        {
            this.context.mso_district_t.Remove(Mapper.Map<mso_district_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDistrictModel model)
        {
            var record = this.context.mso_district_t.Where(p => p.mso_nb == model.MsoNb && p.district_nb == model.DistrictNb).FirstOrDefault();
            Mapper.Map<MsoDistrictModel, mso_district_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDistrictModel model)
        {
            this.context.mso_district_t.Add(Mapper.Map<mso_district_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}