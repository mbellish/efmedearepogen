
// -----------------------------------------------------------------------
// <copyright file="ContractAllocationRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Allocation repository.
    /// </summary>
    public class ContractAllocationRepository : IContractAllocationRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractAllocationRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractAllocationRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractAllocationModel> GetAll()
        {
            return this.context.contract_allocation_t.Select(Mapper.Map<ContractAllocationModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractAllocation by ContractAllocationId
        /// </summary>
		/// <param name="contract_allocation_id">The contract_allocation_id of the entity.</param>        
        /// <returns>A Contract Allocation.</returns>
        public ContractAllocationModel GetByContractAllocationId(Int32 contract_allocation_id)
        {
            return Mapper.Map<ContractAllocationModel>(this.context.contract_allocation_t.Where(p => p.contract_allocation_id == ContractAllocationId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractAllocationModel model)
        {
            this.context.contract_allocation_t.Remove(Mapper.Map<contract_allocation_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractAllocationModel model)
        {
            var record = this.context.contract_allocation_t.Where(p => p.contract_allocation_id == model.ContractAllocationId).FirstOrDefault();
            Mapper.Map<ContractAllocationModel, contract_allocation_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractAllocationModel model)
        {
            this.context.contract_allocation_t.Add(Mapper.Map<contract_allocation_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}