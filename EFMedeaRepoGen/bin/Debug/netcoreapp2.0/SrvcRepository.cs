
// -----------------------------------------------------------------------
// <copyright file="SrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Srvc repository.
    /// </summary>
    public class SrvcRepository : ISrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SrvcModel> GetAll()
        {
            return this.context.srvc_t.Select(Mapper.Map<SrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a Srvc by ProgSrvcNm
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>        
        /// <returns>A Srvc.</returns>
        public SrvcModel GetByProgSrvcNm(String prog_srvc_nm)
        {
            return Mapper.Map<SrvcModel>(this.context.srvc_t.Where(p => p.prog_srvc_nm == ProgSrvcNm));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SrvcModel model)
        {
            this.context.srvc_t.Remove(Mapper.Map<srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SrvcModel model)
        {
            var record = this.context.srvc_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm).FirstOrDefault();
            Mapper.Map<SrvcModel, srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SrvcModel model)
        {
            this.context.srvc_t.Add(Mapper.Map<srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}