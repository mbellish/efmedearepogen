
// -----------------------------------------------------------------------
// <copyright file="ContactUserCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact User Cd repository.
    /// </summary>
    public class ContactUserCdRepository : IContactUserCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactUserCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactUserCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactUserCdModel> GetAll()
        {
            return this.context.contact_user_cd_t.Select(Mapper.Map<ContactUserCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactUserCd by ContactUserCd
        /// </summary>
		/// <param name="contact_user_cd">The contact_user_cd of the entity.</param>        
        /// <returns>A Contact User Cd.</returns>
        public ContactUserCdModel GetByContactUserCd(String contact_user_cd)
        {
            return Mapper.Map<ContactUserCdModel>(this.context.contact_user_cd_t.Where(p => p.contact_user_cd == ContactUserCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactUserCdModel model)
        {
            this.context.contact_user_cd_t.Remove(Mapper.Map<contact_user_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactUserCdModel model)
        {
            var record = this.context.contact_user_cd_t.Where(p => p.contact_user_cd == model.ContactUserCd).FirstOrDefault();
            Mapper.Map<ContactUserCdModel, contact_user_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactUserCdModel model)
        {
            this.context.contact_user_cd_t.Add(Mapper.Map<contact_user_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}