
// -----------------------------------------------------------------------
// <copyright file="MsoContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Contact repository.
    /// </summary>
    public class MsoContactRepository : IMsoContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoContactModel> GetAll()
        {
            return this.context.mso_contact_t.Select(Mapper.Map<MsoContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoContact by ContactNb MsoNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>        
        /// <returns>A Mso Contact.</returns>
        public MsoContactModel GetByContactNbMsoNb(String contact_nb,String mso_nb)
        {
            return Mapper.Map<MsoContactModel>(this.context.mso_contact_t.Where(p => p.contact_nb == ContactNb && p.mso_nb == MsoNb));
        }
       	    public List<MsoContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<MsoContactModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoContactModel model)
        {
            this.context.mso_contact_t.Remove(Mapper.Map<mso_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoContactModel model)
        {
            var record = this.context.mso_contact_t.Where(p => p.contact_nb == model.ContactNb && p.mso_nb == model.MsoNb).FirstOrDefault();
            Mapper.Map<MsoContactModel, mso_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoContactModel model)
        {
            this.context.mso_contact_t.Add(Mapper.Map<mso_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}