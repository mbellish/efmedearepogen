
// -----------------------------------------------------------------------
// <copyright file="LocalAdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Local Ad repository.
    /// </summary>
    public class LocalAdRepository : ILocalAdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalAdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public LocalAdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<LocalAdModel> GetAll()
        {
            return this.context.local_ad_t.Select(Mapper.Map<LocalAdModel>).ToList();
        }

		        /// <summary>
        /// Gets a LocalAd by SysId
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Local Ad.</returns>
        public LocalAdModel GetBySysId(String sys_id)
        {
            return Mapper.Map<LocalAdModel>(this.context.local_ad_t.Where(p => p.sys_id == SysId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(LocalAdModel model)
        {
            this.context.local_ad_t.Remove(Mapper.Map<local_ad_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(LocalAdModel model)
        {
            var record = this.context.local_ad_t.Where(p => p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<LocalAdModel, local_ad_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(LocalAdModel model)
        {
            this.context.local_ad_t.Add(Mapper.Map<local_ad_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}