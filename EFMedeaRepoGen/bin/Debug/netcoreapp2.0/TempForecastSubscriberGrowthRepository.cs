
// -----------------------------------------------------------------------
// <copyright file="TempForecastSubscriberGrowthRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Temp Forecast Subscriber Growth repository.
    /// </summary>
    public class TempForecastSubscriberGrowthRepository : ITempForecastSubscriberGrowthRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TempForecastSubscriberGrowthRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public TempForecastSubscriberGrowthRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<TempForecastSubscriberGrowthModel> GetAll()
        {
            return this.context.temp_forecast_subscriber_growth_t.Select(Mapper.Map<TempForecastSubscriberGrowthModel>).ToList();
        }

		        /// <summary>
        /// Gets a TempForecastSubscriberGrowth by RowId
        /// </summary>
		/// <param name="row_id">The row_id of the entity.</param>        
        /// <returns>A Temp Forecast Subscriber Growth.</returns>
        public TempForecastSubscriberGrowthModel GetByRowId(Int32 row_id)
        {
            return Mapper.Map<TempForecastSubscriberGrowthModel>(this.context.temp_forecast_subscriber_growth_t.Where(p => p.row_id == RowId));
        }
       	    public Void DetermineSubscriberGrowth(Int32 forecastId) {
			throw new NotImplementedException();
		}
		public List<DateTime> GetUniqueDates() {
			throw new NotImplementedException();
		}
		public Void ApplySubscriberGrowth(Int32 forecastId, DateTime subDt, Nullable<DateTime> previousSubDt, Nullable<DateTime> useSubsFromPreviousDate, String userId) {
			throw new NotImplementedException();
		}
		public List<TempForecastSubscriberGrowthModel> BuildSystemList(Int32 forecastId, String cntryCd, String salesRegion) {
			throw new NotImplementedException();
		}
		public Void BulkInsert(List<TempForecastSubscriberGrowthModel> subGrowthModel) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(TempForecastSubscriberGrowthModel model)
        {
            this.context.temp_forecast_subscriber_growth_t.Remove(Mapper.Map<temp_forecast_subscriber_growth_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(TempForecastSubscriberGrowthModel model)
        {
            var record = this.context.temp_forecast_subscriber_growth_t.Where(p => p.row_id == model.RowId).FirstOrDefault();
            Mapper.Map<TempForecastSubscriberGrowthModel, temp_forecast_subscriber_growth_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(TempForecastSubscriberGrowthModel model)
        {
            this.context.temp_forecast_subscriber_growth_t.Add(Mapper.Map<temp_forecast_subscriber_growth_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}