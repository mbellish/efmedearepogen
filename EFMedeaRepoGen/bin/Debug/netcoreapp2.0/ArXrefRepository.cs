
// -----------------------------------------------------------------------
// <copyright file="ArXrefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Ar Xref repository.
    /// </summary>
    public class ArXrefRepository : IArXrefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArXrefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArXrefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArXrefModel> GetAll()
        {
            return this.context.ar_xref_t.Select(Mapper.Map<ArXrefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArXref by InvoiceNb1 InvoiceNb2 PostDt
        /// </summary>
		/// <param name="invoice_nb1">The invoice_nb1 of the entity.</param>/// <param name="invoice_nb2">The invoice_nb2 of the entity.</param>/// <param name="post_dt">The post_dt of the entity.</param>        
        /// <returns>A Ar Xref.</returns>
        public ArXrefModel GetByInvoiceNb1InvoiceNb2PostDt(String invoice_nb1,String invoice_nb2,DateTime post_dt)
        {
            return Mapper.Map<ArXrefModel>(this.context.ar_xref_t.Where(p => p.invoice_nb1 == InvoiceNb1 && p.invoice_nb2 == InvoiceNb2 && p.post_dt == PostDt));
        }
       	    public List<ArXrefModel> GetByInvoiceNb1(String invoiceNb1) {
			throw new NotImplementedException();
		}
		public List<ArXrefModel> GetByInvoiceNb2(String invoiceNb2) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArXrefModel model)
        {
            this.context.ar_xref_t.Remove(Mapper.Map<ar_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArXrefModel model)
        {
            var record = this.context.ar_xref_t.Where(p => p.invoice_nb1 == model.InvoiceNb1 && p.invoice_nb2 == model.InvoiceNb2 && p.post_dt == model.PostDt).FirstOrDefault();
            Mapper.Map<ArXrefModel, ar_xref_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArXrefModel model)
        {
            this.context.ar_xref_t.Add(Mapper.Map<ar_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}