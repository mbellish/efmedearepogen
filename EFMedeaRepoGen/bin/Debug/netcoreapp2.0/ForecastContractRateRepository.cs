
// -----------------------------------------------------------------------
// <copyright file="ForecastContractRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Contract Rate repository.
    /// </summary>
    public class ForecastContractRateRepository : IForecastContractRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastContractRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastContractRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastContractRateModel> GetAll()
        {
            return this.context.forecast_contract_rate_t.Select(Mapper.Map<ForecastContractRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastContractRate by ForecastContractRateId
        /// </summary>
		/// <param name="forecast_contract_rate_id">The forecast_contract_rate_id of the entity.</param>        
        /// <returns>A Forecast Contract Rate.</returns>
        public ForecastContractRateModel GetByForecastContractRateId(Int32 forecast_contract_rate_id)
        {
            return Mapper.Map<ForecastContractRateModel>(this.context.forecast_contract_rate_t.Where(p => p.forecast_contract_rate_id == ForecastContractRateId));
        }
       	    public List<ForecastContractRateModel> GetByForecastContractLaunchId(Int32 forecastContractLaunchId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastContractRateModel model)
        {
            this.context.forecast_contract_rate_t.Remove(Mapper.Map<forecast_contract_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastContractRateModel model)
        {
            var record = this.context.forecast_contract_rate_t.Where(p => p.forecast_contract_rate_id == model.ForecastContractRateId).FirstOrDefault();
            Mapper.Map<ForecastContractRateModel, forecast_contract_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastContractRateModel model)
        {
            this.context.forecast_contract_rate_t.Add(Mapper.Map<forecast_contract_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}