
// -----------------------------------------------------------------------
// <copyright file="SysRateOverrideRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Rate Override repository.
    /// </summary>
    public class SysRateOverrideRepository : ISysRateOverrideRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysRateOverrideRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysRateOverrideRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysRateOverrideModel> GetAll()
        {
            return this.context.sys_rate_override_t.Select(Mapper.Map<SysRateOverrideModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysRateOverride by ContractNb SysId StartDt SubCd
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A Sys Rate Override.</returns>
        public SysRateOverrideModel GetByContractNbSysIdStartDtSubCd(String contract_nb,String sys_id,DateTime start_dt,String sub_cd)
        {
            return Mapper.Map<SysRateOverrideModel>(this.context.sys_rate_override_t.Where(p => p.contract_nb == ContractNb && p.sys_id == SysId && p.start_dt == StartDt && p.sub_cd == SubCd));
        }
       	    public List<SysRateOverrideModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<SysRateOverrideModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysRateOverrideModel model)
        {
            this.context.sys_rate_override_t.Remove(Mapper.Map<sys_rate_override_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysRateOverrideModel model)
        {
            var record = this.context.sys_rate_override_t.Where(p => p.contract_nb == model.ContractNb && p.sys_id == model.SysId && p.start_dt == model.StartDt && p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<SysRateOverrideModel, sys_rate_override_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysRateOverrideModel model)
        {
            this.context.sys_rate_override_t.Add(Mapper.Map<sys_rate_override_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}