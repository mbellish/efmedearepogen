
// -----------------------------------------------------------------------
// <copyright file="JobInvoiceCriteriaChoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Invoice Criteria Choice repository.
    /// </summary>
    public class JobInvoiceCriteriaChoiceRepository : IJobInvoiceCriteriaChoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobInvoiceCriteriaChoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobInvoiceCriteriaChoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobInvoiceCriteriaChoiceModel> GetAll()
        {
            return this.context.job_invoice_criteria_choice_t.Select(Mapper.Map<JobInvoiceCriteriaChoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a JobInvoiceCriteriaChoice by JobId
        /// </summary>
		/// <param name="job_id">The job_id of the entity.</param>        
        /// <returns>A Job Invoice Criteria Choice.</returns>
        public JobInvoiceCriteriaChoiceModel GetByJobId(Int32 job_id)
        {
            return Mapper.Map<JobInvoiceCriteriaChoiceModel>(this.context.job_invoice_criteria_choice_t.Where(p => p.job_id == JobId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobInvoiceCriteriaChoiceModel model)
        {
            this.context.job_invoice_criteria_choice_t.Remove(Mapper.Map<job_invoice_criteria_choice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobInvoiceCriteriaChoiceModel model)
        {
            var record = this.context.job_invoice_criteria_choice_t.Where(p => p.job_id == model.JobId).FirstOrDefault();
            Mapper.Map<JobInvoiceCriteriaChoiceModel, job_invoice_criteria_choice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobInvoiceCriteriaChoiceModel model)
        {
            this.context.job_invoice_criteria_choice_t.Add(Mapper.Map<job_invoice_criteria_choice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}