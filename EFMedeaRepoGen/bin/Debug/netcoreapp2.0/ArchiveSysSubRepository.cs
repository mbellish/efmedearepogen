
// -----------------------------------------------------------------------
// <copyright file="ArchiveSysSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Archive Sys Sub repository.
    /// </summary>
    public class ArchiveSysSubRepository : IArchiveSysSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArchiveSysSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArchiveSysSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArchiveSysSubModel> GetAll()
        {
            return this.context.archive_sys_sub_t.Select(Mapper.Map<ArchiveSysSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArchiveSysSub by CloseMonth SysId SubDt
        /// </summary>
		/// <param name="close_month">The close_month of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>        
        /// <returns>A Archive Sys Sub.</returns>
        public ArchiveSysSubModel GetByCloseMonthSysIdSubDt(DateTime close_month,String sys_id,DateTime sub_dt)
        {
            return Mapper.Map<ArchiveSysSubModel>(this.context.archive_sys_sub_t.Where(p => p.close_month == CloseMonth && p.sys_id == SysId && p.sub_dt == SubDt));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArchiveSysSubModel model)
        {
            this.context.archive_sys_sub_t.Remove(Mapper.Map<archive_sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArchiveSysSubModel model)
        {
            var record = this.context.archive_sys_sub_t.Where(p => p.close_month == model.CloseMonth && p.sys_id == model.SysId && p.sub_dt == model.SubDt).FirstOrDefault();
            Mapper.Map<ArchiveSysSubModel, archive_sys_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArchiveSysSubModel model)
        {
            this.context.archive_sys_sub_t.Add(Mapper.Map<archive_sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}