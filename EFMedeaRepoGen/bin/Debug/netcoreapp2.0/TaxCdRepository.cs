
// -----------------------------------------------------------------------
// <copyright file="TaxCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Tax Cd repository.
    /// </summary>
    public class TaxCdRepository : ITaxCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaxCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public TaxCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<TaxCdModel> GetAll()
        {
            return this.context.tax_cd_t.Select(Mapper.Map<TaxCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a TaxCd by TaxCd
        /// </summary>
		/// <param name="tax_cd">The tax_cd of the entity.</param>        
        /// <returns>A Tax Cd.</returns>
        public TaxCdModel GetByTaxCd(String tax_cd)
        {
            return Mapper.Map<TaxCdModel>(this.context.tax_cd_t.Where(p => p.tax_cd == TaxCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(TaxCdModel model)
        {
            this.context.tax_cd_t.Remove(Mapper.Map<tax_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(TaxCdModel model)
        {
            var record = this.context.tax_cd_t.Where(p => p.tax_cd == model.TaxCd).FirstOrDefault();
            Mapper.Map<TaxCdModel, tax_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(TaxCdModel model)
        {
            this.context.tax_cd_t.Add(Mapper.Map<tax_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}