
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomGroupingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Grouping repository.
    /// </summary>
    public class ArgoreportCustomGroupingRepository : IArgoreportCustomGroupingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomGroupingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomGroupingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomGroupingModel> GetAll()
        {
            return this.context.argoreport_custom_grouping_t.Select(Mapper.Map<ArgoreportCustomGroupingModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomGrouping by ArgoreportCustomId GroupNb ColNm
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="group_nb">The group_nb of the entity.</param>/// <param name="col_nm">The col_nm of the entity.</param>        
        /// <returns>A Argoreport Custom Grouping.</returns>
        public ArgoreportCustomGroupingModel GetByArgoreportCustomIdGroupNbColNm(String argoreport_custom_id,Int32 group_nb,String col_nm)
        {
            return Mapper.Map<ArgoreportCustomGroupingModel>(this.context.argoreport_custom_grouping_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.group_nb == GroupNb && p.col_nm == ColNm));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomGroupingModel model)
        {
            this.context.argoreport_custom_grouping_t.Remove(Mapper.Map<argoreport_custom_grouping_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomGroupingModel model)
        {
            var record = this.context.argoreport_custom_grouping_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.group_nb == model.GroupNb && p.col_nm == model.ColNm).FirstOrDefault();
            Mapper.Map<ArgoreportCustomGroupingModel, argoreport_custom_grouping_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomGroupingModel model)
        {
            this.context.argoreport_custom_grouping_t.Add(Mapper.Map<argoreport_custom_grouping_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}