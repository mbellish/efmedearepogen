
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomColumnDefinitionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Column Definition repository.
    /// </summary>
    public class ArgoreportCustomColumnDefinitionRepository : IArgoreportCustomColumnDefinitionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomColumnDefinitionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomColumnDefinitionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomColumnDefinitionModel> GetAll()
        {
            return this.context.argoreport_custom_column_definition_t.Select(Mapper.Map<ArgoreportCustomColumnDefinitionModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomColumnDefinition by ArgoreportCustomId ColNm OrderNb
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="col_nm">The col_nm of the entity.</param>/// <param name="order_nb">The order_nb of the entity.</param>        
        /// <returns>A Argoreport Custom Column Definition.</returns>
        public ArgoreportCustomColumnDefinitionModel GetByArgoreportCustomIdColNmOrderNb(String argoreport_custom_id,String col_nm,Int32 order_nb)
        {
            return Mapper.Map<ArgoreportCustomColumnDefinitionModel>(this.context.argoreport_custom_column_definition_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.col_nm == ColNm && p.order_nb == OrderNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomColumnDefinitionModel model)
        {
            this.context.argoreport_custom_column_definition_t.Remove(Mapper.Map<argoreport_custom_column_definition_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomColumnDefinitionModel model)
        {
            var record = this.context.argoreport_custom_column_definition_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.col_nm == model.ColNm && p.order_nb == model.OrderNb).FirstOrDefault();
            Mapper.Map<ArgoreportCustomColumnDefinitionModel, argoreport_custom_column_definition_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomColumnDefinitionModel model)
        {
            this.context.argoreport_custom_column_definition_t.Add(Mapper.Map<argoreport_custom_column_definition_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}