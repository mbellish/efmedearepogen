
// -----------------------------------------------------------------------
// <copyright file="MsoSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Sub repository.
    /// </summary>
    public class MsoSubRepository : IMsoSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoSubModel> GetAll()
        {
            return this.context.mso_sub_t.Select(Mapper.Map<MsoSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoSub by MsoNb SubDt
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>        
        /// <returns>A Mso Sub.</returns>
        public MsoSubModel GetByMsoNbSubDt(String mso_nb,DateTime sub_dt)
        {
            return Mapper.Map<MsoSubModel>(this.context.mso_sub_t.Where(p => p.mso_nb == MsoNb && p.sub_dt == SubDt));
        }
       	    public List<MsoSubModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoSubModel model)
        {
            this.context.mso_sub_t.Remove(Mapper.Map<mso_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoSubModel model)
        {
            var record = this.context.mso_sub_t.Where(p => p.mso_nb == model.MsoNb && p.sub_dt == model.SubDt).FirstOrDefault();
            Mapper.Map<MsoSubModel, mso_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoSubModel model)
        {
            this.context.mso_sub_t.Add(Mapper.Map<mso_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}