
// -----------------------------------------------------------------------
// <copyright file="SysSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Sub repository.
    /// </summary>
    public class SysSubRepository : ISysSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSubModel> GetAll()
        {
            return this.context.sys_sub_t.Select(Mapper.Map<SysSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSub by SysId SubDt
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>        
        /// <returns>A Sys Sub.</returns>
        public SysSubModel GetBySysIdSubDt(String sys_id,DateTime sub_dt)
        {
            return Mapper.Map<SysSubModel>(this.context.sys_sub_t.Where(p => p.sys_id == SysId && p.sub_dt == SubDt));
        }
       	    public List<SysSubModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<SysSubModel> GetByRowNb(Int32 rowNb) {
			throw new NotImplementedException();
		}
		public List<SysSubModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSubModel model)
        {
            this.context.sys_sub_t.Remove(Mapper.Map<sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSubModel model)
        {
            var record = this.context.sys_sub_t.Where(p => p.sys_id == model.SysId && p.sub_dt == model.SubDt).FirstOrDefault();
            Mapper.Map<SysSubModel, sys_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSubModel model)
        {
            this.context.sys_sub_t.Add(Mapper.Map<sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}