
// -----------------------------------------------------------------------
// <copyright file="ForecastSalesRegionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Sales Region repository.
    /// </summary>
    public class ForecastSalesRegionRepository : IForecastSalesRegionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastSalesRegionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastSalesRegionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastSalesRegionModel> GetAll()
        {
            return this.context.forecast_sales_region_t.Select(Mapper.Map<ForecastSalesRegionModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastSalesRegion by ForecastId SalesRegion
        /// </summary>
		/// <param name="forecast_id">The forecast_id of the entity.</param>/// <param name="sales_region">The sales_region of the entity.</param>        
        /// <returns>A Forecast Sales Region.</returns>
        public ForecastSalesRegionModel GetByForecastIdSalesRegion(Int32 forecast_id,String sales_region)
        {
            return Mapper.Map<ForecastSalesRegionModel>(this.context.forecast_sales_region_t.Where(p => p.forecast_id == ForecastId && p.sales_region == SalesRegion));
        }
       	    public List<ForecastSalesRegionModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastSalesRegionModel model)
        {
            this.context.forecast_sales_region_t.Remove(Mapper.Map<forecast_sales_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastSalesRegionModel model)
        {
            var record = this.context.forecast_sales_region_t.Where(p => p.forecast_id == model.ForecastId && p.sales_region == model.SalesRegion).FirstOrDefault();
            Mapper.Map<ForecastSalesRegionModel, forecast_sales_region_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastSalesRegionModel model)
        {
            this.context.forecast_sales_region_t.Add(Mapper.Map<forecast_sales_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}