
// -----------------------------------------------------------------------
// <copyright file="ContractCarriageDependencySrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Carriage Dependency Srvc repository.
    /// </summary>
    public class ContractCarriageDependencySrvcRepository : IContractCarriageDependencySrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCarriageDependencySrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCarriageDependencySrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCarriageDependencySrvcModel> GetAll()
        {
            return this.context.contract_carriage_dependency_srvc_t.Select(Mapper.Map<ContractCarriageDependencySrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCarriageDependencySrvc by ContractNb CarriageId ProgSrvcNm
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>        
        /// <returns>A Contract Carriage Dependency Srvc.</returns>
        public ContractCarriageDependencySrvcModel GetByContractNbCarriageIdProgSrvcNm(String contract_nb,String carriage_id,String prog_srvc_nm)
        {
            return Mapper.Map<ContractCarriageDependencySrvcModel>(this.context.contract_carriage_dependency_srvc_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.prog_srvc_nm == ProgSrvcNm));
        }
       	    public List<ContractCarriageDependencySrvcModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ContractCarriageDependencySrvcModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCarriageDependencySrvcModel model)
        {
            this.context.contract_carriage_dependency_srvc_t.Remove(Mapper.Map<contract_carriage_dependency_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCarriageDependencySrvcModel model)
        {
            var record = this.context.contract_carriage_dependency_srvc_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.prog_srvc_nm == model.ProgSrvcNm).FirstOrDefault();
            Mapper.Map<ContractCarriageDependencySrvcModel, contract_carriage_dependency_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCarriageDependencySrvcModel model)
        {
            this.context.contract_carriage_dependency_srvc_t.Add(Mapper.Map<contract_carriage_dependency_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}