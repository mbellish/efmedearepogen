
// -----------------------------------------------------------------------
// <copyright file="GroupCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Group Cd repository.
    /// </summary>
    public class GroupCdRepository : IGroupCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public GroupCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<GroupCdModel> GetAll()
        {
            return this.context.group_cd_t.Select(Mapper.Map<GroupCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a GroupCd by GroupCd
        /// </summary>
		/// <param name="group_cd">The group_cd of the entity.</param>        
        /// <returns>A Group Cd.</returns>
        public GroupCdModel GetByGroupCd(String group_cd)
        {
            return Mapper.Map<GroupCdModel>(this.context.group_cd_t.Where(p => p.group_cd == GroupCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(GroupCdModel model)
        {
            this.context.group_cd_t.Remove(Mapper.Map<group_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(GroupCdModel model)
        {
            var record = this.context.group_cd_t.Where(p => p.group_cd == model.GroupCd).FirstOrDefault();
            Mapper.Map<GroupCdModel, group_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(GroupCdModel model)
        {
            this.context.group_cd_t.Add(Mapper.Map<group_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}