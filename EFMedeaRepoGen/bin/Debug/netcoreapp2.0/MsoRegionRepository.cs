
// -----------------------------------------------------------------------
// <copyright file="MsoRegionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Region repository.
    /// </summary>
    public class MsoRegionRepository : IMsoRegionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoRegionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoRegionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoRegionModel> GetAll()
        {
            return this.context.mso_region_t.Select(Mapper.Map<MsoRegionModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoRegion by MsoNb MsoRegionNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="mso_region_nb">The mso_region_nb of the entity.</param>        
        /// <returns>A Mso Region.</returns>
        public MsoRegionModel GetByMsoNbMsoRegionNb(String mso_nb,String mso_region_nb)
        {
            return Mapper.Map<MsoRegionModel>(this.context.mso_region_t.Where(p => p.mso_nb == MsoNb && p.mso_region_nb == MsoRegionNb));
        }
       	    public List<MsoRegionModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<MsoRegionModel> GetByDelivCntryCd(String delivCntryCd) {
			throw new NotImplementedException();
		}
		public List<MsoRegionModel> GetByDelivSt(String delivSt) {
			throw new NotImplementedException();
		}
		public List<MsoRegionModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoRegionModel> GetByMsoNbMsoDivisionNb(String msoNb, String msoDivisionNb) {
			throw new NotImplementedException();
		}
		public List<MsoRegionModel> GetBySt(String st) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoRegionModel model)
        {
            this.context.mso_region_t.Remove(Mapper.Map<mso_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoRegionModel model)
        {
            var record = this.context.mso_region_t.Where(p => p.mso_nb == model.MsoNb && p.mso_region_nb == model.MsoRegionNb).FirstOrDefault();
            Mapper.Map<MsoRegionModel, mso_region_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoRegionModel model)
        {
            this.context.mso_region_t.Add(Mapper.Map<mso_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}