
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecTaxCalcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Tax Calc repository.
    /// </summary>
    public class InvoiceRecTaxCalcRepository : IInvoiceRecTaxCalcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecTaxCalcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecTaxCalcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecTaxCalcModel> GetAll()
        {
            return this.context.invoice_rec_tax_calc_t.Select(Mapper.Map<InvoiceRecTaxCalcModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecTaxCalc by TaxInvoiceNb InvoiceRecNb
        /// </summary>
		/// <param name="tax_invoice_nb">The tax_invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>        
        /// <returns>A Invoice Rec Tax Calc.</returns>
        public InvoiceRecTaxCalcModel GetByTaxInvoiceNbInvoiceRecNb(String tax_invoice_nb,String invoice_rec_nb)
        {
            return Mapper.Map<InvoiceRecTaxCalcModel>(this.context.invoice_rec_tax_calc_t.Where(p => p.tax_invoice_nb == TaxInvoiceNb && p.invoice_rec_nb == InvoiceRecNb));
        }
       	    public List<InvoiceRecTaxCalcModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecTaxCalcModel model)
        {
            this.context.invoice_rec_tax_calc_t.Remove(Mapper.Map<invoice_rec_tax_calc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecTaxCalcModel model)
        {
            var record = this.context.invoice_rec_tax_calc_t.Where(p => p.tax_invoice_nb == model.TaxInvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb).FirstOrDefault();
            Mapper.Map<InvoiceRecTaxCalcModel, invoice_rec_tax_calc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecTaxCalcModel model)
        {
            this.context.invoice_rec_tax_calc_t.Add(Mapper.Map<invoice_rec_tax_calc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}