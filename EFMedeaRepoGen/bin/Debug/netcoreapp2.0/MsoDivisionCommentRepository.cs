
// -----------------------------------------------------------------------
// <copyright file="MsoDivisionCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Division Comment repository.
    /// </summary>
    public class MsoDivisionCommentRepository : IMsoDivisionCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDivisionCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDivisionCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDivisionCommentModel> GetAll()
        {
            return this.context.mso_division_comment_t.Select(Mapper.Map<MsoDivisionCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDivisionComment by CommentNb MsoNb DivisionNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="division_nb">The division_nb of the entity.</param>        
        /// <returns>A Mso Division Comment.</returns>
        public MsoDivisionCommentModel GetByCommentNbMsoNbDivisionNb(String comment_nb,String mso_nb,String division_nb)
        {
            return Mapper.Map<MsoDivisionCommentModel>(this.context.mso_division_comment_t.Where(p => p.comment_nb == CommentNb && p.mso_nb == MsoNb && p.division_nb == DivisionNb));
        }
       	    public List<MsoDivisionCommentModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoDivisionCommentModel> GetByMsoNbDivisionNb(String msoNb, String divisionNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDivisionCommentModel model)
        {
            this.context.mso_division_comment_t.Remove(Mapper.Map<mso_division_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDivisionCommentModel model)
        {
            var record = this.context.mso_division_comment_t.Where(p => p.comment_nb == model.CommentNb && p.mso_nb == model.MsoNb && p.division_nb == model.DivisionNb).FirstOrDefault();
            Mapper.Map<MsoDivisionCommentModel, mso_division_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDivisionCommentModel model)
        {
            this.context.mso_division_comment_t.Add(Mapper.Map<mso_division_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}