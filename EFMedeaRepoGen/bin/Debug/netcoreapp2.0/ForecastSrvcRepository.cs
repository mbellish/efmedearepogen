
// -----------------------------------------------------------------------
// <copyright file="ForecastSrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Srvc repository.
    /// </summary>
    public class ForecastSrvcRepository : IForecastSrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastSrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastSrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastSrvcModel> GetAll()
        {
            return this.context.forecast_srvc_t.Select(Mapper.Map<ForecastSrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastSrvc by ForecastId ProgSrvcNm
        /// </summary>
		/// <param name="forecast_id">The forecast_id of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>        
        /// <returns>A Forecast Srvc.</returns>
        public ForecastSrvcModel GetByForecastIdProgSrvcNm(Int32 forecast_id,String prog_srvc_nm)
        {
            return Mapper.Map<ForecastSrvcModel>(this.context.forecast_srvc_t.Where(p => p.forecast_id == ForecastId && p.prog_srvc_nm == ProgSrvcNm));
        }
       	    public List<ForecastSrvcModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastSrvcModel model)
        {
            this.context.forecast_srvc_t.Remove(Mapper.Map<forecast_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastSrvcModel model)
        {
            var record = this.context.forecast_srvc_t.Where(p => p.forecast_id == model.ForecastId && p.prog_srvc_nm == model.ProgSrvcNm).FirstOrDefault();
            Mapper.Map<ForecastSrvcModel, forecast_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastSrvcModel model)
        {
            this.context.forecast_srvc_t.Add(Mapper.Map<forecast_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}