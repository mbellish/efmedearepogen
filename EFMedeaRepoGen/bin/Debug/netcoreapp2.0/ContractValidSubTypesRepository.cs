
// -----------------------------------------------------------------------
// <copyright file="ContractValidSubTypesRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Valid Sub Types repository.
    /// </summary>
    public class ContractValidSubTypesRepository : IContractValidSubTypesRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractValidSubTypesRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractValidSubTypesRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractValidSubTypesModel> GetAll()
        {
            return this.context.contract_valid_sub_types_t.Select(Mapper.Map<ContractValidSubTypesModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractValidSubTypes by RowId
        /// </summary>
		/// <param name="row_id">The row_id of the entity.</param>        
        /// <returns>A Contract Valid Sub Types.</returns>
        public ContractValidSubTypesModel GetByRowId(Int32 row_id)
        {
            return Mapper.Map<ContractValidSubTypesModel>(this.context.contract_valid_sub_types_t.Where(p => p.row_id == RowId));
        }
       	    public List<ContractValidSubTypesModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public ContractValidSubTypesModel GetByContractNbSubCd(String contractNb, String subCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractValidSubTypesModel model)
        {
            this.context.contract_valid_sub_types_t.Remove(Mapper.Map<contract_valid_sub_types_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractValidSubTypesModel model)
        {
            var record = this.context.contract_valid_sub_types_t.Where(p => p.row_id == model.RowId).FirstOrDefault();
            Mapper.Map<ContractValidSubTypesModel, contract_valid_sub_types_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractValidSubTypesModel model)
        {
            this.context.contract_valid_sub_types_t.Add(Mapper.Map<contract_valid_sub_types_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}