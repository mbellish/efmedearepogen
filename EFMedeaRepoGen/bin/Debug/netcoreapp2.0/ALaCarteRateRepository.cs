
// -----------------------------------------------------------------------
// <copyright file="ALaCarteRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The A La Carte Rate repository.
    /// </summary>
    public class ALaCarteRateRepository : IALaCarteRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ALaCarteRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ALaCarteRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ALaCarteRateModel> GetAll()
        {
            return this.context.a_la_carte_rate_t.Select(Mapper.Map<ALaCarteRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a ALaCarteRate by StartDt ContractNb StartRetailRate MaxPenPercent CarriageId SubCd
        /// </summary>
		/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="start_retail_rate">The start_retail_rate of the entity.</param>/// <param name="max_pen_percent">The max_pen_percent of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A A La Carte Rate.</returns>
        public ALaCarteRateModel GetByStartDtContractNbStartRetailRateMaxPenPercentCarriageIdSubCd(DateTime start_dt,String contract_nb,Decimal start_retail_rate,Double max_pen_percent,String carriage_id,String sub_cd)
        {
            return Mapper.Map<ALaCarteRateModel>(this.context.a_la_carte_rate_t.Where(p => p.start_dt == StartDt && p.contract_nb == ContractNb && p.start_retail_rate == StartRetailRate && p.max_pen_percent == MaxPenPercent && p.carriage_id == CarriageId && p.sub_cd == SubCd));
        }
       	    public List<ALaCarteRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ALaCarteRateModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		public ALaCarteRateModel GetByStartDtContractNbStartRetailRateCarriageIdSubCdMaxPenPercent(DateTime startDt, String contractNb, Decimal startRetailRate, String carriageID, String subCd, Single maxPenPercent) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ALaCarteRateModel model)
        {
            this.context.a_la_carte_rate_t.Remove(Mapper.Map<a_la_carte_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ALaCarteRateModel model)
        {
            var record = this.context.a_la_carte_rate_t.Where(p => p.start_dt == model.StartDt && p.contract_nb == model.ContractNb && p.start_retail_rate == model.StartRetailRate && p.max_pen_percent == model.MaxPenPercent && p.carriage_id == model.CarriageId && p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<ALaCarteRateModel, a_la_carte_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ALaCarteRateModel model)
        {
            this.context.a_la_carte_rate_t.Add(Mapper.Map<a_la_carte_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}