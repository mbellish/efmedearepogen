
// -----------------------------------------------------------------------
// <copyright file="CommitmentCdDropDownRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Commitment Cd Drop Down repository.
    /// </summary>
    public class CommitmentCdDropDownRepository : ICommitmentCdDropDownRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitmentCdDropDownRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommitmentCdDropDownRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommitmentCdDropDownModel> GetAll()
        {
            return this.context.commitment_cd_drop_down_t.Select(Mapper.Map<CommitmentCdDropDownModel>).ToList();
        }

		        /// <summary>
        /// Gets a CommitmentCdDropDown by CommitmentCdDropDownId
        /// </summary>
		/// <param name="commitment_cd_drop_down_id">The commitment_cd_drop_down_id of the entity.</param>        
        /// <returns>A Commitment Cd Drop Down.</returns>
        public CommitmentCdDropDownModel GetByCommitmentCdDropDownId(Int32 commitment_cd_drop_down_id)
        {
            return Mapper.Map<CommitmentCdDropDownModel>(this.context.commitment_cd_drop_down_t.Where(p => p.commitment_cd_drop_down_id == CommitmentCdDropDownId));
        }
       	    public List<CommitmentCdDropDownModel> GetByCommitmentCd(String commitmentCd) {
			throw new NotImplementedException();
		}
		public CommitmentCdDropDownModel GetByCommitmentCdDropDownDesc(String commitmentCd, String dropDownDesc) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommitmentCdDropDownModel model)
        {
            this.context.commitment_cd_drop_down_t.Remove(Mapper.Map<commitment_cd_drop_down_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommitmentCdDropDownModel model)
        {
            var record = this.context.commitment_cd_drop_down_t.Where(p => p.commitment_cd_drop_down_id == model.CommitmentCdDropDownId).FirstOrDefault();
            Mapper.Map<CommitmentCdDropDownModel, commitment_cd_drop_down_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommitmentCdDropDownModel model)
        {
            this.context.commitment_cd_drop_down_t.Add(Mapper.Map<commitment_cd_drop_down_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}