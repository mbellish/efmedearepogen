
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice repository.
    /// </summary>
    public class PreviewInvoiceRepository : IPreviewInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceModel> GetAll()
        {
            return this.context.preview_invoice_t.Select(Mapper.Map<PreviewInvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoice by InvoiceNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Preview Invoice.</returns>
        public PreviewInvoiceModel GetByInvoiceNb(Int32 invoice_nb)
        {
            return Mapper.Map<PreviewInvoiceModel>(this.context.preview_invoice_t.Where(p => p.invoice_nb == InvoiceNb));
        }
       	    public IEnumerable<PreviewInvoiceModel> GetByContractNbBillingPeriodAndAccInvFl(String contractNb, DateTime billingPeriodStartDt, DateTime billingPeriodEndDt, String accInvFl) {
			throw new NotImplementedException();
		}
		public Void DeleteByInvoiceNb(PreviewInvoiceModel previewInvoiceModel) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceModel model)
        {
            this.context.preview_invoice_t.Remove(Mapper.Map<preview_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceModel model)
        {
            var record = this.context.preview_invoice_t.Where(p => p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceModel, preview_invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceModel model)
        {
            this.context.preview_invoice_t.Add(Mapper.Map<preview_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}