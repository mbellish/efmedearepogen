
// -----------------------------------------------------------------------
// <copyright file="SysRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys repository.
    /// </summary>
    public class SysRepository : ISysRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysModel> GetAll()
        {
            return this.context.sys_t.Select(Mapper.Map<SysModel>).ToList();
        }

		        /// <summary>
        /// Gets a Sys by SysId
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Sys.</returns>
        public SysModel GetBySysId(String sys_id)
        {
            return Mapper.Map<SysModel>(this.context.sys_t.Where(p => p.sys_id == SysId));
        }
       	    public List<SysModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByCommitGroupNbFromContractCommitGroupSys(String commitGroupNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByContractNbSideLetterNbFromSideLetterSys(String contractNb, String sideLetterNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByDelivCntryCd(String delivCntryCd) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByDelivSt(String delivSt) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByMsoNbDistrictNb(String msoNb, String districtNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByMsoNbDivisionNb(String msoNb, String divisionNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByMsoNbMsoRegionNb(String msoNb, String msoRegionNb) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetBySrvcArea(String srvcArea) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetBySt(String st) {
			throw new NotImplementedException();
		}
		public List<SysModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysModel model)
        {
            this.context.sys_t.Remove(Mapper.Map<sys_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysModel model)
        {
            var record = this.context.sys_t.Where(p => p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<SysModel, sys_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysModel model)
        {
            this.context.sys_t.Add(Mapper.Map<sys_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}