
// -----------------------------------------------------------------------
// <copyright file="CommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Comment repository.
    /// </summary>
    public class CommentRepository : ICommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommentModel> GetAll()
        {
            return this.context.comment_t.Select(Mapper.Map<CommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a Comment by CommentNb SysId
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Comment.</returns>
        public CommentModel GetByCommentNbSysId(String comment_nb,String sys_id)
        {
            return Mapper.Map<CommentModel>(this.context.comment_t.Where(p => p.comment_nb == CommentNb && p.sys_id == SysId));
        }
       	    public List<CommentModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommentModel model)
        {
            this.context.comment_t.Remove(Mapper.Map<comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommentModel model)
        {
            var record = this.context.comment_t.Where(p => p.comment_nb == model.CommentNb && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<CommentModel, comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommentModel model)
        {
            this.context.comment_t.Add(Mapper.Map<comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}