
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceIntlRecalcChangeRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Intl Recalc Change repository.
    /// </summary>
    public class PreviewInvoiceIntlRecalcChangeRepository : IPreviewInvoiceIntlRecalcChangeRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceIntlRecalcChangeRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceIntlRecalcChangeRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceIntlRecalcChangeModel> GetAll()
        {
            return this.context.preview_invoice_intl_recalc_change_t.Select(Mapper.Map<PreviewInvoiceIntlRecalcChangeModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceIntlRecalcChange by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Preview Invoice Intl Recalc Change.</returns>
        public PreviewInvoiceIntlRecalcChangeModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<PreviewInvoiceIntlRecalcChangeModel>(this.context.preview_invoice_intl_recalc_change_t.Where(p => p.row_nb == RowNb));
        }
       	    public List<PreviewInvoiceIntlRecalcChangeModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceIntlRecalcChangeModel model)
        {
            this.context.preview_invoice_intl_recalc_change_t.Remove(Mapper.Map<preview_invoice_intl_recalc_change_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceIntlRecalcChangeModel model)
        {
            var record = this.context.preview_invoice_intl_recalc_change_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceIntlRecalcChangeModel, preview_invoice_intl_recalc_change_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceIntlRecalcChangeModel model)
        {
            this.context.preview_invoice_intl_recalc_change_t.Add(Mapper.Map<preview_invoice_intl_recalc_change_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}