
// -----------------------------------------------------------------------
// <copyright file="SysSubResolverHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Sub Resolver Hist repository.
    /// </summary>
    public class SysSubResolverHistRepository : ISysSubResolverHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSubResolverHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSubResolverHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSubResolverHistModel> GetAll()
        {
            return this.context.sys_sub_resolver_hist_t.Select(Mapper.Map<SysSubResolverHistModel>).ToList();
        }

			            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSubResolverHistModel model)
        {
            this.context.sys_sub_resolver_hist_t.Remove(Mapper.Map<sys_sub_resolver_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSubResolverHistModel model)
        {
            var record = this.context.sys_sub_resolver_hist_t.Where(p => ).FirstOrDefault();
            Mapper.Map<SysSubResolverHistModel, sys_sub_resolver_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSubResolverHistModel model)
        {
            this.context.sys_sub_resolver_hist_t.Add(Mapper.Map<sys_sub_resolver_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}