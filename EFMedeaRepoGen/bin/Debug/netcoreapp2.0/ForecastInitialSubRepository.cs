
// -----------------------------------------------------------------------
// <copyright file="ForecastInitialSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Initial Sub repository.
    /// </summary>
    public class ForecastInitialSubRepository : IForecastInitialSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastInitialSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastInitialSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastInitialSubModel> GetAll()
        {
            return this.context.forecast_initial_sub_t.Select(Mapper.Map<ForecastInitialSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastInitialSub by RowId
        /// </summary>
		/// <param name="row_id">The row_id of the entity.</param>        
        /// <returns>A Forecast Initial Sub.</returns>
        public ForecastInitialSubModel GetByRowId(Int32 row_id)
        {
            return Mapper.Map<ForecastInitialSubModel>(this.context.forecast_initial_sub_t.Where(p => p.row_id == RowId));
        }
       	    public List<ForecastInitialSubModel> GetByForecastContractLaunchId(Nullable<Int32> forecastContractLaunchId) {
			throw new NotImplementedException();
		}
		public List<ForecastInitialSubModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		public List<ForecastInitialSubModel> GetByForecastLaunchId(Nullable<Int32> forecastLaunchId) {
			throw new NotImplementedException();
		}
		public List<ForecastInitialSubModel> GetByProgSrvcNm(String progSrvcNm) {
			throw new NotImplementedException();
		}
		public List<ForecastInitialSubModel> GetBySubCd(String subCd) {
			throw new NotImplementedException();
		}
		public Void DeletePriorForecastData(Int32 forecastId, String contractNb, String contractSecurityGroupCd, String userId, String progSrvcNm, String billingCustomerId, String financeRep, String cntryCd, String techCd, String salesRegion) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastInitialSubModel model)
        {
            this.context.forecast_initial_sub_t.Remove(Mapper.Map<forecast_initial_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastInitialSubModel model)
        {
            var record = this.context.forecast_initial_sub_t.Where(p => p.row_id == model.RowId).FirstOrDefault();
            Mapper.Map<ForecastInitialSubModel, forecast_initial_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastInitialSubModel model)
        {
            this.context.forecast_initial_sub_t.Add(Mapper.Map<forecast_initial_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}