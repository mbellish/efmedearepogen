
// -----------------------------------------------------------------------
// <copyright file="SatelliteFeedRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Satellite Feed repository.
    /// </summary>
    public class SatelliteFeedRepository : ISatelliteFeedRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SatelliteFeedRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SatelliteFeedRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SatelliteFeedModel> GetAll()
        {
            return this.context.satellite_feed_t.Select(Mapper.Map<SatelliteFeedModel>).ToList();
        }

		        /// <summary>
        /// Gets a SatelliteFeed by SatelliteFeedId
        /// </summary>
		/// <param name="satellite_feed_id">The satellite_feed_id of the entity.</param>        
        /// <returns>A Satellite Feed.</returns>
        public SatelliteFeedModel GetBySatelliteFeedId(Int32 satellite_feed_id)
        {
            return Mapper.Map<SatelliteFeedModel>(this.context.satellite_feed_t.Where(p => p.satellite_feed_id == SatelliteFeedId));
        }
       	    public List<SatelliteFeedModel> GetBySatelliteId(Int32 satelliteId) {
			throw new NotImplementedException();
		}
		public SatelliteFeedModel GetBySatelliteIdProgSrvcNmFeedCd(Int32 satelliteId, String progSrvcNm, String feedCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SatelliteFeedModel model)
        {
            this.context.satellite_feed_t.Remove(Mapper.Map<satellite_feed_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SatelliteFeedModel model)
        {
            var record = this.context.satellite_feed_t.Where(p => p.satellite_feed_id == model.SatelliteFeedId).FirstOrDefault();
            Mapper.Map<SatelliteFeedModel, satellite_feed_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SatelliteFeedModel model)
        {
            this.context.satellite_feed_t.Add(Mapper.Map<satellite_feed_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}