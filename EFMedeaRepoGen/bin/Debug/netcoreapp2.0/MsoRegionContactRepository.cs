
// -----------------------------------------------------------------------
// <copyright file="MsoRegionContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Region Contact repository.
    /// </summary>
    public class MsoRegionContactRepository : IMsoRegionContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoRegionContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoRegionContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoRegionContactModel> GetAll()
        {
            return this.context.mso_region_contact_t.Select(Mapper.Map<MsoRegionContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoRegionContact by ContactNb MsoNb MsoRegionNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="mso_region_nb">The mso_region_nb of the entity.</param>        
        /// <returns>A Mso Region Contact.</returns>
        public MsoRegionContactModel GetByContactNbMsoNbMsoRegionNb(String contact_nb,String mso_nb,String mso_region_nb)
        {
            return Mapper.Map<MsoRegionContactModel>(this.context.mso_region_contact_t.Where(p => p.contact_nb == ContactNb && p.mso_nb == MsoNb && p.mso_region_nb == MsoRegionNb));
        }
       	    public List<MsoRegionContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<MsoRegionContactModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoRegionContactModel> GetByMsoNbMsoRegionNb(String msoNb, String msoRegionNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoRegionContactModel model)
        {
            this.context.mso_region_contact_t.Remove(Mapper.Map<mso_region_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoRegionContactModel model)
        {
            var record = this.context.mso_region_contact_t.Where(p => p.contact_nb == model.ContactNb && p.mso_nb == model.MsoNb && p.mso_region_nb == model.MsoRegionNb).FirstOrDefault();
            Mapper.Map<MsoRegionContactModel, mso_region_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoRegionContactModel model)
        {
            this.context.mso_region_contact_t.Add(Mapper.Map<mso_region_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}