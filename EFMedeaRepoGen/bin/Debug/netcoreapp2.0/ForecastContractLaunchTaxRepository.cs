
// -----------------------------------------------------------------------
// <copyright file="ForecastContractLaunchTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Contract Launch Tax repository.
    /// </summary>
    public class ForecastContractLaunchTaxRepository : IForecastContractLaunchTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastContractLaunchTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastContractLaunchTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastContractLaunchTaxModel> GetAll()
        {
            return this.context.forecast_contract_launch_tax_t.Select(Mapper.Map<ForecastContractLaunchTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastContractLaunchTax by ForecastContractLaunchTaxId
        /// </summary>
		/// <param name="forecast_contract_launch_tax_id">The forecast_contract_launch_tax_id of the entity.</param>        
        /// <returns>A Forecast Contract Launch Tax.</returns>
        public ForecastContractLaunchTaxModel GetByForecastContractLaunchTaxId(Int32 forecast_contract_launch_tax_id)
        {
            return Mapper.Map<ForecastContractLaunchTaxModel>(this.context.forecast_contract_launch_tax_t.Where(p => p.forecast_contract_launch_tax_id == ForecastContractLaunchTaxId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastContractLaunchTaxModel model)
        {
            this.context.forecast_contract_launch_tax_t.Remove(Mapper.Map<forecast_contract_launch_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastContractLaunchTaxModel model)
        {
            var record = this.context.forecast_contract_launch_tax_t.Where(p => p.forecast_contract_launch_tax_id == model.ForecastContractLaunchTaxId).FirstOrDefault();
            Mapper.Map<ForecastContractLaunchTaxModel, forecast_contract_launch_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastContractLaunchTaxModel model)
        {
            this.context.forecast_contract_launch_tax_t.Add(Mapper.Map<forecast_contract_launch_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}