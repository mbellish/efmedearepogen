
// -----------------------------------------------------------------------
// <copyright file="EquipmentSrvcFeedTierRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Srvc Feed Tier repository.
    /// </summary>
    public class EquipmentSrvcFeedTierRepository : IEquipmentSrvcFeedTierRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentSrvcFeedTierRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentSrvcFeedTierRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentSrvcFeedTierModel> GetAll()
        {
            return this.context.equipment_srvc_feed_tier_t.Select(Mapper.Map<EquipmentSrvcFeedTierModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentSrvcFeedTier by EquipCd ProgSrvcNm FeedCd Tier
        /// </summary>
		/// <param name="equip_cd">The equip_cd of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="feed_cd">The feed_cd of the entity.</param>/// <param name="tier">The tier of the entity.</param>        
        /// <returns>A Equipment Srvc Feed Tier.</returns>
        public EquipmentSrvcFeedTierModel GetByEquipCdProgSrvcNmFeedCdTier(String equip_cd,String prog_srvc_nm,String feed_cd,Int32 tier)
        {
            return Mapper.Map<EquipmentSrvcFeedTierModel>(this.context.equipment_srvc_feed_tier_t.Where(p => p.equip_cd == EquipCd && p.prog_srvc_nm == ProgSrvcNm && p.feed_cd == FeedCd && p.tier == Tier));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentSrvcFeedTierModel model)
        {
            this.context.equipment_srvc_feed_tier_t.Remove(Mapper.Map<equipment_srvc_feed_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentSrvcFeedTierModel model)
        {
            var record = this.context.equipment_srvc_feed_tier_t.Where(p => p.equip_cd == model.EquipCd && p.prog_srvc_nm == model.ProgSrvcNm && p.feed_cd == model.FeedCd && p.tier == model.Tier).FirstOrDefault();
            Mapper.Map<EquipmentSrvcFeedTierModel, equipment_srvc_feed_tier_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentSrvcFeedTierModel model)
        {
            this.context.equipment_srvc_feed_tier_t.Add(Mapper.Map<equipment_srvc_feed_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}