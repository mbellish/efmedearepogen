
// -----------------------------------------------------------------------
// <copyright file="UserContractSecurityGroupRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User Contract Security Group repository.
    /// </summary>
    public class UserContractSecurityGroupRepository : IUserContractSecurityGroupRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserContractSecurityGroupRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserContractSecurityGroupRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserContractSecurityGroupModel> GetAll()
        {
            return this.context.user_contract_security_group_t.Select(Mapper.Map<UserContractSecurityGroupModel>).ToList();
        }

		        /// <summary>
        /// Gets a UserContractSecurityGroup by UserId ContractSecurityGroupCd
        /// </summary>
		/// <param name="user_id">The user_id of the entity.</param>/// <param name="contract_security_group_cd">The contract_security_group_cd of the entity.</param>        
        /// <returns>A User Contract Security Group.</returns>
        public UserContractSecurityGroupModel GetByUserIdContractSecurityGroupCd(String user_id,String contract_security_group_cd)
        {
            return Mapper.Map<UserContractSecurityGroupModel>(this.context.user_contract_security_group_t.Where(p => p.user_id == UserId && p.contract_security_group_cd == ContractSecurityGroupCd));
        }
       	    public List<UserContractSecurityGroupModel> GetByUserId(String userId) {
			throw new NotImplementedException();
		}
		public List<UserContractSecurityGroupModel> GetByContractSecurityGroupCd(String contractSecurityGroupCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserContractSecurityGroupModel model)
        {
            this.context.user_contract_security_group_t.Remove(Mapper.Map<user_contract_security_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserContractSecurityGroupModel model)
        {
            var record = this.context.user_contract_security_group_t.Where(p => p.user_id == model.UserId && p.contract_security_group_cd == model.ContractSecurityGroupCd).FirstOrDefault();
            Mapper.Map<UserContractSecurityGroupModel, user_contract_security_group_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserContractSecurityGroupModel model)
        {
            this.context.user_contract_security_group_t.Add(Mapper.Map<user_contract_security_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}