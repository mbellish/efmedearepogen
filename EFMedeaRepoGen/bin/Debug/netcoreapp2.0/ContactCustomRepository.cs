
// -----------------------------------------------------------------------
// <copyright file="ContactCustomRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact Custom repository.
    /// </summary>
    public class ContactCustomRepository : IContactCustomRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactCustomRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactCustomRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactCustomModel> GetAll()
        {
            return this.context.contact_custom_t.Select(Mapper.Map<ContactCustomModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactCustom by ContactNb ContactUserCd
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="contact_user_cd">The contact_user_cd of the entity.</param>        
        /// <returns>A Contact Custom.</returns>
        public ContactCustomModel GetByContactNbContactUserCd(String contact_nb,String contact_user_cd)
        {
            return Mapper.Map<ContactCustomModel>(this.context.contact_custom_t.Where(p => p.contact_nb == ContactNb && p.contact_user_cd == ContactUserCd));
        }
       	    public List<ContactCustomModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactCustomModel model)
        {
            this.context.contact_custom_t.Remove(Mapper.Map<contact_custom_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactCustomModel model)
        {
            var record = this.context.contact_custom_t.Where(p => p.contact_nb == model.ContactNb && p.contact_user_cd == model.ContactUserCd).FirstOrDefault();
            Mapper.Map<ContactCustomModel, contact_custom_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactCustomModel model)
        {
            this.context.contact_custom_t.Add(Mapper.Map<contact_custom_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}