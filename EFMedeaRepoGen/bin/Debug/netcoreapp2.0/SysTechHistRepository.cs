
// -----------------------------------------------------------------------
// <copyright file="SysTechHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Tech Hist repository.
    /// </summary>
    public class SysTechHistRepository : ISysTechHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysTechHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysTechHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysTechHistModel> GetAll()
        {
            return this.context.sys_tech_hist_t.Select(Mapper.Map<SysTechHistModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysTechHist by SysTechHistId
        /// </summary>
		/// <param name="sys_tech_hist_id">The sys_tech_hist_id of the entity.</param>        
        /// <returns>A Sys Tech Hist.</returns>
        public SysTechHistModel GetBySysTechHistId(Int32 sys_tech_hist_id)
        {
            return Mapper.Map<SysTechHistModel>(this.context.sys_tech_hist_t.Where(p => p.sys_tech_hist_id == SysTechHistId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysTechHistModel model)
        {
            this.context.sys_tech_hist_t.Remove(Mapper.Map<sys_tech_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysTechHistModel model)
        {
            var record = this.context.sys_tech_hist_t.Where(p => p.sys_tech_hist_id == model.SysTechHistId).FirstOrDefault();
            Mapper.Map<SysTechHistModel, sys_tech_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysTechHistModel model)
        {
            this.context.sys_tech_hist_t.Add(Mapper.Map<sys_tech_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}