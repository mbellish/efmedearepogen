
// -----------------------------------------------------------------------
// <copyright file="ContractCarriageUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Carriage User Def repository.
    /// </summary>
    public class ContractCarriageUserDefRepository : IContractCarriageUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCarriageUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCarriageUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCarriageUserDefModel> GetAll()
        {
            return this.context.contract_carriage_user_def_t.Select(Mapper.Map<ContractCarriageUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCarriageUserDef by ContractNb CarriageId UserDefCd
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>        
        /// <returns>A Contract Carriage User Def.</returns>
        public ContractCarriageUserDefModel GetByContractNbCarriageIdUserDefCd(String contract_nb,String carriage_id,String user_def_cd)
        {
            return Mapper.Map<ContractCarriageUserDefModel>(this.context.contract_carriage_user_def_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.user_def_cd == UserDefCd));
        }
       	    public List<ContractCarriageUserDefModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ContractCarriageUserDefModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCarriageUserDefModel model)
        {
            this.context.contract_carriage_user_def_t.Remove(Mapper.Map<contract_carriage_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCarriageUserDefModel model)
        {
            var record = this.context.contract_carriage_user_def_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.user_def_cd == model.UserDefCd).FirstOrDefault();
            Mapper.Map<ContractCarriageUserDefModel, contract_carriage_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCarriageUserDefModel model)
        {
            this.context.contract_carriage_user_def_t.Add(Mapper.Map<contract_carriage_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}