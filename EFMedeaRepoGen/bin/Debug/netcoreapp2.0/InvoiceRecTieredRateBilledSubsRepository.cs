
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecTieredRateBilledSubsRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Tiered Rate Billed Subs repository.
    /// </summary>
    public class InvoiceRecTieredRateBilledSubsRepository : IInvoiceRecTieredRateBilledSubsRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecTieredRateBilledSubsRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecTieredRateBilledSubsRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecTieredRateBilledSubsModel> GetAll()
        {
            return this.context.invoice_rec_tiered_rate_billed_subs_t.Select(Mapper.Map<InvoiceRecTieredRateBilledSubsModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecTieredRateBilledSubs by TieredRateBilledSubsId
        /// </summary>
		/// <param name="tiered_rate_billed_subs_id">The tiered_rate_billed_subs_id of the entity.</param>        
        /// <returns>A Invoice Rec Tiered Rate Billed Subs.</returns>
        public InvoiceRecTieredRateBilledSubsModel GetByTieredRateBilledSubsId(Int32 tiered_rate_billed_subs_id)
        {
            return Mapper.Map<InvoiceRecTieredRateBilledSubsModel>(this.context.invoice_rec_tiered_rate_billed_subs_t.Where(p => p.tiered_rate_billed_subs_id == TieredRateBilledSubsId));
        }
       	    public InvoiceRecTieredRateBilledSubsModel GetByInvoiceNbInvoiceRecNb(String invoiceNb, String invoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecTieredRateBilledSubsModel model)
        {
            this.context.invoice_rec_tiered_rate_billed_subs_t.Remove(Mapper.Map<invoice_rec_tiered_rate_billed_subs_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecTieredRateBilledSubsModel model)
        {
            var record = this.context.invoice_rec_tiered_rate_billed_subs_t.Where(p => p.tiered_rate_billed_subs_id == model.TieredRateBilledSubsId).FirstOrDefault();
            Mapper.Map<InvoiceRecTieredRateBilledSubsModel, invoice_rec_tiered_rate_billed_subs_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecTieredRateBilledSubsModel model)
        {
            this.context.invoice_rec_tiered_rate_billed_subs_t.Add(Mapper.Map<invoice_rec_tiered_rate_billed_subs_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}