
// -----------------------------------------------------------------------
// <copyright file="SatelliteRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Satellite repository.
    /// </summary>
    public class SatelliteRepository : ISatelliteRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SatelliteRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SatelliteRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SatelliteModel> GetAll()
        {
            return this.context.satellite_t.Select(Mapper.Map<SatelliteModel>).ToList();
        }

		        /// <summary>
        /// Gets a Satellite by SatelliteId
        /// </summary>
		/// <param name="satellite_id">The satellite_id of the entity.</param>        
        /// <returns>A Satellite.</returns>
        public SatelliteModel GetBySatelliteId(Int32 satellite_id)
        {
            return Mapper.Map<SatelliteModel>(this.context.satellite_t.Where(p => p.satellite_id == SatelliteId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SatelliteModel model)
        {
            this.context.satellite_t.Remove(Mapper.Map<satellite_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SatelliteModel model)
        {
            var record = this.context.satellite_t.Where(p => p.satellite_id == model.SatelliteId).FirstOrDefault();
            Mapper.Map<SatelliteModel, satellite_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SatelliteModel model)
        {
            this.context.satellite_t.Add(Mapper.Map<satellite_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}