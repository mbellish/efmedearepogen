
// -----------------------------------------------------------------------
// <copyright file="ViewCommitRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The View Commit repository.
    /// </summary>
    public class ViewCommitRepository : IViewCommitRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewCommitRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ViewCommitRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ViewCommitModel> GetAll()
        {
            return this.context.view_commit_t.Select(Mapper.Map<ViewCommitModel>).ToList();
        }

		        /// <summary>
        /// Gets a ViewCommit by ContractNb StartDt CommitCd SysId SubCd RowNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="commit_cd">The commit_cd of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A View Commit.</returns>
        public ViewCommitModel GetByContractNbStartDtCommitCdSysIdSubCdRowNb(String contract_nb,DateTime start_dt,String commit_cd,String sys_id,String sub_cd,Int32 row_nb)
        {
            return Mapper.Map<ViewCommitModel>(this.context.view_commit_t.Where(p => p.contract_nb == ContractNb && p.start_dt == StartDt && p.commit_cd == CommitCd && p.sys_id == SysId && p.sub_cd == SubCd && p.row_nb == RowNb));
        }
       	    public List<ViewCommitModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ViewCommitModel model)
        {
            this.context.view_commit_t.Remove(Mapper.Map<view_commit_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ViewCommitModel model)
        {
            var record = this.context.view_commit_t.Where(p => p.contract_nb == model.ContractNb && p.start_dt == model.StartDt && p.commit_cd == model.CommitCd && p.sys_id == model.SysId && p.sub_cd == model.SubCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<ViewCommitModel, view_commit_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ViewCommitModel model)
        {
            this.context.view_commit_t.Add(Mapper.Map<view_commit_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}