
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecAllocationRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Allocation repository.
    /// </summary>
    public class InvoiceRecAllocationRepository : IInvoiceRecAllocationRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecAllocationRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecAllocationRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecAllocationModel> GetAll()
        {
            return this.context.invoice_rec_allocation_t.Select(Mapper.Map<InvoiceRecAllocationModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecAllocation by RowId
        /// </summary>
		/// <param name="row_id">The row_id of the entity.</param>        
        /// <returns>A Invoice Rec Allocation.</returns>
        public InvoiceRecAllocationModel GetByRowId(Int32 row_id)
        {
            return Mapper.Map<InvoiceRecAllocationModel>(this.context.invoice_rec_allocation_t.Where(p => p.row_id == RowId));
        }
       	    public List<InvoiceRecAllocationModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceRecAllocationModel> GetByInvoiceNbInvoiceRecNb(String invoiceNb, String invoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecAllocationModel model)
        {
            this.context.invoice_rec_allocation_t.Remove(Mapper.Map<invoice_rec_allocation_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecAllocationModel model)
        {
            var record = this.context.invoice_rec_allocation_t.Where(p => p.row_id == model.RowId).FirstOrDefault();
            Mapper.Map<InvoiceRecAllocationModel, invoice_rec_allocation_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecAllocationModel model)
        {
            this.context.invoice_rec_allocation_t.Add(Mapper.Map<invoice_rec_allocation_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}