
// -----------------------------------------------------------------------
// <copyright file="MsoDistrictContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso District Contact repository.
    /// </summary>
    public class MsoDistrictContactRepository : IMsoDistrictContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDistrictContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDistrictContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDistrictContactModel> GetAll()
        {
            return this.context.mso_district_contact_t.Select(Mapper.Map<MsoDistrictContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDistrictContact by ContactNb MsoNb DistrictNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="district_nb">The district_nb of the entity.</param>        
        /// <returns>A Mso District Contact.</returns>
        public MsoDistrictContactModel GetByContactNbMsoNbDistrictNb(String contact_nb,String mso_nb,String district_nb)
        {
            return Mapper.Map<MsoDistrictContactModel>(this.context.mso_district_contact_t.Where(p => p.contact_nb == ContactNb && p.mso_nb == MsoNb && p.district_nb == DistrictNb));
        }
       	    public List<MsoDistrictContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictContactModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictContactModel> GetByMsoNbDistrictNb(String msoNb, String districtNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDistrictContactModel model)
        {
            this.context.mso_district_contact_t.Remove(Mapper.Map<mso_district_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDistrictContactModel model)
        {
            var record = this.context.mso_district_contact_t.Where(p => p.contact_nb == model.ContactNb && p.mso_nb == model.MsoNb && p.district_nb == model.DistrictNb).FirstOrDefault();
            Mapper.Map<MsoDistrictContactModel, mso_district_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDistrictContactModel model)
        {
            this.context.mso_district_contact_t.Add(Mapper.Map<mso_district_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}