
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomCriteriaRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Criteria repository.
    /// </summary>
    public class ArgoreportCustomCriteriaRepository : IArgoreportCustomCriteriaRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomCriteriaRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomCriteriaRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomCriteriaModel> GetAll()
        {
            return this.context.argoreport_custom_criteria_t.Select(Mapper.Map<ArgoreportCustomCriteriaModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomCriteria by ArgoreportCustomId ColNm
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="col_nm">The col_nm of the entity.</param>        
        /// <returns>A Argoreport Custom Criteria.</returns>
        public ArgoreportCustomCriteriaModel GetByArgoreportCustomIdColNm(String argoreport_custom_id,String col_nm)
        {
            return Mapper.Map<ArgoreportCustomCriteriaModel>(this.context.argoreport_custom_criteria_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.col_nm == ColNm));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomCriteriaModel model)
        {
            this.context.argoreport_custom_criteria_t.Remove(Mapper.Map<argoreport_custom_criteria_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomCriteriaModel model)
        {
            var record = this.context.argoreport_custom_criteria_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.col_nm == model.ColNm).FirstOrDefault();
            Mapper.Map<ArgoreportCustomCriteriaModel, argoreport_custom_criteria_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomCriteriaModel model)
        {
            this.context.argoreport_custom_criteria_t.Add(Mapper.Map<argoreport_custom_criteria_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}