
// -----------------------------------------------------------------------
// <copyright file="SysHeadendRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Headend repository.
    /// </summary>
    public class SysHeadendRepository : ISysHeadendRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysHeadendRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysHeadendRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysHeadendModel> GetAll()
        {
            return this.context.sys_headend_t.Select(Mapper.Map<SysHeadendModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysHeadend by SysId HeadendNb
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="headend_nb">The headend_nb of the entity.</param>        
        /// <returns>A Sys Headend.</returns>
        public SysHeadendModel GetBySysIdHeadendNb(String sys_id,String headend_nb)
        {
            return Mapper.Map<SysHeadendModel>(this.context.sys_headend_t.Where(p => p.sys_id == SysId && p.headend_nb == HeadendNb));
        }
       	    public List<SysHeadendModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysHeadendModel model)
        {
            this.context.sys_headend_t.Remove(Mapper.Map<sys_headend_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysHeadendModel model)
        {
            var record = this.context.sys_headend_t.Where(p => p.sys_id == model.SysId && p.headend_nb == model.HeadendNb).FirstOrDefault();
            Mapper.Map<SysHeadendModel, sys_headend_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysHeadendModel model)
        {
            this.context.sys_headend_t.Add(Mapper.Map<sys_headend_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}