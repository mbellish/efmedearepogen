
// -----------------------------------------------------------------------
// <copyright file="SmartCardTypeCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Smart Card Type Cd repository.
    /// </summary>
    public class SmartCardTypeCdRepository : ISmartCardTypeCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartCardTypeCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SmartCardTypeCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SmartCardTypeCdModel> GetAll()
        {
            return this.context.smart_card_type_cd_t.Select(Mapper.Map<SmartCardTypeCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a SmartCardTypeCd by SmartCardTypeCd
        /// </summary>
		/// <param name="smart_card_type_cd">The smart_card_type_cd of the entity.</param>        
        /// <returns>A Smart Card Type Cd.</returns>
        public SmartCardTypeCdModel GetBySmartCardTypeCd(String smart_card_type_cd)
        {
            return Mapper.Map<SmartCardTypeCdModel>(this.context.smart_card_type_cd_t.Where(p => p.smart_card_type_cd == SmartCardTypeCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SmartCardTypeCdModel model)
        {
            this.context.smart_card_type_cd_t.Remove(Mapper.Map<smart_card_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SmartCardTypeCdModel model)
        {
            var record = this.context.smart_card_type_cd_t.Where(p => p.smart_card_type_cd == model.SmartCardTypeCd).FirstOrDefault();
            Mapper.Map<SmartCardTypeCdModel, smart_card_type_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SmartCardTypeCdModel model)
        {
            this.context.smart_card_type_cd_t.Add(Mapper.Map<smart_card_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}