
// -----------------------------------------------------------------------
// <copyright file="MaxLaunchRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Max Launch Rate repository.
    /// </summary>
    public class MaxLaunchRateRepository : IMaxLaunchRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxLaunchRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MaxLaunchRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MaxLaunchRateModel> GetAll()
        {
            return this.context.max_launch_rate_t.Select(Mapper.Map<MaxLaunchRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a MaxLaunchRate by ContractNb CarriageId StartDt SubCd CommitCd SysId
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="commit_cd">The commit_cd of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Max Launch Rate.</returns>
        public MaxLaunchRateModel GetByContractNbCarriageIdStartDtSubCdCommitCdSysId(String contract_nb,String carriage_id,DateTime start_dt,String sub_cd,String commit_cd,String sys_id)
        {
            return Mapper.Map<MaxLaunchRateModel>(this.context.max_launch_rate_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.start_dt == StartDt && p.sub_cd == SubCd && p.commit_cd == CommitCd && p.sys_id == SysId));
        }
       	    public List<MaxLaunchRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MaxLaunchRateModel model)
        {
            this.context.max_launch_rate_t.Remove(Mapper.Map<max_launch_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MaxLaunchRateModel model)
        {
            var record = this.context.max_launch_rate_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.commit_cd == model.CommitCd && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<MaxLaunchRateModel, max_launch_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MaxLaunchRateModel model)
        {
            this.context.max_launch_rate_t.Add(Mapper.Map<max_launch_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}