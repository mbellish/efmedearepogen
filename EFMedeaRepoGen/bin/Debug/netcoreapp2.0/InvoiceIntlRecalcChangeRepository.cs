
// -----------------------------------------------------------------------
// <copyright file="InvoiceIntlRecalcChangeRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Intl Recalc Change repository.
    /// </summary>
    public class InvoiceIntlRecalcChangeRepository : IInvoiceIntlRecalcChangeRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceIntlRecalcChangeRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceIntlRecalcChangeRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceIntlRecalcChangeModel> GetAll()
        {
            return this.context.invoice_intl_recalc_change_t.Select(Mapper.Map<InvoiceIntlRecalcChangeModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceIntlRecalcChange by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Invoice Intl Recalc Change.</returns>
        public InvoiceIntlRecalcChangeModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<InvoiceIntlRecalcChangeModel>(this.context.invoice_intl_recalc_change_t.Where(p => p.row_nb == RowNb));
        }
       	    public List<InvoiceIntlRecalcChangeModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceIntlRecalcChangeModel model)
        {
            this.context.invoice_intl_recalc_change_t.Remove(Mapper.Map<invoice_intl_recalc_change_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceIntlRecalcChangeModel model)
        {
            var record = this.context.invoice_intl_recalc_change_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<InvoiceIntlRecalcChangeModel, invoice_intl_recalc_change_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceIntlRecalcChangeModel model)
        {
            this.context.invoice_intl_recalc_change_t.Add(Mapper.Map<invoice_intl_recalc_change_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}