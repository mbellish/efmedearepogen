
// -----------------------------------------------------------------------
// <copyright file="EquipmentBncTemplateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Bnc Template repository.
    /// </summary>
    public class EquipmentBncTemplateRepository : IEquipmentBncTemplateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentBncTemplateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentBncTemplateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentBncTemplateModel> GetAll()
        {
            return this.context.equipment_bnc_template_t.Select(Mapper.Map<EquipmentBncTemplateModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentBncTemplate by EquipmentBncTemplateId
        /// </summary>
		/// <param name="equipment_bnc_template_id">The equipment_bnc_template_id of the entity.</param>        
        /// <returns>A Equipment Bnc Template.</returns>
        public EquipmentBncTemplateModel GetByEquipmentBncTemplateId(Int32 equipment_bnc_template_id)
        {
            return Mapper.Map<EquipmentBncTemplateModel>(this.context.equipment_bnc_template_t.Where(p => p.equipment_bnc_template_id == EquipmentBncTemplateId));
        }
       	    public EquipmentBncTemplateModel GetByEquipCdTier(String equipCd, Int32 tier) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentBncTemplateModel model)
        {
            this.context.equipment_bnc_template_t.Remove(Mapper.Map<equipment_bnc_template_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentBncTemplateModel model)
        {
            var record = this.context.equipment_bnc_template_t.Where(p => p.equipment_bnc_template_id == model.EquipmentBncTemplateId).FirstOrDefault();
            Mapper.Map<EquipmentBncTemplateModel, equipment_bnc_template_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentBncTemplateModel model)
        {
            this.context.equipment_bnc_template_t.Add(Mapper.Map<equipment_bnc_template_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}