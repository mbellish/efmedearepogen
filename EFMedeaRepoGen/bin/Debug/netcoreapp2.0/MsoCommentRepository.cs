
// -----------------------------------------------------------------------
// <copyright file="MsoCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Comment repository.
    /// </summary>
    public class MsoCommentRepository : IMsoCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoCommentModel> GetAll()
        {
            return this.context.mso_comment_t.Select(Mapper.Map<MsoCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoComment by CommentNb MsoNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>        
        /// <returns>A Mso Comment.</returns>
        public MsoCommentModel GetByCommentNbMsoNb(String comment_nb,String mso_nb)
        {
            return Mapper.Map<MsoCommentModel>(this.context.mso_comment_t.Where(p => p.comment_nb == CommentNb && p.mso_nb == MsoNb));
        }
       	    public List<MsoCommentModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoCommentModel model)
        {
            this.context.mso_comment_t.Remove(Mapper.Map<mso_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoCommentModel model)
        {
            var record = this.context.mso_comment_t.Where(p => p.comment_nb == model.CommentNb && p.mso_nb == model.MsoNb).FirstOrDefault();
            Mapper.Map<MsoCommentModel, mso_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoCommentModel model)
        {
            this.context.mso_comment_t.Add(Mapper.Map<mso_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}