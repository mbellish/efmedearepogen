
// -----------------------------------------------------------------------
// <copyright file="ForecastTypeCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Type Cd repository.
    /// </summary>
    public class ForecastTypeCdRepository : IForecastTypeCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastTypeCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastTypeCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastTypeCdModel> GetAll()
        {
            return this.context.forecast_type_cd_t.Select(Mapper.Map<ForecastTypeCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastTypeCd by ForecastTypeCd
        /// </summary>
		/// <param name="forecast_type_cd">The forecast_type_cd of the entity.</param>        
        /// <returns>A Forecast Type Cd.</returns>
        public ForecastTypeCdModel GetByForecastTypeCd(String forecast_type_cd)
        {
            return Mapper.Map<ForecastTypeCdModel>(this.context.forecast_type_cd_t.Where(p => p.forecast_type_cd == ForecastTypeCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastTypeCdModel model)
        {
            this.context.forecast_type_cd_t.Remove(Mapper.Map<forecast_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastTypeCdModel model)
        {
            var record = this.context.forecast_type_cd_t.Where(p => p.forecast_type_cd == model.ForecastTypeCd).FirstOrDefault();
            Mapper.Map<ForecastTypeCdModel, forecast_type_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastTypeCdModel model)
        {
            this.context.forecast_type_cd_t.Add(Mapper.Map<forecast_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}