
// -----------------------------------------------------------------------
// <copyright file="BlendRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Blend Rate repository.
    /// </summary>
    public class BlendRateRepository : IBlendRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BlendRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BlendRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BlendRateModel> GetAll()
        {
            return this.context.blend_rate_t.Select(Mapper.Map<BlendRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a BlendRate by ContractNb CarriageId StartDt SubCd StartSubCnt
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="start_sub_cnt">The start_sub_cnt of the entity.</param>        
        /// <returns>A Blend Rate.</returns>
        public BlendRateModel GetByContractNbCarriageIdStartDtSubCdStartSubCnt(String contract_nb,String carriage_id,DateTime start_dt,String sub_cd,Decimal start_sub_cnt)
        {
            return Mapper.Map<BlendRateModel>(this.context.blend_rate_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.start_dt == StartDt && p.sub_cd == SubCd && p.start_sub_cnt == StartSubCnt));
        }
       	    public List<BlendRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<BlendRateModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BlendRateModel model)
        {
            this.context.blend_rate_t.Remove(Mapper.Map<blend_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BlendRateModel model)
        {
            var record = this.context.blend_rate_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.start_sub_cnt == model.StartSubCnt).FirstOrDefault();
            Mapper.Map<BlendRateModel, blend_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BlendRateModel model)
        {
            this.context.blend_rate_t.Add(Mapper.Map<blend_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}