
// -----------------------------------------------------------------------
// <copyright file="JobUnitRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Unit repository.
    /// </summary>
    public class JobUnitRepository : IJobUnitRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobUnitRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobUnitRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobUnitModel> GetAll()
        {
            return this.context.job_unit_t.Select(Mapper.Map<JobUnitModel>).ToList();
        }

		        /// <summary>
        /// Gets a JobUnit by JobUnitId
        /// </summary>
		/// <param name="job_unit_id">The job_unit_id of the entity.</param>        
        /// <returns>A Job Unit.</returns>
        public JobUnitModel GetByJobUnitId(Int32 job_unit_id)
        {
            return Mapper.Map<JobUnitModel>(this.context.job_unit_t.Where(p => p.job_unit_id == JobUnitId));
        }
       	    public List<JobUnitModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		public List<JobUnitModel> GetPendingJobUnits() {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobUnitModel model)
        {
            this.context.job_unit_t.Remove(Mapper.Map<job_unit_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobUnitModel model)
        {
            var record = this.context.job_unit_t.Where(p => p.job_unit_id == model.JobUnitId).FirstOrDefault();
            Mapper.Map<JobUnitModel, job_unit_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobUnitModel model)
        {
            this.context.job_unit_t.Add(Mapper.Map<job_unit_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}