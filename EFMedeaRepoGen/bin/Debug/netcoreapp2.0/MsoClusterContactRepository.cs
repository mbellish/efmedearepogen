
// -----------------------------------------------------------------------
// <copyright file="MsoClusterContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster Contact repository.
    /// </summary>
    public class MsoClusterContactRepository : IMsoClusterContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterContactModel> GetAll()
        {
            return this.context.mso_cluster_contact_t.Select(Mapper.Map<MsoClusterContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoClusterContact by ContactNb MsoNb ClusterNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>        
        /// <returns>A Mso Cluster Contact.</returns>
        public MsoClusterContactModel GetByContactNbMsoNbClusterNb(String contact_nb,String mso_nb,String cluster_nb)
        {
            return Mapper.Map<MsoClusterContactModel>(this.context.mso_cluster_contact_t.Where(p => p.contact_nb == ContactNb && p.mso_nb == MsoNb && p.cluster_nb == ClusterNb));
        }
       	    public List<MsoClusterContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterContactModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterContactModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterContactModel model)
        {
            this.context.mso_cluster_contact_t.Remove(Mapper.Map<mso_cluster_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterContactModel model)
        {
            var record = this.context.mso_cluster_contact_t.Where(p => p.contact_nb == model.ContactNb && p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb).FirstOrDefault();
            Mapper.Map<MsoClusterContactModel, mso_cluster_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterContactModel model)
        {
            this.context.mso_cluster_contact_t.Add(Mapper.Map<mso_cluster_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}