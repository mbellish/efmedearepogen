
// -----------------------------------------------------------------------
// <copyright file="CarriagePenaltyRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Carriage Penalty repository.
    /// </summary>
    public class CarriagePenaltyRepository : ICarriagePenaltyRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CarriagePenaltyRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CarriagePenaltyRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CarriagePenaltyModel> GetAll()
        {
            return this.context.carriage_penalty_t.Select(Mapper.Map<CarriagePenaltyModel>).ToList();
        }

		        /// <summary>
        /// Gets a CarriagePenalty by ContractNb CarriageId StartDt CarriagePenaltyNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="carriage_penalty_nb">The carriage_penalty_nb of the entity.</param>        
        /// <returns>A Carriage Penalty.</returns>
        public CarriagePenaltyModel GetByContractNbCarriageIdStartDtCarriagePenaltyNb(String contract_nb,String carriage_id,DateTime start_dt,String carriage_penalty_nb)
        {
            return Mapper.Map<CarriagePenaltyModel>(this.context.carriage_penalty_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.start_dt == StartDt && p.carriage_penalty_nb == CarriagePenaltyNb));
        }
       	    public List<CarriagePenaltyModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<CarriagePenaltyModel> GetByContractNbCarriageId(String contractNb, String carriageID) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CarriagePenaltyModel model)
        {
            this.context.carriage_penalty_t.Remove(Mapper.Map<carriage_penalty_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CarriagePenaltyModel model)
        {
            var record = this.context.carriage_penalty_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.start_dt == model.StartDt && p.carriage_penalty_nb == model.CarriagePenaltyNb).FirstOrDefault();
            Mapper.Map<CarriagePenaltyModel, carriage_penalty_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CarriagePenaltyModel model)
        {
            this.context.carriage_penalty_t.Add(Mapper.Map<carriage_penalty_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}