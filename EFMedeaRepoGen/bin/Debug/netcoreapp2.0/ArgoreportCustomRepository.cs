
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom repository.
    /// </summary>
    public class ArgoreportCustomRepository : IArgoreportCustomRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomModel> GetAll()
        {
            return this.context.argoreport_custom_t.Select(Mapper.Map<ArgoreportCustomModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustom by ArgoreportCustomId
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>        
        /// <returns>A Argoreport Custom.</returns>
        public ArgoreportCustomModel GetByArgoreportCustomId(String argoreport_custom_id)
        {
            return Mapper.Map<ArgoreportCustomModel>(this.context.argoreport_custom_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomModel model)
        {
            this.context.argoreport_custom_t.Remove(Mapper.Map<argoreport_custom_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomModel model)
        {
            var record = this.context.argoreport_custom_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId).FirstOrDefault();
            Mapper.Map<ArgoreportCustomModel, argoreport_custom_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomModel model)
        {
            this.context.argoreport_custom_t.Add(Mapper.Map<argoreport_custom_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}