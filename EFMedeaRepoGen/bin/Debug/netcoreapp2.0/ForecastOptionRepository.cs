
// -----------------------------------------------------------------------
// <copyright file="ForecastOptionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Option repository.
    /// </summary>
    public class ForecastOptionRepository : IForecastOptionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastOptionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastOptionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastOptionModel> GetAll()
        {
            return this.context.forecast_option_t.Select(Mapper.Map<ForecastOptionModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastOption by ForecastId
        /// </summary>
		/// <param name="forecast_id">The forecast_id of the entity.</param>        
        /// <returns>A Forecast Option.</returns>
        public ForecastOptionModel GetByForecastId(Int32 forecast_id)
        {
            return Mapper.Map<ForecastOptionModel>(this.context.forecast_option_t.Where(p => p.forecast_id == ForecastId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastOptionModel model)
        {
            this.context.forecast_option_t.Remove(Mapper.Map<forecast_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastOptionModel model)
        {
            var record = this.context.forecast_option_t.Where(p => p.forecast_id == model.ForecastId).FirstOrDefault();
            Mapper.Map<ForecastOptionModel, forecast_option_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastOptionModel model)
        {
            this.context.forecast_option_t.Add(Mapper.Map<forecast_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}