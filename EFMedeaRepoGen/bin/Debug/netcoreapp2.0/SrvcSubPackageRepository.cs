
// -----------------------------------------------------------------------
// <copyright file="SrvcSubPackageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Srvc Sub Package repository.
    /// </summary>
    public class SrvcSubPackageRepository : ISrvcSubPackageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SrvcSubPackageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SrvcSubPackageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SrvcSubPackageModel> GetAll()
        {
            return this.context.srvc_sub_package_t.Select(Mapper.Map<SrvcSubPackageModel>).ToList();
        }

		        /// <summary>
        /// Gets a SrvcSubPackage by SrvcSubPackageId
        /// </summary>
		/// <param name="srvc_sub_package_id">The srvc_sub_package_id of the entity.</param>        
        /// <returns>A Srvc Sub Package.</returns>
        public SrvcSubPackageModel GetBySrvcSubPackageId(Int32 srvc_sub_package_id)
        {
            return Mapper.Map<SrvcSubPackageModel>(this.context.srvc_sub_package_t.Where(p => p.srvc_sub_package_id == SrvcSubPackageId));
        }
       	    public SrvcSubPackageModel GetBySysIdPackageProgSrvcNmProgSrvcNmSubDtSubCd(String sysId, String packageProgSrvcNm, String progSrvcNm, DateTime subDt, String subCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SrvcSubPackageModel model)
        {
            this.context.srvc_sub_package_t.Remove(Mapper.Map<srvc_sub_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SrvcSubPackageModel model)
        {
            var record = this.context.srvc_sub_package_t.Where(p => p.srvc_sub_package_id == model.SrvcSubPackageId).FirstOrDefault();
            Mapper.Map<SrvcSubPackageModel, srvc_sub_package_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SrvcSubPackageModel model)
        {
            this.context.srvc_sub_package_t.Add(Mapper.Map<srvc_sub_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}