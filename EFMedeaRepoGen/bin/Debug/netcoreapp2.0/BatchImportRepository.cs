
// -----------------------------------------------------------------------
// <copyright file="BatchImportRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Batch Import repository.
    /// </summary>
    public class BatchImportRepository : IBatchImportRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchImportRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BatchImportRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BatchImportModel> GetAll()
        {
            return this.context.batch_import_t.Select(Mapper.Map<BatchImportModel>).ToList();
        }

		        /// <summary>
        /// Gets a BatchImport by ImportId
        /// </summary>
		/// <param name="import_id">The import_id of the entity.</param>        
        /// <returns>A Batch Import.</returns>
        public BatchImportModel GetByImportId(Int32 import_id)
        {
            return Mapper.Map<BatchImportModel>(this.context.batch_import_t.Where(p => p.import_id == ImportId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BatchImportModel model)
        {
            this.context.batch_import_t.Remove(Mapper.Map<batch_import_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BatchImportModel model)
        {
            var record = this.context.batch_import_t.Where(p => p.import_id == model.ImportId).FirstOrDefault();
            Mapper.Map<BatchImportModel, batch_import_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BatchImportModel model)
        {
            this.context.batch_import_t.Add(Mapper.Map<batch_import_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}