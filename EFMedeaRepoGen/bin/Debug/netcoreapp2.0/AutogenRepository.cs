
// -----------------------------------------------------------------------
// <copyright file="AutogenRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Autogen repository.
    /// </summary>
    public class AutogenRepository : IAutogenRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutogenRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public AutogenRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<AutogenModel> GetAll()
        {
            return this.context.autogen_t.Select(Mapper.Map<AutogenModel>).ToList();
        }

		        /// <summary>
        /// Gets a Autogen by Field
        /// </summary>
		/// <param name="field">The field of the entity.</param>        
        /// <returns>A Autogen.</returns>
        public AutogenModel GetByField(String field)
        {
            return Mapper.Map<AutogenModel>(this.context.autogen_t.Where(p => p.field == Field));
        }
       	    public String GenerateAutogenID(String field, Int32 size) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(AutogenModel model)
        {
            this.context.autogen_t.Remove(Mapper.Map<autogen_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(AutogenModel model)
        {
            var record = this.context.autogen_t.Where(p => p.field == model.Field).FirstOrDefault();
            Mapper.Map<AutogenModel, autogen_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(AutogenModel model)
        {
            this.context.autogen_t.Add(Mapper.Map<autogen_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}