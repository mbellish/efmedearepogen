
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomOptionCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Option Cd repository.
    /// </summary>
    public class ArgoreportCustomOptionCdRepository : IArgoreportCustomOptionCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomOptionCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomOptionCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomOptionCdModel> GetAll()
        {
            return this.context.argoreport_custom_option_cd_t.Select(Mapper.Map<ArgoreportCustomOptionCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomOptionCd by OptionCd
        /// </summary>
		/// <param name="option_cd">The option_cd of the entity.</param>        
        /// <returns>A Argoreport Custom Option Cd.</returns>
        public ArgoreportCustomOptionCdModel GetByOptionCd(String option_cd)
        {
            return Mapper.Map<ArgoreportCustomOptionCdModel>(this.context.argoreport_custom_option_cd_t.Where(p => p.option_cd == OptionCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomOptionCdModel model)
        {
            this.context.argoreport_custom_option_cd_t.Remove(Mapper.Map<argoreport_custom_option_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomOptionCdModel model)
        {
            var record = this.context.argoreport_custom_option_cd_t.Where(p => p.option_cd == model.OptionCd).FirstOrDefault();
            Mapper.Map<ArgoreportCustomOptionCdModel, argoreport_custom_option_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomOptionCdModel model)
        {
            this.context.argoreport_custom_option_cd_t.Add(Mapper.Map<argoreport_custom_option_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}