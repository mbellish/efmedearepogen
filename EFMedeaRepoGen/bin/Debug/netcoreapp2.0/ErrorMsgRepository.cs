
// -----------------------------------------------------------------------
// <copyright file="ErrorMsgRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Error Msg repository.
    /// </summary>
    public class ErrorMsgRepository : IErrorMsgRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMsgRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ErrorMsgRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ErrorMsgModel> GetAll()
        {
            return this.context.error_msg_t.Select(Mapper.Map<ErrorMsgModel>).ToList();
        }

		        /// <summary>
        /// Gets a ErrorMsg by ErrorNb
        /// </summary>
		/// <param name="error_nb">The error_nb of the entity.</param>        
        /// <returns>A Error Msg.</returns>
        public ErrorMsgModel GetByErrorNb(Int32 error_nb)
        {
            return Mapper.Map<ErrorMsgModel>(this.context.error_msg_t.Where(p => p.error_nb == ErrorNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ErrorMsgModel model)
        {
            this.context.error_msg_t.Remove(Mapper.Map<error_msg_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ErrorMsgModel model)
        {
            var record = this.context.error_msg_t.Where(p => p.error_nb == model.ErrorNb).FirstOrDefault();
            Mapper.Map<ErrorMsgModel, error_msg_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ErrorMsgModel model)
        {
            this.context.error_msg_t.Add(Mapper.Map<error_msg_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}