
// -----------------------------------------------------------------------
// <copyright file="ContractCommitmentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Commitment repository.
    /// </summary>
    public class ContractCommitmentRepository : IContractCommitmentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCommitmentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCommitmentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCommitmentModel> GetAll()
        {
            return this.context.contract_commitment_t.Select(Mapper.Map<ContractCommitmentModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCommitment by ContractCommitmentId
        /// </summary>
		/// <param name="contract_commitment_id">The contract_commitment_id of the entity.</param>        
        /// <returns>A Contract Commitment.</returns>
        public ContractCommitmentModel GetByContractCommitmentId(Int32 contract_commitment_id)
        {
            return Mapper.Map<ContractCommitmentModel>(this.context.contract_commitment_t.Where(p => p.contract_commitment_id == ContractCommitmentId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCommitmentModel model)
        {
            this.context.contract_commitment_t.Remove(Mapper.Map<contract_commitment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCommitmentModel model)
        {
            var record = this.context.contract_commitment_t.Where(p => p.contract_commitment_id == model.ContractCommitmentId).FirstOrDefault();
            Mapper.Map<ContractCommitmentModel, contract_commitment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCommitmentModel model)
        {
            this.context.contract_commitment_t.Add(Mapper.Map<contract_commitment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}