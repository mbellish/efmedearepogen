
// -----------------------------------------------------------------------
// <copyright file="BankCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Bank Cd repository.
    /// </summary>
    public class BankCdRepository : IBankCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BankCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BankCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BankCdModel> GetAll()
        {
            return this.context.bank_cd_t.Select(Mapper.Map<BankCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a BankCd by BankCd
        /// </summary>
		/// <param name="bank_cd">The bank_cd of the entity.</param>        
        /// <returns>A Bank Cd.</returns>
        public BankCdModel GetByBankCd(String bank_cd)
        {
            return Mapper.Map<BankCdModel>(this.context.bank_cd_t.Where(p => p.bank_cd == BankCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BankCdModel model)
        {
            this.context.bank_cd_t.Remove(Mapper.Map<bank_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BankCdModel model)
        {
            var record = this.context.bank_cd_t.Where(p => p.bank_cd == model.BankCd).FirstOrDefault();
            Mapper.Map<BankCdModel, bank_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BankCdModel model)
        {
            this.context.bank_cd_t.Add(Mapper.Map<bank_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}