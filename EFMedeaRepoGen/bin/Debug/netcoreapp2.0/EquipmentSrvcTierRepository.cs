
// -----------------------------------------------------------------------
// <copyright file="EquipmentSrvcTierRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Srvc Tier repository.
    /// </summary>
    public class EquipmentSrvcTierRepository : IEquipmentSrvcTierRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentSrvcTierRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentSrvcTierRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentSrvcTierModel> GetAll()
        {
            return this.context.equipment_srvc_tier_t.Select(Mapper.Map<EquipmentSrvcTierModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentSrvcTier by EquipCd ProgSrvcNm Tier
        /// </summary>
		/// <param name="equip_cd">The equip_cd of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="tier">The tier of the entity.</param>        
        /// <returns>A Equipment Srvc Tier.</returns>
        public EquipmentSrvcTierModel GetByEquipCdProgSrvcNmTier(String equip_cd,String prog_srvc_nm,Int32 tier)
        {
            return Mapper.Map<EquipmentSrvcTierModel>(this.context.equipment_srvc_tier_t.Where(p => p.equip_cd == EquipCd && p.prog_srvc_nm == ProgSrvcNm && p.tier == Tier));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentSrvcTierModel model)
        {
            this.context.equipment_srvc_tier_t.Remove(Mapper.Map<equipment_srvc_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentSrvcTierModel model)
        {
            var record = this.context.equipment_srvc_tier_t.Where(p => p.equip_cd == model.EquipCd && p.prog_srvc_nm == model.ProgSrvcNm && p.tier == model.Tier).FirstOrDefault();
            Mapper.Map<EquipmentSrvcTierModel, equipment_srvc_tier_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentSrvcTierModel model)
        {
            this.context.equipment_srvc_tier_t.Add(Mapper.Map<equipment_srvc_tier_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}