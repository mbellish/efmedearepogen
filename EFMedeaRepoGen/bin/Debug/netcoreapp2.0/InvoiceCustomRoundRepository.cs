
// -----------------------------------------------------------------------
// <copyright file="InvoiceCustomRoundRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Custom Round repository.
    /// </summary>
    public class InvoiceCustomRoundRepository : IInvoiceCustomRoundRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceCustomRoundRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceCustomRoundRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceCustomRoundModel> GetAll()
        {
            return this.context.invoice_custom_round_t.Select(Mapper.Map<InvoiceCustomRoundModel>).ToList();
        }

			            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceCustomRoundModel model)
        {
            this.context.invoice_custom_round_t.Remove(Mapper.Map<invoice_custom_round_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceCustomRoundModel model)
        {
            var record = this.context.invoice_custom_round_t.Where(p => ).FirstOrDefault();
            Mapper.Map<InvoiceCustomRoundModel, invoice_custom_round_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceCustomRoundModel model)
        {
            this.context.invoice_custom_round_t.Add(Mapper.Map<invoice_custom_round_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}