
// -----------------------------------------------------------------------
// <copyright file="ForecastSubGrowthRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Sub Growth repository.
    /// </summary>
    public class ForecastSubGrowthRepository : IForecastSubGrowthRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastSubGrowthRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastSubGrowthRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastSubGrowthModel> GetAll()
        {
            return this.context.forecast_sub_growth_t.Select(Mapper.Map<ForecastSubGrowthModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastSubGrowth by ForecastSubGrowthId
        /// </summary>
		/// <param name="forecast_sub_growth_id">The forecast_sub_growth_id of the entity.</param>        
        /// <returns>A Forecast Sub Growth.</returns>
        public ForecastSubGrowthModel GetByForecastSubGrowthId(Int32 forecast_sub_growth_id)
        {
            return Mapper.Map<ForecastSubGrowthModel>(this.context.forecast_sub_growth_t.Where(p => p.forecast_sub_growth_id == ForecastSubGrowthId));
        }
       	    public List<ForecastSubGrowthModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<ForecastSubGrowthModel> GetByForecastGrowthParameterCd(String forecastGrowthParameterCd) {
			throw new NotImplementedException();
		}
		public List<ForecastSubGrowthModel> GetByForecastGrowthSpanId(Int32 forecastGrowthSpanId) {
			throw new NotImplementedException();
		}
		public List<ForecastSubGrowthModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastSubGrowthModel model)
        {
            this.context.forecast_sub_growth_t.Remove(Mapper.Map<forecast_sub_growth_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastSubGrowthModel model)
        {
            var record = this.context.forecast_sub_growth_t.Where(p => p.forecast_sub_growth_id == model.ForecastSubGrowthId).FirstOrDefault();
            Mapper.Map<ForecastSubGrowthModel, forecast_sub_growth_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastSubGrowthModel model)
        {
            this.context.forecast_sub_growth_t.Add(Mapper.Map<forecast_sub_growth_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}