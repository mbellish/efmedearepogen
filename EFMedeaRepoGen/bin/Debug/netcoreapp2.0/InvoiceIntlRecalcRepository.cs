
// -----------------------------------------------------------------------
// <copyright file="InvoiceIntlRecalcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Intl Recalc repository.
    /// </summary>
    public class InvoiceIntlRecalcRepository : IInvoiceIntlRecalcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceIntlRecalcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceIntlRecalcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceIntlRecalcModel> GetAll()
        {
            return this.context.invoice_intl_recalc_t.Select(Mapper.Map<InvoiceIntlRecalcModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceIntlRecalc by ContractNb InvoiceNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Invoice Intl Recalc.</returns>
        public InvoiceIntlRecalcModel GetByContractNbInvoiceNb(String contract_nb,String invoice_nb)
        {
            return Mapper.Map<InvoiceIntlRecalcModel>(this.context.invoice_intl_recalc_t.Where(p => p.contract_nb == ContractNb && p.invoice_nb == InvoiceNb));
        }
       	    public List<InvoiceIntlRecalcModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceIntlRecalcModel> GetByProcessFl(String processFl) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceIntlRecalcModel model)
        {
            this.context.invoice_intl_recalc_t.Remove(Mapper.Map<invoice_intl_recalc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceIntlRecalcModel model)
        {
            var record = this.context.invoice_intl_recalc_t.Where(p => p.contract_nb == model.ContractNb && p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<InvoiceIntlRecalcModel, invoice_intl_recalc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceIntlRecalcModel model)
        {
            this.context.invoice_intl_recalc_t.Add(Mapper.Map<invoice_intl_recalc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}