
// -----------------------------------------------------------------------
// <copyright file="PotentialAdjustCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Potential Adjust Cd repository.
    /// </summary>
    public class PotentialAdjustCdRepository : IPotentialAdjustCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PotentialAdjustCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PotentialAdjustCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PotentialAdjustCdModel> GetAll()
        {
            return this.context.potential_adjust_cd_t.Select(Mapper.Map<PotentialAdjustCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a PotentialAdjustCd by PotentialAdjustmentCd
        /// </summary>
		/// <param name="potential_adjustment_cd">The potential_adjustment_cd of the entity.</param>        
        /// <returns>A Potential Adjust Cd.</returns>
        public PotentialAdjustCdModel GetByPotentialAdjustmentCd(String potential_adjustment_cd)
        {
            return Mapper.Map<PotentialAdjustCdModel>(this.context.potential_adjust_cd_t.Where(p => p.potential_adjustment_cd == PotentialAdjustmentCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PotentialAdjustCdModel model)
        {
            this.context.potential_adjust_cd_t.Remove(Mapper.Map<potential_adjust_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PotentialAdjustCdModel model)
        {
            var record = this.context.potential_adjust_cd_t.Where(p => p.potential_adjustment_cd == model.PotentialAdjustmentCd).FirstOrDefault();
            Mapper.Map<PotentialAdjustCdModel, potential_adjust_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PotentialAdjustCdModel model)
        {
            this.context.potential_adjust_cd_t.Add(Mapper.Map<potential_adjust_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}