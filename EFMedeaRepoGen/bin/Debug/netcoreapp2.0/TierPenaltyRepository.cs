
// -----------------------------------------------------------------------
// <copyright file="TierPenaltyRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Tier Penalty repository.
    /// </summary>
    public class TierPenaltyRepository : ITierPenaltyRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TierPenaltyRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public TierPenaltyRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<TierPenaltyModel> GetAll()
        {
            return this.context.tier_penalty_t.Select(Mapper.Map<TierPenaltyModel>).ToList();
        }

		        /// <summary>
        /// Gets a TierPenalty by ContractNb CarriageId StartDt SubCd PenetratePercent
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="penetrate_percent">The penetrate_percent of the entity.</param>        
        /// <returns>A Tier Penalty.</returns>
        public TierPenaltyModel GetByContractNbCarriageIdStartDtSubCdPenetratePercent(String contract_nb,String carriage_id,DateTime start_dt,String sub_cd,Double penetrate_percent)
        {
            return Mapper.Map<TierPenaltyModel>(this.context.tier_penalty_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.start_dt == StartDt && p.sub_cd == SubCd && p.penetrate_percent == PenetratePercent));
        }
       	    public List<TierPenaltyModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<TierPenaltyModel> GetByContractNbCarriageId(String contractNb, String carriageID) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(TierPenaltyModel model)
        {
            this.context.tier_penalty_t.Remove(Mapper.Map<tier_penalty_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(TierPenaltyModel model)
        {
            var record = this.context.tier_penalty_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.penetrate_percent == model.PenetratePercent).FirstOrDefault();
            Mapper.Map<TierPenaltyModel, tier_penalty_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(TierPenaltyModel model)
        {
            this.context.tier_penalty_t.Add(Mapper.Map<tier_penalty_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}