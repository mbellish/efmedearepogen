
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceErrorRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Error repository.
    /// </summary>
    public class PreviewInvoiceErrorRepository : IPreviewInvoiceErrorRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceErrorRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceErrorRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceErrorModel> GetAll()
        {
            return this.context.preview_invoice_error_t.Select(Mapper.Map<PreviewInvoiceErrorModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceError by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Preview Invoice Error.</returns>
        public PreviewInvoiceErrorModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<PreviewInvoiceErrorModel>(this.context.preview_invoice_error_t.Where(p => p.row_nb == RowNb));
        }
       	    public List<PreviewInvoiceErrorModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceErrorModel model)
        {
            this.context.preview_invoice_error_t.Remove(Mapper.Map<preview_invoice_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceErrorModel model)
        {
            var record = this.context.preview_invoice_error_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceErrorModel, preview_invoice_error_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceErrorModel model)
        {
            this.context.preview_invoice_error_t.Add(Mapper.Map<preview_invoice_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}