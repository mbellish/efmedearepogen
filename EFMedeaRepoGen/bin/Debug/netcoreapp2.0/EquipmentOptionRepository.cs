
// -----------------------------------------------------------------------
// <copyright file="EquipmentOptionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment Option repository.
    /// </summary>
    public class EquipmentOptionRepository : IEquipmentOptionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentOptionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentOptionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentOptionModel> GetAll()
        {
            return this.context.equipment_option_t.Select(Mapper.Map<EquipmentOptionModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentOption by EquipmentOptionId
        /// </summary>
		/// <param name="equipment_option_id">The equipment_option_id of the entity.</param>        
        /// <returns>A Equipment Option.</returns>
        public EquipmentOptionModel GetByEquipmentOptionId(Int32 equipment_option_id)
        {
            return Mapper.Map<EquipmentOptionModel>(this.context.equipment_option_t.Where(p => p.equipment_option_id == EquipmentOptionId));
        }
       	    public EquipmentOptionModel Get() {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentOptionModel model)
        {
            this.context.equipment_option_t.Remove(Mapper.Map<equipment_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentOptionModel model)
        {
            var record = this.context.equipment_option_t.Where(p => p.equipment_option_id == model.EquipmentOptionId).FirstOrDefault();
            Mapper.Map<EquipmentOptionModel, equipment_option_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentOptionModel model)
        {
            this.context.equipment_option_t.Add(Mapper.Map<equipment_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}