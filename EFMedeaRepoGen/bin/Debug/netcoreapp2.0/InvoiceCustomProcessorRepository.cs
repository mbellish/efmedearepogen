
// -----------------------------------------------------------------------
// <copyright file="InvoiceCustomProcessorRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Custom Processor repository.
    /// </summary>
    public class InvoiceCustomProcessorRepository : IInvoiceCustomProcessorRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceCustomProcessorRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceCustomProcessorRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceCustomProcessorModel> GetAll()
        {
            return this.context.invoice_custom_processor_t.Select(Mapper.Map<InvoiceCustomProcessorModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceCustomProcessor by ProcessorNb
        /// </summary>
		/// <param name="processor_nb">The processor_nb of the entity.</param>        
        /// <returns>A Invoice Custom Processor.</returns>
        public InvoiceCustomProcessorModel GetByProcessorNb(Int32 processor_nb)
        {
            return Mapper.Map<InvoiceCustomProcessorModel>(this.context.invoice_custom_processor_t.Where(p => p.processor_nb == ProcessorNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceCustomProcessorModel model)
        {
            this.context.invoice_custom_processor_t.Remove(Mapper.Map<invoice_custom_processor_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceCustomProcessorModel model)
        {
            var record = this.context.invoice_custom_processor_t.Where(p => p.processor_nb == model.ProcessorNb).FirstOrDefault();
            Mapper.Map<InvoiceCustomProcessorModel, invoice_custom_processor_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceCustomProcessorModel model)
        {
            this.context.invoice_custom_processor_t.Add(Mapper.Map<invoice_custom_processor_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}