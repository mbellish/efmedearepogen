
// -----------------------------------------------------------------------
// <copyright file="SmartCardStatusCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Smart Card Status Cd repository.
    /// </summary>
    public class SmartCardStatusCdRepository : ISmartCardStatusCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartCardStatusCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SmartCardStatusCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SmartCardStatusCdModel> GetAll()
        {
            return this.context.smart_card_status_cd_t.Select(Mapper.Map<SmartCardStatusCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a SmartCardStatusCd by SmartCardStatusCd
        /// </summary>
		/// <param name="smart_card_status_cd">The smart_card_status_cd of the entity.</param>        
        /// <returns>A Smart Card Status Cd.</returns>
        public SmartCardStatusCdModel GetBySmartCardStatusCd(String smart_card_status_cd)
        {
            return Mapper.Map<SmartCardStatusCdModel>(this.context.smart_card_status_cd_t.Where(p => p.smart_card_status_cd == SmartCardStatusCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SmartCardStatusCdModel model)
        {
            this.context.smart_card_status_cd_t.Remove(Mapper.Map<smart_card_status_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SmartCardStatusCdModel model)
        {
            var record = this.context.smart_card_status_cd_t.Where(p => p.smart_card_status_cd == model.SmartCardStatusCd).FirstOrDefault();
            Mapper.Map<SmartCardStatusCdModel, smart_card_status_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SmartCardStatusCdModel model)
        {
            this.context.smart_card_status_cd_t.Add(Mapper.Map<smart_card_status_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}