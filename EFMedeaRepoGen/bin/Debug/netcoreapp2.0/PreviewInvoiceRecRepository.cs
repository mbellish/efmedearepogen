
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceRecRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Rec repository.
    /// </summary>
    public class PreviewInvoiceRecRepository : IPreviewInvoiceRecRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceRecRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceRecRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceRecModel> GetAll()
        {
            return this.context.preview_invoice_rec_t.Select(Mapper.Map<PreviewInvoiceRecModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceRec by InvoiceNb InvoiceRecNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>        
        /// <returns>A Preview Invoice Rec.</returns>
        public PreviewInvoiceRecModel GetByInvoiceNbInvoiceRecNb(Int32 invoice_nb,String invoice_rec_nb)
        {
            return Mapper.Map<PreviewInvoiceRecModel>(this.context.preview_invoice_rec_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb));
        }
       	    public List<PreviewInvoiceRecModel> GetByInvoiceNb(Int32 invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceRecModel model)
        {
            this.context.preview_invoice_rec_t.Remove(Mapper.Map<preview_invoice_rec_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceRecModel model)
        {
            var record = this.context.preview_invoice_rec_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceRecModel, preview_invoice_rec_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceRecModel model)
        {
            this.context.preview_invoice_rec_t.Add(Mapper.Map<preview_invoice_rec_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}