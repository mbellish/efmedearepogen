
// -----------------------------------------------------------------------
// <copyright file="ZoneCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Zone Cd repository.
    /// </summary>
    public class ZoneCdRepository : IZoneCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ZoneCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ZoneCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ZoneCdModel> GetAll()
        {
            return this.context.zone_cd_t.Select(Mapper.Map<ZoneCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a ZoneCd by ZoneCd
        /// </summary>
		/// <param name="zone_cd">The zone_cd of the entity.</param>        
        /// <returns>A Zone Cd.</returns>
        public ZoneCdModel GetByZoneCd(String zone_cd)
        {
            return Mapper.Map<ZoneCdModel>(this.context.zone_cd_t.Where(p => p.zone_cd == ZoneCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ZoneCdModel model)
        {
            this.context.zone_cd_t.Remove(Mapper.Map<zone_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ZoneCdModel model)
        {
            var record = this.context.zone_cd_t.Where(p => p.zone_cd == model.ZoneCd).FirstOrDefault();
            Mapper.Map<ZoneCdModel, zone_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ZoneCdModel model)
        {
            this.context.zone_cd_t.Add(Mapper.Map<zone_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}