
// -----------------------------------------------------------------------
// <copyright file="ForecastInvoiceRecRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Invoice Rec repository.
    /// </summary>
    public class ForecastInvoiceRecRepository : IForecastInvoiceRecRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastInvoiceRecRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastInvoiceRecRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastInvoiceRecModel> GetAll()
        {
            return this.context.forecast_invoice_rec_t.Select(Mapper.Map<ForecastInvoiceRecModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastInvoiceRec by InvoiceNb InvoiceRecNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>        
        /// <returns>A Forecast Invoice Rec.</returns>
        public ForecastInvoiceRecModel GetByInvoiceNbInvoiceRecNb(String invoice_nb,String invoice_rec_nb)
        {
            return Mapper.Map<ForecastInvoiceRecModel>(this.context.forecast_invoice_rec_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb));
        }
       	    public List<ForecastInvoiceRecModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastInvoiceRecModel model)
        {
            this.context.forecast_invoice_rec_t.Remove(Mapper.Map<forecast_invoice_rec_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastInvoiceRecModel model)
        {
            var record = this.context.forecast_invoice_rec_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb).FirstOrDefault();
            Mapper.Map<ForecastInvoiceRecModel, forecast_invoice_rec_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastInvoiceRecModel model)
        {
            this.context.forecast_invoice_rec_t.Add(Mapper.Map<forecast_invoice_rec_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}