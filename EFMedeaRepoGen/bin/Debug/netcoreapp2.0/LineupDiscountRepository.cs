
// -----------------------------------------------------------------------
// <copyright file="LineupDiscountRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Lineup Discount repository.
    /// </summary>
    public class LineupDiscountRepository : ILineupDiscountRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LineupDiscountRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public LineupDiscountRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<LineupDiscountModel> GetAll()
        {
            return this.context.lineup_discount_t.Select(Mapper.Map<LineupDiscountModel>).ToList();
        }

		        /// <summary>
        /// Gets a LineupDiscount by ContractNb ProgSrvcNm BlockNb RowNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="block_nb">The block_nb of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Lineup Discount.</returns>
        public LineupDiscountModel GetByContractNbProgSrvcNmBlockNbRowNb(String contract_nb,String prog_srvc_nm,Int32 block_nb,Int32 row_nb)
        {
            return Mapper.Map<LineupDiscountModel>(this.context.lineup_discount_t.Where(p => p.contract_nb == ContractNb && p.prog_srvc_nm == ProgSrvcNm && p.block_nb == BlockNb && p.row_nb == RowNb));
        }
       	    public List<LineupDiscountModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(LineupDiscountModel model)
        {
            this.context.lineup_discount_t.Remove(Mapper.Map<lineup_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(LineupDiscountModel model)
        {
            var record = this.context.lineup_discount_t.Where(p => p.contract_nb == model.ContractNb && p.prog_srvc_nm == model.ProgSrvcNm && p.block_nb == model.BlockNb && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<LineupDiscountModel, lineup_discount_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(LineupDiscountModel model)
        {
            this.context.lineup_discount_t.Add(Mapper.Map<lineup_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}