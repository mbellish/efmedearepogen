
// -----------------------------------------------------------------------
// <copyright file="MasterAgreementRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Master Agreement repository.
    /// </summary>
    public class MasterAgreementRepository : IMasterAgreementRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterAgreementRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MasterAgreementRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MasterAgreementModel> GetAll()
        {
            return this.context.master_agreement_t.Select(Mapper.Map<MasterAgreementModel>).ToList();
        }

		        /// <summary>
        /// Gets a MasterAgreement by MasterAgreementId
        /// </summary>
		/// <param name="master_agreement_id">The master_agreement_id of the entity.</param>        
        /// <returns>A Master Agreement.</returns>
        public MasterAgreementModel GetByMasterAgreementId(String master_agreement_id)
        {
            return Mapper.Map<MasterAgreementModel>(this.context.master_agreement_t.Where(p => p.master_agreement_id == MasterAgreementId));
        }
       	    public List<MasterAgreementModel> GetByAgreementCd(String agreementCd) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByBillingCustomerId(String billingCustomerId) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByContractSecurityGroupCd(String contractSecurityGroupCd) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByFinanceRepId(String financeRepId) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByLegalEntityCurrencyCd(String legalEntityCurrencyCd) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetBySalesExecId(String salesExecId) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetBySalesRegion(String salesRegion) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByStatusCd(String statusCd) {
			throw new NotImplementedException();
		}
		public List<MasterAgreementModel> GetByTechCd(String techCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MasterAgreementModel model)
        {
            this.context.master_agreement_t.Remove(Mapper.Map<master_agreement_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MasterAgreementModel model)
        {
            var record = this.context.master_agreement_t.Where(p => p.master_agreement_id == model.MasterAgreementId).FirstOrDefault();
            Mapper.Map<MasterAgreementModel, master_agreement_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MasterAgreementModel model)
        {
            this.context.master_agreement_t.Add(Mapper.Map<master_agreement_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}