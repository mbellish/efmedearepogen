
// -----------------------------------------------------------------------
// <copyright file="SysDiscountRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Discount repository.
    /// </summary>
    public class SysDiscountRepository : ISysDiscountRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysDiscountRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysDiscountRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysDiscountModel> GetAll()
        {
            return this.context.sys_discount_t.Select(Mapper.Map<SysDiscountModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysDiscount by RowId
        /// </summary>
		/// <param name="row_id">The row_id of the entity.</param>        
        /// <returns>A Sys Discount.</returns>
        public SysDiscountModel GetByRowId(Int32 row_id)
        {
            return Mapper.Map<SysDiscountModel>(this.context.sys_discount_t.Where(p => p.row_id == RowId));
        }
       	    public List<SysDiscountModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysDiscountModel model)
        {
            this.context.sys_discount_t.Remove(Mapper.Map<sys_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysDiscountModel model)
        {
            var record = this.context.sys_discount_t.Where(p => p.row_id == model.RowId).FirstOrDefault();
            Mapper.Map<SysDiscountModel, sys_discount_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysDiscountModel model)
        {
            this.context.sys_discount_t.Add(Mapper.Map<sys_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}