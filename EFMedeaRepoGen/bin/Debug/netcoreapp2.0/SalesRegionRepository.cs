
// -----------------------------------------------------------------------
// <copyright file="SalesRegionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sales Region repository.
    /// </summary>
    public class SalesRegionRepository : ISalesRegionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesRegionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SalesRegionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SalesRegionModel> GetAll()
        {
            return this.context.sales_region_t.Select(Mapper.Map<SalesRegionModel>).ToList();
        }

		        /// <summary>
        /// Gets a SalesRegion by SalesRegion
        /// </summary>
		/// <param name="sales_region">The sales_region of the entity.</param>        
        /// <returns>A Sales Region.</returns>
        public SalesRegionModel GetBySalesRegion(String sales_region)
        {
            return Mapper.Map<SalesRegionModel>(this.context.sales_region_t.Where(p => p.sales_region == SalesRegion));
        }
       	    public List<SalesRegionModel> GetByForecastIdFromForecastSalesRegion(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SalesRegionModel model)
        {
            this.context.sales_region_t.Remove(Mapper.Map<sales_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SalesRegionModel model)
        {
            var record = this.context.sales_region_t.Where(p => p.sales_region == model.SalesRegion).FirstOrDefault();
            Mapper.Map<SalesRegionModel, sales_region_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SalesRegionModel model)
        {
            this.context.sales_region_t.Add(Mapper.Map<sales_region_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}