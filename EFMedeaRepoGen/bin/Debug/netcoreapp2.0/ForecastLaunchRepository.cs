
// -----------------------------------------------------------------------
// <copyright file="ForecastLaunchRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Launch repository.
    /// </summary>
    public class ForecastLaunchRepository : IForecastLaunchRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastLaunchRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastLaunchRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastLaunchModel> GetAll()
        {
            return this.context.forecast_launch_t.Select(Mapper.Map<ForecastLaunchModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastLaunch by ForecastLaunchId
        /// </summary>
		/// <param name="forecast_launch_id">The forecast_launch_id of the entity.</param>        
        /// <returns>A Forecast Launch.</returns>
        public ForecastLaunchModel GetByForecastLaunchId(Int32 forecast_launch_id)
        {
            return Mapper.Map<ForecastLaunchModel>(this.context.forecast_launch_t.Where(p => p.forecast_launch_id == ForecastLaunchId));
        }
       	    public List<ForecastLaunchModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastLaunchModel model)
        {
            this.context.forecast_launch_t.Remove(Mapper.Map<forecast_launch_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastLaunchModel model)
        {
            var record = this.context.forecast_launch_t.Where(p => p.forecast_launch_id == model.ForecastLaunchId).FirstOrDefault();
            Mapper.Map<ForecastLaunchModel, forecast_launch_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastLaunchModel model)
        {
            this.context.forecast_launch_t.Add(Mapper.Map<forecast_launch_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}