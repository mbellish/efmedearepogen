
// -----------------------------------------------------------------------
// <copyright file="ContactCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact Comment repository.
    /// </summary>
    public class ContactCommentRepository : IContactCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactCommentModel> GetAll()
        {
            return this.context.contact_comment_t.Select(Mapper.Map<ContactCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactComment by CommentNb ContactNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="contact_nb">The contact_nb of the entity.</param>        
        /// <returns>A Contact Comment.</returns>
        public ContactCommentModel GetByCommentNbContactNb(String comment_nb,String contact_nb)
        {
            return Mapper.Map<ContactCommentModel>(this.context.contact_comment_t.Where(p => p.comment_nb == CommentNb && p.contact_nb == ContactNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactCommentModel model)
        {
            this.context.contact_comment_t.Remove(Mapper.Map<contact_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactCommentModel model)
        {
            var record = this.context.contact_comment_t.Where(p => p.comment_nb == model.CommentNb && p.contact_nb == model.ContactNb).FirstOrDefault();
            Mapper.Map<ContactCommentModel, contact_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactCommentModel model)
        {
            this.context.contact_comment_t.Add(Mapper.Map<contact_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}