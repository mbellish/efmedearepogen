
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceIntlRecalcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Intl Recalc repository.
    /// </summary>
    public class PreviewInvoiceIntlRecalcRepository : IPreviewInvoiceIntlRecalcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceIntlRecalcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceIntlRecalcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceIntlRecalcModel> GetAll()
        {
            return this.context.preview_invoice_intl_recalc_t.Select(Mapper.Map<PreviewInvoiceIntlRecalcModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceIntlRecalc by InvoiceNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Preview Invoice Intl Recalc.</returns>
        public PreviewInvoiceIntlRecalcModel GetByInvoiceNb(String invoice_nb)
        {
            return Mapper.Map<PreviewInvoiceIntlRecalcModel>(this.context.preview_invoice_intl_recalc_t.Where(p => p.invoice_nb == InvoiceNb));
        }
       	    public List<PreviewInvoiceIntlRecalcModel> GetByProcessFl(String processFl) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceIntlRecalcModel model)
        {
            this.context.preview_invoice_intl_recalc_t.Remove(Mapper.Map<preview_invoice_intl_recalc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceIntlRecalcModel model)
        {
            var record = this.context.preview_invoice_intl_recalc_t.Where(p => p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceIntlRecalcModel, preview_invoice_intl_recalc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceIntlRecalcModel model)
        {
            this.context.preview_invoice_intl_recalc_t.Add(Mapper.Map<preview_invoice_intl_recalc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}