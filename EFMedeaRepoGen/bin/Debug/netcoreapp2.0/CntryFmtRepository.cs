
// -----------------------------------------------------------------------
// <copyright file="CntryFmtRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Cntry Fmt repository.
    /// </summary>
    public class CntryFmtRepository : ICntryFmtRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CntryFmtRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CntryFmtRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CntryFmtModel> GetAll()
        {
            return this.context.cntry_fmt_t.Select(Mapper.Map<CntryFmtModel>).ToList();
        }

		        /// <summary>
        /// Gets a CntryFmt by CntryCd
        /// </summary>
		/// <param name="cntry_cd">The cntry_cd of the entity.</param>        
        /// <returns>A Cntry Fmt.</returns>
        public CntryFmtModel GetByCntryCd(String cntry_cd)
        {
            return Mapper.Map<CntryFmtModel>(this.context.cntry_fmt_t.Where(p => p.cntry_cd == CntryCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CntryFmtModel model)
        {
            this.context.cntry_fmt_t.Remove(Mapper.Map<cntry_fmt_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CntryFmtModel model)
        {
            var record = this.context.cntry_fmt_t.Where(p => p.cntry_cd == model.CntryCd).FirstOrDefault();
            Mapper.Map<CntryFmtModel, cntry_fmt_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CntryFmtModel model)
        {
            this.context.cntry_fmt_t.Add(Mapper.Map<cntry_fmt_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}