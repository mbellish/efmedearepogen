
// -----------------------------------------------------------------------
// <copyright file="ArRecXrefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Ar Rec Xref repository.
    /// </summary>
    public class ArRecXrefRepository : IArRecXrefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArRecXrefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArRecXrefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArRecXrefModel> GetAll()
        {
            return this.context.ar_rec_xref_t.Select(Mapper.Map<ArRecXrefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArRecXref by InvoiceNb1 InvoiceRecNb1 InvoiceNb2 InvoiceRecNb2
        /// </summary>
		/// <param name="invoice_nb1">The invoice_nb1 of the entity.</param>/// <param name="invoice_rec_nb1">The invoice_rec_nb1 of the entity.</param>/// <param name="invoice_nb2">The invoice_nb2 of the entity.</param>/// <param name="invoice_rec_nb2">The invoice_rec_nb2 of the entity.</param>        
        /// <returns>A Ar Rec Xref.</returns>
        public ArRecXrefModel GetByInvoiceNb1InvoiceRecNb1InvoiceNb2InvoiceRecNb2(String invoice_nb1,String invoice_rec_nb1,String invoice_nb2,String invoice_rec_nb2)
        {
            return Mapper.Map<ArRecXrefModel>(this.context.ar_rec_xref_t.Where(p => p.invoice_nb1 == InvoiceNb1 && p.invoice_rec_nb1 == InvoiceRecNb1 && p.invoice_nb2 == InvoiceNb2 && p.invoice_rec_nb2 == InvoiceRecNb2));
        }
       	    public List<ArRecXrefModel> GetByInvoiceNb1(String invoiceNb1) {
			throw new NotImplementedException();
		}
		public List<ArRecXrefModel> GetByInvoiceNb2(String invoiceNb2) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArRecXrefModel model)
        {
            this.context.ar_rec_xref_t.Remove(Mapper.Map<ar_rec_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArRecXrefModel model)
        {
            var record = this.context.ar_rec_xref_t.Where(p => p.invoice_nb1 == model.InvoiceNb1 && p.invoice_rec_nb1 == model.InvoiceRecNb1 && p.invoice_nb2 == model.InvoiceNb2 && p.invoice_rec_nb2 == model.InvoiceRecNb2).FirstOrDefault();
            Mapper.Map<ArRecXrefModel, ar_rec_xref_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArRecXrefModel model)
        {
            this.context.ar_rec_xref_t.Add(Mapper.Map<ar_rec_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}