
// -----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User repository.
    /// </summary>
    public class UserRepository : IUserRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserModel> GetAll()
        {
            return this.context.user_t.Select(Mapper.Map<UserModel>).ToList();
        }

		        /// <summary>
        /// Gets a User by UserId
        /// </summary>
		/// <param name="user_id">The user_id of the entity.</param>        
        /// <returns>A User.</returns>
        public UserModel GetByUserId(String user_id)
        {
            return Mapper.Map<UserModel>(this.context.user_t.Where(p => p.user_id == UserId));
        }
       	    public List<UserModel> GetByLdapUser(String ldapUser) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserModel model)
        {
            this.context.user_t.Remove(Mapper.Map<user_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserModel model)
        {
            var record = this.context.user_t.Where(p => p.user_id == model.UserId).FirstOrDefault();
            Mapper.Map<UserModel, user_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserModel model)
        {
            this.context.user_t.Add(Mapper.Map<user_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}