
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomOptionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Option repository.
    /// </summary>
    public class ArgoreportCustomOptionRepository : IArgoreportCustomOptionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomOptionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomOptionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomOptionModel> GetAll()
        {
            return this.context.argoreport_custom_option_t.Select(Mapper.Map<ArgoreportCustomOptionModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomOption by ArgoreportCustomId OptionCd
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="option_cd">The option_cd of the entity.</param>        
        /// <returns>A Argoreport Custom Option.</returns>
        public ArgoreportCustomOptionModel GetByArgoreportCustomIdOptionCd(String argoreport_custom_id,String option_cd)
        {
            return Mapper.Map<ArgoreportCustomOptionModel>(this.context.argoreport_custom_option_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.option_cd == OptionCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomOptionModel model)
        {
            this.context.argoreport_custom_option_t.Remove(Mapper.Map<argoreport_custom_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomOptionModel model)
        {
            var record = this.context.argoreport_custom_option_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.option_cd == model.OptionCd).FirstOrDefault();
            Mapper.Map<ArgoreportCustomOptionModel, argoreport_custom_option_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomOptionModel model)
        {
            this.context.argoreport_custom_option_t.Add(Mapper.Map<argoreport_custom_option_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}