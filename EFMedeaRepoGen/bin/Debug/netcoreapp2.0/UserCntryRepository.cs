
// -----------------------------------------------------------------------
// <copyright file="UserCntryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User Cntry repository.
    /// </summary>
    public class UserCntryRepository : IUserCntryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserCntryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserCntryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserCntryModel> GetAll()
        {
            return this.context.user_cntry_t.Select(Mapper.Map<UserCntryModel>).ToList();
        }

		        /// <summary>
        /// Gets a UserCntry by UserId CntryCd
        /// </summary>
		/// <param name="user_id">The user_id of the entity.</param>/// <param name="cntry_cd">The cntry_cd of the entity.</param>        
        /// <returns>A User Cntry.</returns>
        public UserCntryModel GetByUserIdCntryCd(String user_id,String cntry_cd)
        {
            return Mapper.Map<UserCntryModel>(this.context.user_cntry_t.Where(p => p.user_id == UserId && p.cntry_cd == CntryCd));
        }
       	    public List<UserCntryModel> GetByUserId(String userId) {
			throw new NotImplementedException();
		}
		public List<UserCntryModel> GetByCntryCd(String cntryCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserCntryModel model)
        {
            this.context.user_cntry_t.Remove(Mapper.Map<user_cntry_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserCntryModel model)
        {
            var record = this.context.user_cntry_t.Where(p => p.user_id == model.UserId && p.cntry_cd == model.CntryCd).FirstOrDefault();
            Mapper.Map<UserCntryModel, user_cntry_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserCntryModel model)
        {
            this.context.user_cntry_t.Add(Mapper.Map<user_cntry_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}