
// -----------------------------------------------------------------------
// <copyright file="ContractTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Tax repository.
    /// </summary>
    public class ContractTaxRepository : IContractTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractTaxModel> GetAll()
        {
            return this.context.contract_tax_t.Select(Mapper.Map<ContractTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractTax by ContractTaxId
        /// </summary>
		/// <param name="contract_tax_id">The contract_tax_id of the entity.</param>        
        /// <returns>A Contract Tax.</returns>
        public ContractTaxModel GetByContractTaxId(Int32 contract_tax_id)
        {
            return Mapper.Map<ContractTaxModel>(this.context.contract_tax_t.Where(p => p.contract_tax_id == ContractTaxId));
        }
       	    public IList<ContractTaxModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractTaxModel model)
        {
            this.context.contract_tax_t.Remove(Mapper.Map<contract_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractTaxModel model)
        {
            var record = this.context.contract_tax_t.Where(p => p.contract_tax_id == model.ContractTaxId).FirstOrDefault();
            Mapper.Map<ContractTaxModel, contract_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractTaxModel model)
        {
            this.context.contract_tax_t.Add(Mapper.Map<contract_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}