
// -----------------------------------------------------------------------
// <copyright file="SysContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Contact repository.
    /// </summary>
    public class SysContactRepository : ISysContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysContactModel> GetAll()
        {
            return this.context.sys_contact_t.Select(Mapper.Map<SysContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysContact by ContactNb SysId
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Sys Contact.</returns>
        public SysContactModel GetByContactNbSysId(String contact_nb,String sys_id)
        {
            return Mapper.Map<SysContactModel>(this.context.sys_contact_t.Where(p => p.contact_nb == ContactNb && p.sys_id == SysId));
        }
       	    public List<SysContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<SysContactModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysContactModel model)
        {
            this.context.sys_contact_t.Remove(Mapper.Map<sys_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysContactModel model)
        {
            var record = this.context.sys_contact_t.Where(p => p.contact_nb == model.ContactNb && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<SysContactModel, sys_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysContactModel model)
        {
            this.context.sys_contact_t.Add(Mapper.Map<sys_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}