
// -----------------------------------------------------------------------
// <copyright file="UserSrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User Srvc repository.
    /// </summary>
    public class UserSrvcRepository : IUserSrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserSrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserSrvcModel> GetAll()
        {
            return this.context.user_srvc_t.Select(Mapper.Map<UserSrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a UserSrvc by UserId ProgSrvcNm
        /// </summary>
		/// <param name="user_id">The user_id of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>        
        /// <returns>A User Srvc.</returns>
        public UserSrvcModel GetByUserIdProgSrvcNm(String user_id,String prog_srvc_nm)
        {
            return Mapper.Map<UserSrvcModel>(this.context.user_srvc_t.Where(p => p.user_id == UserId && p.prog_srvc_nm == ProgSrvcNm));
        }
       	    public List<UserSrvcModel> GetByUserId(String userId) {
			throw new NotImplementedException();
		}
		public List<UserSrvcModel> GetByProgSrvcNm(String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserSrvcModel model)
        {
            this.context.user_srvc_t.Remove(Mapper.Map<user_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserSrvcModel model)
        {
            var record = this.context.user_srvc_t.Where(p => p.user_id == model.UserId && p.prog_srvc_nm == model.ProgSrvcNm).FirstOrDefault();
            Mapper.Map<UserSrvcModel, user_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserSrvcModel model)
        {
            this.context.user_srvc_t.Add(Mapper.Map<user_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}