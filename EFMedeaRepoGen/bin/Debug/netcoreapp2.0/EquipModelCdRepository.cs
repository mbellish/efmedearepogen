
// -----------------------------------------------------------------------
// <copyright file="EquipModelCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equip Model Cd repository.
    /// </summary>
    public class EquipModelCdRepository : IEquipModelCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipModelCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipModelCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipModelCdModel> GetAll()
        {
            return this.context.equip_model_cd_t.Select(Mapper.Map<EquipModelCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipModelCd by EquipModelCd
        /// </summary>
		/// <param name="equip_model_cd">The equip_model_cd of the entity.</param>        
        /// <returns>A Equip Model Cd.</returns>
        public EquipModelCdModel GetByEquipModelCd(String equip_model_cd)
        {
            return Mapper.Map<EquipModelCdModel>(this.context.equip_model_cd_t.Where(p => p.equip_model_cd == EquipModelCd));
        }
       	    public List<EquipModelCdModel> GetByProgSrvcNmFromModelSrvc(String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipModelCdModel model)
        {
            this.context.equip_model_cd_t.Remove(Mapper.Map<equip_model_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipModelCdModel model)
        {
            var record = this.context.equip_model_cd_t.Where(p => p.equip_model_cd == model.EquipModelCd).FirstOrDefault();
            Mapper.Map<EquipModelCdModel, equip_model_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipModelCdModel model)
        {
            this.context.equip_model_cd_t.Add(Mapper.Map<equip_model_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}