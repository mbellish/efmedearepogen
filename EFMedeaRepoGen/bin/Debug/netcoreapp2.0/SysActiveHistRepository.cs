
// -----------------------------------------------------------------------
// <copyright file="SysActiveHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Active Hist repository.
    /// </summary>
    public class SysActiveHistRepository : ISysActiveHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysActiveHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysActiveHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysActiveHistModel> GetAll()
        {
            return this.context.sys_active_hist_t.Select(Mapper.Map<SysActiveHistModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysActiveHist by RowNb SysId
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Sys Active Hist.</returns>
        public SysActiveHistModel GetByRowNbSysId(Int32 row_nb,String sys_id)
        {
            return Mapper.Map<SysActiveHistModel>(this.context.sys_active_hist_t.Where(p => p.row_nb == RowNb && p.sys_id == SysId));
        }
       	    public List<SysActiveHistModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysActiveHistModel model)
        {
            this.context.sys_active_hist_t.Remove(Mapper.Map<sys_active_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysActiveHistModel model)
        {
            var record = this.context.sys_active_hist_t.Where(p => p.row_nb == model.RowNb && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<SysActiveHistModel, sys_active_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysActiveHistModel model)
        {
            this.context.sys_active_hist_t.Add(Mapper.Map<sys_active_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}