
// -----------------------------------------------------------------------
// <copyright file="CommentTypeCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Comment Type Cd repository.
    /// </summary>
    public class CommentTypeCdRepository : ICommentTypeCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentTypeCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommentTypeCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommentTypeCdModel> GetAll()
        {
            return this.context.comment_type_cd_t.Select(Mapper.Map<CommentTypeCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a CommentTypeCd by CommentCd
        /// </summary>
		/// <param name="comment_cd">The comment_cd of the entity.</param>        
        /// <returns>A Comment Type Cd.</returns>
        public CommentTypeCdModel GetByCommentCd(String comment_cd)
        {
            return Mapper.Map<CommentTypeCdModel>(this.context.comment_type_cd_t.Where(p => p.comment_cd == CommentCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommentTypeCdModel model)
        {
            this.context.comment_type_cd_t.Remove(Mapper.Map<comment_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommentTypeCdModel model)
        {
            var record = this.context.comment_type_cd_t.Where(p => p.comment_cd == model.CommentCd).FirstOrDefault();
            Mapper.Map<CommentTypeCdModel, comment_type_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommentTypeCdModel model)
        {
            this.context.comment_type_cd_t.Add(Mapper.Map<comment_type_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}