
// -----------------------------------------------------------------------
// <copyright file="BatchRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Batch repository.
    /// </summary>
    public class BatchRepository : IBatchRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BatchRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BatchModel> GetAll()
        {
            return this.context.batch_t.Select(Mapper.Map<BatchModel>).ToList();
        }

		        /// <summary>
        /// Gets a Batch by BatchNb
        /// </summary>
		/// <param name="batch_nb">The batch_nb of the entity.</param>        
        /// <returns>A Batch.</returns>
        public BatchModel GetByBatchNb(String batch_nb)
        {
            return Mapper.Map<BatchModel>(this.context.batch_t.Where(p => p.batch_nb == BatchNb));
        }
       	    public List<BatchModel> GetByBatchType(String batchType) {
			throw new NotImplementedException();
		}
		public List<BatchModel> GetByPaymentBankCd(String paymentBankCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BatchModel model)
        {
            this.context.batch_t.Remove(Mapper.Map<batch_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BatchModel model)
        {
            var record = this.context.batch_t.Where(p => p.batch_nb == model.BatchNb).FirstOrDefault();
            Mapper.Map<BatchModel, batch_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BatchModel model)
        {
            this.context.batch_t.Add(Mapper.Map<batch_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}