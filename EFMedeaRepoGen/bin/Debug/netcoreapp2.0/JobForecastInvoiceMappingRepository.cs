
// -----------------------------------------------------------------------
// <copyright file="JobForecastInvoiceMappingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Forecast Invoice Mapping repository.
    /// </summary>
    public class JobForecastInvoiceMappingRepository : IJobForecastInvoiceMappingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobForecastInvoiceMappingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobForecastInvoiceMappingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobForecastInvoiceMappingModel> GetAll()
        {
            return this.context.job_forecast_invoice_mapping_t.Select(Mapper.Map<JobForecastInvoiceMappingModel>).ToList();
        }

			    public List<JobForecastInvoiceMappingModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobForecastInvoiceMappingModel model)
        {
            this.context.job_forecast_invoice_mapping_t.Remove(Mapper.Map<job_forecast_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobForecastInvoiceMappingModel model)
        {
            var record = this.context.job_forecast_invoice_mapping_t.Where(p => ).FirstOrDefault();
            Mapper.Map<JobForecastInvoiceMappingModel, job_forecast_invoice_mapping_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobForecastInvoiceMappingModel model)
        {
            this.context.job_forecast_invoice_mapping_t.Add(Mapper.Map<job_forecast_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}