
// -----------------------------------------------------------------------
// <copyright file="ContractCommitGroupSysRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Commit Group Sys repository.
    /// </summary>
    public class ContractCommitGroupSysRepository : IContractCommitGroupSysRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCommitGroupSysRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCommitGroupSysRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCommitGroupSysModel> GetAll()
        {
            return this.context.contract_commit_group_sys_t.Select(Mapper.Map<ContractCommitGroupSysModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCommitGroupSys by CommitGroupNb SysId
        /// </summary>
		/// <param name="commit_group_nb">The commit_group_nb of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Contract Commit Group Sys.</returns>
        public ContractCommitGroupSysModel GetByCommitGroupNbSysId(String commit_group_nb,String sys_id)
        {
            return Mapper.Map<ContractCommitGroupSysModel>(this.context.contract_commit_group_sys_t.Where(p => p.commit_group_nb == CommitGroupNb && p.sys_id == SysId));
        }
       	    public List<ContractCommitGroupSysModel> GetByCommitGroupNb(String commitGroupNb) {
			throw new NotImplementedException();
		}
		public List<ContractCommitGroupSysModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCommitGroupSysModel model)
        {
            this.context.contract_commit_group_sys_t.Remove(Mapper.Map<contract_commit_group_sys_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCommitGroupSysModel model)
        {
            var record = this.context.contract_commit_group_sys_t.Where(p => p.commit_group_nb == model.CommitGroupNb && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<ContractCommitGroupSysModel, contract_commit_group_sys_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCommitGroupSysModel model)
        {
            this.context.contract_commit_group_sys_t.Add(Mapper.Map<contract_commit_group_sys_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}