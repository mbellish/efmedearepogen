
// -----------------------------------------------------------------------
// <copyright file="SysSrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc repository.
    /// </summary>
    public class SysSrvcRepository : ISysSrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcModel> GetAll()
        {
            return this.context.sys_srvc_t.Select(Mapper.Map<SysSrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvc by ProgSrvcNm SysId
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Sys Srvc.</returns>
        public SysSrvcModel GetByProgSrvcNmSysId(String prog_srvc_nm,String sys_id)
        {
            return Mapper.Map<SysSrvcModel>(this.context.sys_srvc_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.sys_id == SysId));
        }
       	    public List<SysSrvcModel> GetByProgSrvcNm(String progSrvcNm) {
			throw new NotImplementedException();
		}
		public List<SysSrvcModel> GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		public SysSrvcModel GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		public List<SysSrvcModel> GetByContractNb(String contract_nb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcModel model)
        {
            this.context.sys_srvc_t.Remove(Mapper.Map<sys_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcModel model)
        {
            var record = this.context.sys_srvc_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<SysSrvcModel, sys_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcModel model)
        {
            this.context.sys_srvc_t.Add(Mapper.Map<sys_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}