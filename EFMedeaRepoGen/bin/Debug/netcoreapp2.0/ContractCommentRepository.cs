
// -----------------------------------------------------------------------
// <copyright file="ContractCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Comment repository.
    /// </summary>
    public class ContractCommentRepository : IContractCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCommentModel> GetAll()
        {
            return this.context.contract_comment_t.Select(Mapper.Map<ContractCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractComment by ContractNb CommentNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="comment_nb">The comment_nb of the entity.</param>        
        /// <returns>A Contract Comment.</returns>
        public ContractCommentModel GetByContractNbCommentNb(String contract_nb,String comment_nb)
        {
            return Mapper.Map<ContractCommentModel>(this.context.contract_comment_t.Where(p => p.contract_nb == ContractNb && p.comment_nb == CommentNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCommentModel model)
        {
            this.context.contract_comment_t.Remove(Mapper.Map<contract_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCommentModel model)
        {
            var record = this.context.contract_comment_t.Where(p => p.contract_nb == model.ContractNb && p.comment_nb == model.CommentNb).FirstOrDefault();
            Mapper.Map<ContractCommentModel, contract_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCommentModel model)
        {
            this.context.contract_comment_t.Add(Mapper.Map<contract_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}