
// -----------------------------------------------------------------------
// <copyright file="CloseRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Close repository.
    /// </summary>
    public class CloseRepository : ICloseRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloseRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CloseRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CloseModel> GetAll()
        {
            return this.context.close_t.Select(Mapper.Map<CloseModel>).ToList();
        }

		        /// <summary>
        /// Gets a Close by CloseDt CloseForDt
        /// </summary>
		/// <param name="close_dt">The close_dt of the entity.</param>/// <param name="close_for_dt">The close_for_dt of the entity.</param>        
        /// <returns>A Close.</returns>
        public CloseModel GetByCloseDtCloseForDt(DateTime close_dt,DateTime close_for_dt)
        {
            return Mapper.Map<CloseModel>(this.context.close_t.Where(p => p.close_dt == CloseDt && p.close_for_dt == CloseForDt));
        }
       	    public CloseModel GetByCloseDt(DateTime closeDt) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CloseModel model)
        {
            this.context.close_t.Remove(Mapper.Map<close_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CloseModel model)
        {
            var record = this.context.close_t.Where(p => p.close_dt == model.CloseDt && p.close_for_dt == model.CloseForDt).FirstOrDefault();
            Mapper.Map<CloseModel, close_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CloseModel model)
        {
            this.context.close_t.Add(Mapper.Map<close_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}