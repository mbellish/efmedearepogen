
// -----------------------------------------------------------------------
// <copyright file="MsoPenDiscountRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Pen Discount repository.
    /// </summary>
    public class MsoPenDiscountRepository : IMsoPenDiscountRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoPenDiscountRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoPenDiscountRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoPenDiscountModel> GetAll()
        {
            return this.context.mso_pen_discount_t.Select(Mapper.Map<MsoPenDiscountModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoPenDiscount by DiscountId
        /// </summary>
		/// <param name="discount_id">The discount_id of the entity.</param>        
        /// <returns>A Mso Pen Discount.</returns>
        public MsoPenDiscountModel GetByDiscountId(Int32 discount_id)
        {
            return Mapper.Map<MsoPenDiscountModel>(this.context.mso_pen_discount_t.Where(p => p.discount_id == DiscountId));
        }
       	    public List<MsoPenDiscountModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<MsoPenDiscountModel> GetByContractNbStartDt(String contractNb, DateTime startDt) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoPenDiscountModel model)
        {
            this.context.mso_pen_discount_t.Remove(Mapper.Map<mso_pen_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoPenDiscountModel model)
        {
            var record = this.context.mso_pen_discount_t.Where(p => p.discount_id == model.DiscountId).FirstOrDefault();
            Mapper.Map<MsoPenDiscountModel, mso_pen_discount_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoPenDiscountModel model)
        {
            this.context.mso_pen_discount_t.Add(Mapper.Map<mso_pen_discount_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}