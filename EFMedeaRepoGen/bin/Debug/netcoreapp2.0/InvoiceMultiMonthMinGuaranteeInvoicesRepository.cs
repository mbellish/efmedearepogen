
// -----------------------------------------------------------------------
// <copyright file="InvoiceMultiMonthMinGuaranteeInvoicesRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Multi Month Min Guarantee Invoices repository.
    /// </summary>
    public class InvoiceMultiMonthMinGuaranteeInvoicesRepository : IInvoiceMultiMonthMinGuaranteeInvoicesRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceMultiMonthMinGuaranteeInvoicesRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceMultiMonthMinGuaranteeInvoicesRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceMultiMonthMinGuaranteeInvoicesModel> GetAll()
        {
            return this.context.invoice_multi_month_min_guarantee_invoices_t.Select(Mapper.Map<InvoiceMultiMonthMinGuaranteeInvoicesModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceMultiMonthMinGuaranteeInvoices by InvoiceNb PriorInvoiceNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="prior_invoice_nb">The prior_invoice_nb of the entity.</param>        
        /// <returns>A Invoice Multi Month Min Guarantee Invoices.</returns>
        public InvoiceMultiMonthMinGuaranteeInvoicesModel GetByInvoiceNbPriorInvoiceNb(String invoice_nb,String prior_invoice_nb)
        {
            return Mapper.Map<InvoiceMultiMonthMinGuaranteeInvoicesModel>(this.context.invoice_multi_month_min_guarantee_invoices_t.Where(p => p.invoice_nb == InvoiceNb && p.prior_invoice_nb == PriorInvoiceNb));
        }
       	    public List<InvoiceMultiMonthMinGuaranteeInvoicesModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceMultiMonthMinGuaranteeInvoicesModel model)
        {
            this.context.invoice_multi_month_min_guarantee_invoices_t.Remove(Mapper.Map<invoice_multi_month_min_guarantee_invoices_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceMultiMonthMinGuaranteeInvoicesModel model)
        {
            var record = this.context.invoice_multi_month_min_guarantee_invoices_t.Where(p => p.invoice_nb == model.InvoiceNb && p.prior_invoice_nb == model.PriorInvoiceNb).FirstOrDefault();
            Mapper.Map<InvoiceMultiMonthMinGuaranteeInvoicesModel, invoice_multi_month_min_guarantee_invoices_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceMultiMonthMinGuaranteeInvoicesModel model)
        {
            this.context.invoice_multi_month_min_guarantee_invoices_t.Add(Mapper.Map<invoice_multi_month_min_guarantee_invoices_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}