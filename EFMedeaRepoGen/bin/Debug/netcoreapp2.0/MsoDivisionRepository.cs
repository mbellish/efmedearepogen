
// -----------------------------------------------------------------------
// <copyright file="MsoDivisionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Division repository.
    /// </summary>
    public class MsoDivisionRepository : IMsoDivisionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDivisionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDivisionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDivisionModel> GetAll()
        {
            return this.context.mso_division_t.Select(Mapper.Map<MsoDivisionModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDivision by MsoNb DivisionNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="division_nb">The division_nb of the entity.</param>        
        /// <returns>A Mso Division.</returns>
        public MsoDivisionModel GetByMsoNbDivisionNb(String mso_nb,String division_nb)
        {
            return Mapper.Map<MsoDivisionModel>(this.context.mso_division_t.Where(p => p.mso_nb == MsoNb && p.division_nb == DivisionNb));
        }
       	    public List<MsoDivisionModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDivisionModel model)
        {
            this.context.mso_division_t.Remove(Mapper.Map<mso_division_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDivisionModel model)
        {
            var record = this.context.mso_division_t.Where(p => p.mso_nb == model.MsoNb && p.division_nb == model.DivisionNb).FirstOrDefault();
            Mapper.Map<MsoDivisionModel, mso_division_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDivisionModel model)
        {
            this.context.mso_division_t.Add(Mapper.Map<mso_division_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}