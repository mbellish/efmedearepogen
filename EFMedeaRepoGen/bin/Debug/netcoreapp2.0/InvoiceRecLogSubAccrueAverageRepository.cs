
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogSubAccrueAverageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log Sub Accrue Average repository.
    /// </summary>
    public class InvoiceRecLogSubAccrueAverageRepository : IInvoiceRecLogSubAccrueAverageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogSubAccrueAverageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogSubAccrueAverageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogSubAccrueAverageModel> GetAll()
        {
            return this.context.invoice_rec_log_sub_accrue_average_t.Select(Mapper.Map<InvoiceRecLogSubAccrueAverageModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLogSubAccrueAverage by InvoiceRecLogSubAccrueAverageId
        /// </summary>
		/// <param name="invoice_rec_log_sub_accrue_average_id">The invoice_rec_log_sub_accrue_average_id of the entity.</param>        
        /// <returns>A Invoice Rec Log Sub Accrue Average.</returns>
        public InvoiceRecLogSubAccrueAverageModel GetByInvoiceRecLogSubAccrueAverageId(Int32 invoice_rec_log_sub_accrue_average_id)
        {
            return Mapper.Map<InvoiceRecLogSubAccrueAverageModel>(this.context.invoice_rec_log_sub_accrue_average_t.Where(p => p.invoice_rec_log_sub_accrue_average_id == InvoiceRecLogSubAccrueAverageId));
        }
       	    public List<InvoiceRecLogSubAccrueAverageModel> GetByInvoiceRecLogId(Int32 invoiceRecLogId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogSubAccrueAverageModel model)
        {
            this.context.invoice_rec_log_sub_accrue_average_t.Remove(Mapper.Map<invoice_rec_log_sub_accrue_average_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogSubAccrueAverageModel model)
        {
            var record = this.context.invoice_rec_log_sub_accrue_average_t.Where(p => p.invoice_rec_log_sub_accrue_average_id == model.InvoiceRecLogSubAccrueAverageId).FirstOrDefault();
            Mapper.Map<InvoiceRecLogSubAccrueAverageModel, invoice_rec_log_sub_accrue_average_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogSubAccrueAverageModel model)
        {
            this.context.invoice_rec_log_sub_accrue_average_t.Add(Mapper.Map<invoice_rec_log_sub_accrue_average_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}