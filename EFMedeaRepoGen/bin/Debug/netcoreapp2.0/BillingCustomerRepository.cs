
// -----------------------------------------------------------------------
// <copyright file="BillingCustomerRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Billing Customer repository.
    /// </summary>
    public class BillingCustomerRepository : IBillingCustomerRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillingCustomerRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BillingCustomerRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BillingCustomerModel> GetAll()
        {
            return this.context.billing_customer_t.Select(Mapper.Map<BillingCustomerModel>).ToList();
        }

		        /// <summary>
        /// Gets a BillingCustomer by BillingCustomerId
        /// </summary>
		/// <param name="billing_customer_id">The billing_customer_id of the entity.</param>        
        /// <returns>A Billing Customer.</returns>
        public BillingCustomerModel GetByBillingCustomerId(String billing_customer_id)
        {
            return Mapper.Map<BillingCustomerModel>(this.context.billing_customer_t.Where(p => p.billing_customer_id == BillingCustomerId));
        }
       	    public List<BillingCustomerModel> GetByBillingCustomerNameWithNoPunctuation(String billingCustomerName) {
			throw new NotImplementedException();
		}
		public List<BillingCustomerModel> GetByAltId(String altId) {
			throw new NotImplementedException();
		}
		public BillingCustomerModel GetWhereContractsAssigned(String billingCustomerId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BillingCustomerModel model)
        {
            this.context.billing_customer_t.Remove(Mapper.Map<billing_customer_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BillingCustomerModel model)
        {
            var record = this.context.billing_customer_t.Where(p => p.billing_customer_id == model.BillingCustomerId).FirstOrDefault();
            Mapper.Map<BillingCustomerModel, billing_customer_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BillingCustomerModel model)
        {
            this.context.billing_customer_t.Add(Mapper.Map<billing_customer_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}