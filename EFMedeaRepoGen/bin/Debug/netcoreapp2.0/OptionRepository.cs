
// -----------------------------------------------------------------------
// <copyright file="OptionRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Option repository.
    /// </summary>
    public class OptionRepository : IOptionRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public OptionRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<OptionModel> GetAll()
        {
            return this.context.option_t.Select(Mapper.Map<OptionModel>).ToList();
        }

		        /// <summary>
        /// Gets a Option by OptionCd
        /// </summary>
		/// <param name="option_cd">The option_cd of the entity.</param>        
        /// <returns>A Option.</returns>
        public OptionModel GetByOptionCd(String option_cd)
        {
            return Mapper.Map<OptionModel>(this.context.option_t.Where(p => p.option_cd == OptionCd));
        }
       	    public OptionModel GetOptionallyRequiredField(String screenID, String datawindowName, String columnName) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(OptionModel model)
        {
            this.context.option_t.Remove(Mapper.Map<option_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(OptionModel model)
        {
            var record = this.context.option_t.Where(p => p.option_cd == model.OptionCd).FirstOrDefault();
            Mapper.Map<OptionModel, option_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(OptionModel model)
        {
            this.context.option_t.Add(Mapper.Map<option_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}