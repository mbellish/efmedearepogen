
// -----------------------------------------------------------------------
// <copyright file="MsoDivisionContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Division Contact repository.
    /// </summary>
    public class MsoDivisionContactRepository : IMsoDivisionContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDivisionContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDivisionContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDivisionContactModel> GetAll()
        {
            return this.context.mso_division_contact_t.Select(Mapper.Map<MsoDivisionContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDivisionContact by ContactNb MsoNb DivisionNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="division_nb">The division_nb of the entity.</param>        
        /// <returns>A Mso Division Contact.</returns>
        public MsoDivisionContactModel GetByContactNbMsoNbDivisionNb(String contact_nb,String mso_nb,String division_nb)
        {
            return Mapper.Map<MsoDivisionContactModel>(this.context.mso_division_contact_t.Where(p => p.contact_nb == ContactNb && p.mso_nb == MsoNb && p.division_nb == DivisionNb));
        }
       	    public List<MsoDivisionContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<MsoDivisionContactModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoDivisionContactModel> GetByMsoNbDivisionNb(String msoNb, String divisionNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDivisionContactModel model)
        {
            this.context.mso_division_contact_t.Remove(Mapper.Map<mso_division_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDivisionContactModel model)
        {
            var record = this.context.mso_division_contact_t.Where(p => p.contact_nb == model.ContactNb && p.mso_nb == model.MsoNb && p.division_nb == model.DivisionNb).FirstOrDefault();
            Mapper.Map<MsoDivisionContactModel, mso_division_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDivisionContactModel model)
        {
            this.context.mso_division_contact_t.Add(Mapper.Map<mso_division_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}