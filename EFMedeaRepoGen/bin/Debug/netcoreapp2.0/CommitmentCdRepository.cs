
// -----------------------------------------------------------------------
// <copyright file="CommitmentCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Commitment Cd repository.
    /// </summary>
    public class CommitmentCdRepository : ICommitmentCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitmentCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommitmentCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommitmentCdModel> GetAll()
        {
            return this.context.commitment_cd_t.Select(Mapper.Map<CommitmentCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a CommitmentCd by CommitmentCd
        /// </summary>
		/// <param name="commitment_cd">The commitment_cd of the entity.</param>        
        /// <returns>A Commitment Cd.</returns>
        public CommitmentCdModel GetByCommitmentCd(String commitment_cd)
        {
            return Mapper.Map<CommitmentCdModel>(this.context.commitment_cd_t.Where(p => p.commitment_cd == CommitmentCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommitmentCdModel model)
        {
            this.context.commitment_cd_t.Remove(Mapper.Map<commitment_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommitmentCdModel model)
        {
            var record = this.context.commitment_cd_t.Where(p => p.commitment_cd == model.CommitmentCd).FirstOrDefault();
            Mapper.Map<CommitmentCdModel, commitment_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommitmentCdModel model)
        {
            this.context.commitment_cd_t.Add(Mapper.Map<commitment_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}