
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogRevenueAccrueAverageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log Revenue Accrue Average repository.
    /// </summary>
    public class InvoiceRecLogRevenueAccrueAverageRepository : IInvoiceRecLogRevenueAccrueAverageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogRevenueAccrueAverageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogRevenueAccrueAverageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogRevenueAccrueAverageModel> GetAll()
        {
            return this.context.invoice_rec_log_revenue_accrue_average_t.Select(Mapper.Map<InvoiceRecLogRevenueAccrueAverageModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLogRevenueAccrueAverage by InvoiceRecLogRevenueAccrueAverageId
        /// </summary>
		/// <param name="invoice_rec_log_revenue_accrue_average_id">The invoice_rec_log_revenue_accrue_average_id of the entity.</param>        
        /// <returns>A Invoice Rec Log Revenue Accrue Average.</returns>
        public InvoiceRecLogRevenueAccrueAverageModel GetByInvoiceRecLogRevenueAccrueAverageId(Int32 invoice_rec_log_revenue_accrue_average_id)
        {
            return Mapper.Map<InvoiceRecLogRevenueAccrueAverageModel>(this.context.invoice_rec_log_revenue_accrue_average_t.Where(p => p.invoice_rec_log_revenue_accrue_average_id == InvoiceRecLogRevenueAccrueAverageId));
        }
       	    public List<InvoiceRecLogRevenueAccrueAverageModel> GetByInvoiceRecLogId(Int32 invoiceRecLogId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogRevenueAccrueAverageModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_average_t.Remove(Mapper.Map<invoice_rec_log_revenue_accrue_average_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogRevenueAccrueAverageModel model)
        {
            var record = this.context.invoice_rec_log_revenue_accrue_average_t.Where(p => p.invoice_rec_log_revenue_accrue_average_id == model.InvoiceRecLogRevenueAccrueAverageId).FirstOrDefault();
            Mapper.Map<InvoiceRecLogRevenueAccrueAverageModel, invoice_rec_log_revenue_accrue_average_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogRevenueAccrueAverageModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_average_t.Add(Mapper.Map<invoice_rec_log_revenue_accrue_average_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}