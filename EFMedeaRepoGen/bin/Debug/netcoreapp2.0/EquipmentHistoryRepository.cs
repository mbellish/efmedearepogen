
// -----------------------------------------------------------------------
// <copyright file="EquipmentHistoryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Equipment History repository.
    /// </summary>
    public class EquipmentHistoryRepository : IEquipmentHistoryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentHistoryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public EquipmentHistoryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<EquipmentHistoryModel> GetAll()
        {
            return this.context.equipment_history_t.Select(Mapper.Map<EquipmentHistoryModel>).ToList();
        }

		        /// <summary>
        /// Gets a EquipmentHistory by RowNb
        /// </summary>
		/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Equipment History.</returns>
        public EquipmentHistoryModel GetByRowNb(Int32 row_nb)
        {
            return Mapper.Map<EquipmentHistoryModel>(this.context.equipment_history_t.Where(p => p.row_nb == RowNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(EquipmentHistoryModel model)
        {
            this.context.equipment_history_t.Remove(Mapper.Map<equipment_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(EquipmentHistoryModel model)
        {
            var record = this.context.equipment_history_t.Where(p => p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<EquipmentHistoryModel, equipment_history_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(EquipmentHistoryModel model)
        {
            this.context.equipment_history_t.Add(Mapper.Map<equipment_history_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}