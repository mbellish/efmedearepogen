
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomFilterRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Filter repository.
    /// </summary>
    public class ArgoreportCustomFilterRepository : IArgoreportCustomFilterRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomFilterRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomFilterRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomFilterModel> GetAll()
        {
            return this.context.argoreport_custom_filter_t.Select(Mapper.Map<ArgoreportCustomFilterModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomFilter by ArgoreportCustomId RowNb
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Argoreport Custom Filter.</returns>
        public ArgoreportCustomFilterModel GetByArgoreportCustomIdRowNb(String argoreport_custom_id,Int32 row_nb)
        {
            return Mapper.Map<ArgoreportCustomFilterModel>(this.context.argoreport_custom_filter_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.row_nb == RowNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomFilterModel model)
        {
            this.context.argoreport_custom_filter_t.Remove(Mapper.Map<argoreport_custom_filter_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomFilterModel model)
        {
            var record = this.context.argoreport_custom_filter_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<ArgoreportCustomFilterModel, argoreport_custom_filter_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomFilterModel model)
        {
            this.context.argoreport_custom_filter_t.Add(Mapper.Map<argoreport_custom_filter_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}