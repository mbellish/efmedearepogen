
// -----------------------------------------------------------------------
// <copyright file="ForecastCountryRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Country repository.
    /// </summary>
    public class ForecastCountryRepository : IForecastCountryRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastCountryRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastCountryRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastCountryModel> GetAll()
        {
            return this.context.forecast_country_t.Select(Mapper.Map<ForecastCountryModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastCountry by ForecastId CntryCd
        /// </summary>
		/// <param name="forecast_id">The forecast_id of the entity.</param>/// <param name="cntry_cd">The cntry_cd of the entity.</param>        
        /// <returns>A Forecast Country.</returns>
        public ForecastCountryModel GetByForecastIdCntryCd(Int32 forecast_id,String cntry_cd)
        {
            return Mapper.Map<ForecastCountryModel>(this.context.forecast_country_t.Where(p => p.forecast_id == ForecastId && p.cntry_cd == CntryCd));
        }
       	    public List<ForecastCountryModel> GetByForecastId(Int32 forecastId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastCountryModel model)
        {
            this.context.forecast_country_t.Remove(Mapper.Map<forecast_country_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastCountryModel model)
        {
            var record = this.context.forecast_country_t.Where(p => p.forecast_id == model.ForecastId && p.cntry_cd == model.CntryCd).FirstOrDefault();
            Mapper.Map<ForecastCountryModel, forecast_country_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastCountryModel model)
        {
            this.context.forecast_country_t.Add(Mapper.Map<forecast_country_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}