
// -----------------------------------------------------------------------
// <copyright file="BatchImportMatchMessagesRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Batch Import Match Messages repository.
    /// </summary>
    public class BatchImportMatchMessagesRepository : IBatchImportMatchMessagesRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchImportMatchMessagesRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BatchImportMatchMessagesRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BatchImportMatchMessagesModel> GetAll()
        {
            return this.context.batch_import_match_messages_t.Select(Mapper.Map<BatchImportMatchMessagesModel>).ToList();
        }

			    public BatchImportMatchMessagesModel GetById(Int32 id) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BatchImportMatchMessagesModel model)
        {
            this.context.batch_import_match_messages_t.Remove(Mapper.Map<batch_import_match_messages_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BatchImportMatchMessagesModel model)
        {
            var record = this.context.batch_import_match_messages_t.Where(p => ).FirstOrDefault();
            Mapper.Map<BatchImportMatchMessagesModel, batch_import_match_messages_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BatchImportMatchMessagesModel model)
        {
            this.context.batch_import_match_messages_t.Add(Mapper.Map<batch_import_match_messages_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}