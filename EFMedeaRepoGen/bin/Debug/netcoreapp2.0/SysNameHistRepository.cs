
// -----------------------------------------------------------------------
// <copyright file="SysNameHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Name Hist repository.
    /// </summary>
    public class SysNameHistRepository : ISysNameHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysNameHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysNameHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysNameHistModel> GetAll()
        {
            return this.context.sys_name_hist_t.Select(Mapper.Map<SysNameHistModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysNameHist by SysId NewNameDt
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="new_name_dt">The new_name_dt of the entity.</param>        
        /// <returns>A Sys Name Hist.</returns>
        public SysNameHistModel GetBySysIdNewNameDt(String sys_id,DateTime new_name_dt)
        {
            return Mapper.Map<SysNameHistModel>(this.context.sys_name_hist_t.Where(p => p.sys_id == SysId && p.new_name_dt == NewNameDt));
        }
       	    public SysNameHistModel GetBySysId(String sysId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysNameHistModel model)
        {
            this.context.sys_name_hist_t.Remove(Mapper.Map<sys_name_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysNameHistModel model)
        {
            var record = this.context.sys_name_hist_t.Where(p => p.sys_id == model.SysId && p.new_name_dt == model.NewNameDt).FirstOrDefault();
            Mapper.Map<SysNameHistModel, sys_name_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysNameHistModel model)
        {
            this.context.sys_name_hist_t.Add(Mapper.Map<sys_name_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}