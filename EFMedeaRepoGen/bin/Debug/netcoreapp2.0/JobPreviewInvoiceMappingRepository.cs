
// -----------------------------------------------------------------------
// <copyright file="JobPreviewInvoiceMappingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Preview Invoice Mapping repository.
    /// </summary>
    public class JobPreviewInvoiceMappingRepository : IJobPreviewInvoiceMappingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobPreviewInvoiceMappingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobPreviewInvoiceMappingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobPreviewInvoiceMappingModel> GetAll()
        {
            return this.context.job_preview_invoice_mapping_t.Select(Mapper.Map<JobPreviewInvoiceMappingModel>).ToList();
        }

			    public List<JobPreviewInvoiceMappingModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobPreviewInvoiceMappingModel model)
        {
            this.context.job_preview_invoice_mapping_t.Remove(Mapper.Map<job_preview_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobPreviewInvoiceMappingModel model)
        {
            var record = this.context.job_preview_invoice_mapping_t.Where(p => ).FirstOrDefault();
            Mapper.Map<JobPreviewInvoiceMappingModel, job_preview_invoice_mapping_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobPreviewInvoiceMappingModel model)
        {
            this.context.job_preview_invoice_mapping_t.Add(Mapper.Map<job_preview_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}