
// -----------------------------------------------------------------------
// <copyright file="ContractUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract User Def repository.
    /// </summary>
    public class ContractUserDefRepository : IContractUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractUserDefModel> GetAll()
        {
            return this.context.contract_user_def_t.Select(Mapper.Map<ContractUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractUserDef by ContractNb UserDefCd RowNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Contract User Def.</returns>
        public ContractUserDefModel GetByContractNbUserDefCdRowNb(String contract_nb,String user_def_cd,Int32 row_nb)
        {
            return Mapper.Map<ContractUserDefModel>(this.context.contract_user_def_t.Where(p => p.contract_nb == ContractNb && p.user_def_cd == UserDefCd && p.row_nb == RowNb));
        }
       	    public ContractUserDefModel GetByRowContractUserDef(Int32 rowNb, String contractNb, String userDefCd) {
			throw new NotImplementedException();
		}
		public List<ContractUserDefModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractUserDefModel model)
        {
            this.context.contract_user_def_t.Remove(Mapper.Map<contract_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractUserDefModel model)
        {
            var record = this.context.contract_user_def_t.Where(p => p.contract_nb == model.ContractNb && p.user_def_cd == model.UserDefCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<ContractUserDefModel, contract_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractUserDefModel model)
        {
            this.context.contract_user_def_t.Add(Mapper.Map<contract_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}