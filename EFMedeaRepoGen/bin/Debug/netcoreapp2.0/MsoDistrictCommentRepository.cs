
// -----------------------------------------------------------------------
// <copyright file="MsoDistrictCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso District Comment repository.
    /// </summary>
    public class MsoDistrictCommentRepository : IMsoDistrictCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoDistrictCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoDistrictCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoDistrictCommentModel> GetAll()
        {
            return this.context.mso_district_comment_t.Select(Mapper.Map<MsoDistrictCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoDistrictComment by CommentNb MsoNb DistrictNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="district_nb">The district_nb of the entity.</param>        
        /// <returns>A Mso District Comment.</returns>
        public MsoDistrictCommentModel GetByCommentNbMsoNbDistrictNb(String comment_nb,String mso_nb,String district_nb)
        {
            return Mapper.Map<MsoDistrictCommentModel>(this.context.mso_district_comment_t.Where(p => p.comment_nb == CommentNb && p.mso_nb == MsoNb && p.district_nb == DistrictNb));
        }
       	    public List<MsoDistrictCommentModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoDistrictCommentModel> GetByMsoNbDistrictNb(String msoNb, String districtNb) {
			throw new NotImplementedException();
		}
		public MsoDistrictCommentModel GetByMsoNbDistrictNbCommentNb(String msoNb, String districtNb, String commentNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoDistrictCommentModel model)
        {
            this.context.mso_district_comment_t.Remove(Mapper.Map<mso_district_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoDistrictCommentModel model)
        {
            var record = this.context.mso_district_comment_t.Where(p => p.comment_nb == model.CommentNb && p.mso_nb == model.MsoNb && p.district_nb == model.DistrictNb).FirstOrDefault();
            Mapper.Map<MsoDistrictCommentModel, mso_district_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoDistrictCommentModel model)
        {
            this.context.mso_district_comment_t.Add(Mapper.Map<mso_district_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}