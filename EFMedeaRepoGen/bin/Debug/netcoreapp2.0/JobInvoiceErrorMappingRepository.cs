
// -----------------------------------------------------------------------
// <copyright file="JobInvoiceErrorMappingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Invoice Error Mapping repository.
    /// </summary>
    public class JobInvoiceErrorMappingRepository : IJobInvoiceErrorMappingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobInvoiceErrorMappingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobInvoiceErrorMappingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobInvoiceErrorMappingModel> GetAll()
        {
            return this.context.job_invoice_error_mapping_t.Select(Mapper.Map<JobInvoiceErrorMappingModel>).ToList();
        }

			    public List<JobInvoiceErrorMappingModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobInvoiceErrorMappingModel model)
        {
            this.context.job_invoice_error_mapping_t.Remove(Mapper.Map<job_invoice_error_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobInvoiceErrorMappingModel model)
        {
            var record = this.context.job_invoice_error_mapping_t.Where(p => ).FirstOrDefault();
            Mapper.Map<JobInvoiceErrorMappingModel, job_invoice_error_mapping_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobInvoiceErrorMappingModel model)
        {
            this.context.job_invoice_error_mapping_t.Add(Mapper.Map<job_invoice_error_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}