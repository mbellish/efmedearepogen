
// -----------------------------------------------------------------------
// <copyright file="ContactUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact User Def repository.
    /// </summary>
    public class ContactUserDefRepository : IContactUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactUserDefModel> GetAll()
        {
            return this.context.contact_user_def_t.Select(Mapper.Map<ContactUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactUserDef by ContactNb UserDefCd RowNb
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Contact User Def.</returns>
        public ContactUserDefModel GetByContactNbUserDefCdRowNb(String contact_nb,String user_def_cd,Int32 row_nb)
        {
            return Mapper.Map<ContactUserDefModel>(this.context.contact_user_def_t.Where(p => p.contact_nb == ContactNb && p.user_def_cd == UserDefCd && p.row_nb == RowNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactUserDefModel model)
        {
            this.context.contact_user_def_t.Remove(Mapper.Map<contact_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactUserDefModel model)
        {
            var record = this.context.contact_user_def_t.Where(p => p.contact_nb == model.ContactNb && p.user_def_cd == model.UserDefCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<ContactUserDefModel, contact_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactUserDefModel model)
        {
            this.context.contact_user_def_t.Add(Mapper.Map<contact_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}