
// -----------------------------------------------------------------------
// <copyright file="ForecastSrvcSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Srvc Sub repository.
    /// </summary>
    public class ForecastSrvcSubRepository : IForecastSrvcSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastSrvcSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastSrvcSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastSrvcSubModel> GetAll()
        {
            return this.context.forecast_srvc_sub_t.Select(Mapper.Map<ForecastSrvcSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastSrvcSub by ForecastSrvcSubId
        /// </summary>
		/// <param name="forecast_srvc_sub_id">The forecast_srvc_sub_id of the entity.</param>        
        /// <returns>A Forecast Srvc Sub.</returns>
        public ForecastSrvcSubModel GetByForecastSrvcSubId(Int32 forecast_srvc_sub_id)
        {
            return Mapper.Map<ForecastSrvcSubModel>(this.context.forecast_srvc_sub_t.Where(p => p.forecast_srvc_sub_id == ForecastSrvcSubId));
        }
       	    public IList<ForecastSrvcSubModel> GetByForecastIdSysIdProgSrvcNm(Int32 forecastId, String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		public IList<ForecastSrvcSubModel> GetByForecastContractLaunchId(Int32 forecastContractLaunchId) {
			throw new NotImplementedException();
		}
		public IList<ForecastSrvcSubModel> GetByForecastLaunchId(Int32 forecastLaunchId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastSrvcSubModel model)
        {
            this.context.forecast_srvc_sub_t.Remove(Mapper.Map<forecast_srvc_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastSrvcSubModel model)
        {
            var record = this.context.forecast_srvc_sub_t.Where(p => p.forecast_srvc_sub_id == model.ForecastSrvcSubId).FirstOrDefault();
            Mapper.Map<ForecastSrvcSubModel, forecast_srvc_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastSrvcSubModel model)
        {
            this.context.forecast_srvc_sub_t.Add(Mapper.Map<forecast_srvc_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}