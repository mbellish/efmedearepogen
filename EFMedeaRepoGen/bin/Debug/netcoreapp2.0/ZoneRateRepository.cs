
// -----------------------------------------------------------------------
// <copyright file="ZoneRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Zone Rate repository.
    /// </summary>
    public class ZoneRateRepository : IZoneRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ZoneRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ZoneRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ZoneRateModel> GetAll()
        {
            return this.context.zone_rate_t.Select(Mapper.Map<ZoneRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a ZoneRate by ContractNb CarriageId StartDt SubCd ZoneCd PenetratePercent
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="zone_cd">The zone_cd of the entity.</param>/// <param name="penetrate_percent">The penetrate_percent of the entity.</param>        
        /// <returns>A Zone Rate.</returns>
        public ZoneRateModel GetByContractNbCarriageIdStartDtSubCdZoneCdPenetratePercent(String contract_nb,String carriage_id,DateTime start_dt,String sub_cd,String zone_cd,Double penetrate_percent)
        {
            return Mapper.Map<ZoneRateModel>(this.context.zone_rate_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId && p.start_dt == StartDt && p.sub_cd == SubCd && p.zone_cd == ZoneCd && p.penetrate_percent == PenetratePercent));
        }
       	    public List<ZoneRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ZoneRateModel> GetByContractNbCarriageId(String contractNb, String carriageId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ZoneRateModel model)
        {
            this.context.zone_rate_t.Remove(Mapper.Map<zone_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ZoneRateModel model)
        {
            var record = this.context.zone_rate_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId && p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.zone_cd == model.ZoneCd && p.penetrate_percent == model.PenetratePercent).FirstOrDefault();
            Mapper.Map<ZoneRateModel, zone_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ZoneRateModel model)
        {
            this.context.zone_rate_t.Add(Mapper.Map<zone_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}