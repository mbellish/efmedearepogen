
// -----------------------------------------------------------------------
// <copyright file="ArchiveSrvcSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Archive Srvc Sub repository.
    /// </summary>
    public class ArchiveSrvcSubRepository : IArchiveSrvcSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArchiveSrvcSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArchiveSrvcSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArchiveSrvcSubModel> GetAll()
        {
            return this.context.archive_srvc_sub_t.Select(Mapper.Map<ArchiveSrvcSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArchiveSrvcSub by CloseMonth SysId ProgSrvcNm SubDt SubCd
        /// </summary>
		/// <param name="close_month">The close_month of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sub_dt">The sub_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A Archive Srvc Sub.</returns>
        public ArchiveSrvcSubModel GetByCloseMonthSysIdProgSrvcNmSubDtSubCd(DateTime close_month,String sys_id,String prog_srvc_nm,DateTime sub_dt,String sub_cd)
        {
            return Mapper.Map<ArchiveSrvcSubModel>(this.context.archive_srvc_sub_t.Where(p => p.close_month == CloseMonth && p.sys_id == SysId && p.prog_srvc_nm == ProgSrvcNm && p.sub_dt == SubDt && p.sub_cd == SubCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArchiveSrvcSubModel model)
        {
            this.context.archive_srvc_sub_t.Remove(Mapper.Map<archive_srvc_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArchiveSrvcSubModel model)
        {
            var record = this.context.archive_srvc_sub_t.Where(p => p.close_month == model.CloseMonth && p.sys_id == model.SysId && p.prog_srvc_nm == model.ProgSrvcNm && p.sub_dt == model.SubDt && p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<ArchiveSrvcSubModel, archive_srvc_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArchiveSrvcSubModel model)
        {
            this.context.archive_srvc_sub_t.Add(Mapper.Map<archive_srvc_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}