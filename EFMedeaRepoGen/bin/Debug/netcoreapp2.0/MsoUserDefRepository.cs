
// -----------------------------------------------------------------------
// <copyright file="MsoUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso User Def repository.
    /// </summary>
    public class MsoUserDefRepository : IMsoUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoUserDefModel> GetAll()
        {
            return this.context.mso_user_def_t.Select(Mapper.Map<MsoUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoUserDef by MsoNb UserDefCd RowNb
        /// </summary>
		/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Mso User Def.</returns>
        public MsoUserDefModel GetByMsoNbUserDefCdRowNb(String mso_nb,String user_def_cd,Int32 row_nb)
        {
            return Mapper.Map<MsoUserDefModel>(this.context.mso_user_def_t.Where(p => p.mso_nb == MsoNb && p.user_def_cd == UserDefCd && p.row_nb == RowNb));
        }
       	    public List<MsoUserDefModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public MsoUserDefModel GetByRowNbMsoNbUserDefCd(Int32 rowNb, String msoNb, String userDefCd) {
			throw new NotImplementedException();
		}
		public List<MsoUserDefModel> GetByUserDefCdDropDownId(Nullable<Int32> userDefCdDropDownId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoUserDefModel model)
        {
            this.context.mso_user_def_t.Remove(Mapper.Map<mso_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoUserDefModel model)
        {
            var record = this.context.mso_user_def_t.Where(p => p.mso_nb == model.MsoNb && p.user_def_cd == model.UserDefCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<MsoUserDefModel, mso_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoUserDefModel model)
        {
            this.context.mso_user_def_t.Add(Mapper.Map<mso_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}