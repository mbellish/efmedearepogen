
// -----------------------------------------------------------------------
// <copyright file="ExchangeRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Exchange Rate repository.
    /// </summary>
    public class ExchangeRateRepository : IExchangeRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExchangeRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ExchangeRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ExchangeRateModel> GetAll()
        {
            return this.context.exchange_rate_t.Select(Mapper.Map<ExchangeRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a ExchangeRate by ExchangeDt FromCurrencyCd ToCurrencyCd
        /// </summary>
		/// <param name="exchange_dt">The exchange_dt of the entity.</param>/// <param name="from_currency_cd">The from_currency_cd of the entity.</param>/// <param name="to_currency_cd">The to_currency_cd of the entity.</param>        
        /// <returns>A Exchange Rate.</returns>
        public ExchangeRateModel GetByExchangeDtFromCurrencyCdToCurrencyCd(DateTime exchange_dt,String from_currency_cd,String to_currency_cd)
        {
            return Mapper.Map<ExchangeRateModel>(this.context.exchange_rate_t.Where(p => p.exchange_dt == ExchangeDt && p.from_currency_cd == FromCurrencyCd && p.to_currency_cd == ToCurrencyCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ExchangeRateModel model)
        {
            this.context.exchange_rate_t.Remove(Mapper.Map<exchange_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ExchangeRateModel model)
        {
            var record = this.context.exchange_rate_t.Where(p => p.exchange_dt == model.ExchangeDt && p.from_currency_cd == model.FromCurrencyCd && p.to_currency_cd == model.ToCurrencyCd).FirstOrDefault();
            Mapper.Map<ExchangeRateModel, exchange_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ExchangeRateModel model)
        {
            this.context.exchange_rate_t.Add(Mapper.Map<exchange_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}