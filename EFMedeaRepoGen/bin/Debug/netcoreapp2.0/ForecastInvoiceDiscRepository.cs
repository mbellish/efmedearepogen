
// -----------------------------------------------------------------------
// <copyright file="ForecastInvoiceDiscRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Invoice Disc repository.
    /// </summary>
    public class ForecastInvoiceDiscRepository : IForecastInvoiceDiscRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastInvoiceDiscRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastInvoiceDiscRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastInvoiceDiscModel> GetAll()
        {
            return this.context.forecast_invoice_disc_t.Select(Mapper.Map<ForecastInvoiceDiscModel>).ToList();
        }

			    public List<ForecastInvoiceDiscModel> GetByForecastId(Nullable<Int32> forecastId) {
			throw new NotImplementedException();
		}
		public List<ForecastInvoiceDiscModel> GetByForecastIdInvoiceNb(Nullable<Int32> forecastId, String invoiceNb) {
			throw new NotImplementedException();
		}
		public ForecastInvoiceDiscModel GetById(Int32 id) {
			throw new NotImplementedException();
		}
		public ForecastInvoiceDiscModel GetByInvoiceNbInvoiceRecNbInvoiceDiscNb(String invoiceNb, String invoiceRecNb, String invoiceDiscNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastInvoiceDiscModel model)
        {
            this.context.forecast_invoice_disc_t.Remove(Mapper.Map<forecast_invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastInvoiceDiscModel model)
        {
            var record = this.context.forecast_invoice_disc_t.Where(p => ).FirstOrDefault();
            Mapper.Map<ForecastInvoiceDiscModel, forecast_invoice_disc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastInvoiceDiscModel model)
        {
            this.context.forecast_invoice_disc_t.Add(Mapper.Map<forecast_invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}