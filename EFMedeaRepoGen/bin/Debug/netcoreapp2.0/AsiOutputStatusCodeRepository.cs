
// -----------------------------------------------------------------------
// <copyright file="AsiOutputStatusCodeRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Asi Output Status Code repository.
    /// </summary>
    public class AsiOutputStatusCodeRepository : IAsiOutputStatusCodeRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsiOutputStatusCodeRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public AsiOutputStatusCodeRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<AsiOutputStatusCodeModel> GetAll()
        {
            return this.context.asi_output_status_code_t.Select(Mapper.Map<AsiOutputStatusCodeModel>).ToList();
        }

		        /// <summary>
        /// Gets a AsiOutputStatusCode by AsiOutputStatusCd
        /// </summary>
		/// <param name="asi_output_status_cd">The asi_output_status_cd of the entity.</param>        
        /// <returns>A Asi Output Status Code.</returns>
        public AsiOutputStatusCodeModel GetByAsiOutputStatusCd(String asi_output_status_cd)
        {
            return Mapper.Map<AsiOutputStatusCodeModel>(this.context.asi_output_status_code_t.Where(p => p.asi_output_status_cd == AsiOutputStatusCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(AsiOutputStatusCodeModel model)
        {
            this.context.asi_output_status_code_t.Remove(Mapper.Map<asi_output_status_code_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(AsiOutputStatusCodeModel model)
        {
            var record = this.context.asi_output_status_code_t.Where(p => p.asi_output_status_cd == model.AsiOutputStatusCd).FirstOrDefault();
            Mapper.Map<AsiOutputStatusCodeModel, asi_output_status_code_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(AsiOutputStatusCodeModel model)
        {
            this.context.asi_output_status_code_t.Add(Mapper.Map<asi_output_status_code_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}