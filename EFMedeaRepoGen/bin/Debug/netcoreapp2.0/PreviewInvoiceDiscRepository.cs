
// -----------------------------------------------------------------------
// <copyright file="PreviewInvoiceDiscRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Preview Invoice Disc repository.
    /// </summary>
    public class PreviewInvoiceDiscRepository : IPreviewInvoiceDiscRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewInvoiceDiscRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public PreviewInvoiceDiscRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<PreviewInvoiceDiscModel> GetAll()
        {
            return this.context.preview_invoice_disc_t.Select(Mapper.Map<PreviewInvoiceDiscModel>).ToList();
        }

		        /// <summary>
        /// Gets a PreviewInvoiceDisc by InvoiceNb InvoiceRecNb InvoiceDiscNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>/// <param name="invoice_disc_nb">The invoice_disc_nb of the entity.</param>        
        /// <returns>A Preview Invoice Disc.</returns>
        public PreviewInvoiceDiscModel GetByInvoiceNbInvoiceRecNbInvoiceDiscNb(Int32 invoice_nb,String invoice_rec_nb,Int32 invoice_disc_nb)
        {
            return Mapper.Map<PreviewInvoiceDiscModel>(this.context.preview_invoice_disc_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb && p.invoice_disc_nb == InvoiceDiscNb));
        }
       	    public List<PreviewInvoiceDiscModel> GetByInvoiceNb(Int32 invoiceNb) {
			throw new NotImplementedException();
		}
		public List<PreviewInvoiceDiscModel> GetByInvoiceNbInvoiceRecNb(Int32 invoiceNb, String invoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(PreviewInvoiceDiscModel model)
        {
            this.context.preview_invoice_disc_t.Remove(Mapper.Map<preview_invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(PreviewInvoiceDiscModel model)
        {
            var record = this.context.preview_invoice_disc_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb && p.invoice_disc_nb == model.InvoiceDiscNb).FirstOrDefault();
            Mapper.Map<PreviewInvoiceDiscModel, preview_invoice_disc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(PreviewInvoiceDiscModel model)
        {
            this.context.preview_invoice_disc_t.Add(Mapper.Map<preview_invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}