
// -----------------------------------------------------------------------
// <copyright file="CableModemRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Cable Modem repository.
    /// </summary>
    public class CableModemRepository : ICableModemRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CableModemRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CableModemRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CableModemModel> GetAll()
        {
            return this.context.cable_modem_t.Select(Mapper.Map<CableModemModel>).ToList();
        }

		        /// <summary>
        /// Gets a CableModem by SysId
        /// </summary>
		/// <param name="sys_id">The sys_id of the entity.</param>        
        /// <returns>A Cable Modem.</returns>
        public CableModemModel GetBySysId(String sys_id)
        {
            return Mapper.Map<CableModemModel>(this.context.cable_modem_t.Where(p => p.sys_id == SysId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CableModemModel model)
        {
            this.context.cable_modem_t.Remove(Mapper.Map<cable_modem_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CableModemModel model)
        {
            var record = this.context.cable_modem_t.Where(p => p.sys_id == model.SysId).FirstOrDefault();
            Mapper.Map<CableModemModel, cable_modem_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CableModemModel model)
        {
            this.context.cable_modem_t.Add(Mapper.Map<cable_modem_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}