
// -----------------------------------------------------------------------
// <copyright file="CurrencyCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Currency Cd repository.
    /// </summary>
    public class CurrencyCdRepository : ICurrencyCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CurrencyCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CurrencyCdModel> GetAll()
        {
            return this.context.currency_cd_t.Select(Mapper.Map<CurrencyCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a CurrencyCd by CurrencyCd
        /// </summary>
		/// <param name="currency_cd">The currency_cd of the entity.</param>        
        /// <returns>A Currency Cd.</returns>
        public CurrencyCdModel GetByCurrencyCd(String currency_cd)
        {
            return Mapper.Map<CurrencyCdModel>(this.context.currency_cd_t.Where(p => p.currency_cd == CurrencyCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CurrencyCdModel model)
        {
            this.context.currency_cd_t.Remove(Mapper.Map<currency_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CurrencyCdModel model)
        {
            var record = this.context.currency_cd_t.Where(p => p.currency_cd == model.CurrencyCd).FirstOrDefault();
            Mapper.Map<CurrencyCdModel, currency_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CurrencyCdModel model)
        {
            this.context.currency_cd_t.Add(Mapper.Map<currency_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}