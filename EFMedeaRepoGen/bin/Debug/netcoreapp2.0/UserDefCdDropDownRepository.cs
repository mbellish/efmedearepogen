
// -----------------------------------------------------------------------
// <copyright file="UserDefCdDropDownRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User Def Cd Drop Down repository.
    /// </summary>
    public class UserDefCdDropDownRepository : IUserDefCdDropDownRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefCdDropDownRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserDefCdDropDownRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserDefCdDropDownModel> GetAll()
        {
            return this.context.user_def_cd_drop_down_t.Select(Mapper.Map<UserDefCdDropDownModel>).ToList();
        }

		        /// <summary>
        /// Gets a UserDefCdDropDown by UserDefCdDropDownId
        /// </summary>
		/// <param name="user_def_cd_drop_down_id">The user_def_cd_drop_down_id of the entity.</param>        
        /// <returns>A User Def Cd Drop Down.</returns>
        public UserDefCdDropDownModel GetByUserDefCdDropDownId(Int32 user_def_cd_drop_down_id)
        {
            return Mapper.Map<UserDefCdDropDownModel>(this.context.user_def_cd_drop_down_t.Where(p => p.user_def_cd_drop_down_id == UserDefCdDropDownId));
        }
       	    public List<UserDefCdDropDownModel> GetByEntityTypUserDefCd(String entityTyp, String userDefCd) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserDefCdDropDownModel model)
        {
            this.context.user_def_cd_drop_down_t.Remove(Mapper.Map<user_def_cd_drop_down_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserDefCdDropDownModel model)
        {
            var record = this.context.user_def_cd_drop_down_t.Where(p => p.user_def_cd_drop_down_id == model.UserDefCdDropDownId).FirstOrDefault();
            Mapper.Map<UserDefCdDropDownModel, user_def_cd_drop_down_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserDefCdDropDownModel model)
        {
            this.context.user_def_cd_drop_down_t.Add(Mapper.Map<user_def_cd_drop_down_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}