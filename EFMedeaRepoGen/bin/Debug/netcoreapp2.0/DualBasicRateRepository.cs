
// -----------------------------------------------------------------------
// <copyright file="DualBasicRateRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Dual Basic Rate repository.
    /// </summary>
    public class DualBasicRateRepository : IDualBasicRateRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="DualBasicRateRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public DualBasicRateRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<DualBasicRateModel> GetAll()
        {
            return this.context.dual_basic_rate_t.Select(Mapper.Map<DualBasicRateModel>).ToList();
        }

		        /// <summary>
        /// Gets a DualBasicRate by StartDt SubCd BillablePen TotalPen ContractNb CarriageId
        /// </summary>
		/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>/// <param name="billable_pen">The billable_pen of the entity.</param>/// <param name="total_pen">The total_pen of the entity.</param>/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>        
        /// <returns>A Dual Basic Rate.</returns>
        public DualBasicRateModel GetByStartDtSubCdBillablePenTotalPenContractNbCarriageId(DateTime start_dt,String sub_cd,Double billable_pen,Double total_pen,String contract_nb,String carriage_id)
        {
            return Mapper.Map<DualBasicRateModel>(this.context.dual_basic_rate_t.Where(p => p.start_dt == StartDt && p.sub_cd == SubCd && p.billable_pen == BillablePen && p.total_pen == TotalPen && p.contract_nb == ContractNb && p.carriage_id == CarriageId));
        }
       	    public List<DualBasicRateModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<DualBasicRateModel> GetByContractNbCarriageId(String contractNb, String carriageID) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(DualBasicRateModel model)
        {
            this.context.dual_basic_rate_t.Remove(Mapper.Map<dual_basic_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(DualBasicRateModel model)
        {
            var record = this.context.dual_basic_rate_t.Where(p => p.start_dt == model.StartDt && p.sub_cd == model.SubCd && p.billable_pen == model.BillablePen && p.total_pen == model.TotalPen && p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId).FirstOrDefault();
            Mapper.Map<DualBasicRateModel, dual_basic_rate_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(DualBasicRateModel model)
        {
            this.context.dual_basic_rate_t.Add(Mapper.Map<dual_basic_rate_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}