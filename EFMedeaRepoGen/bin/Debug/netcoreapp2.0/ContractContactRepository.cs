
// -----------------------------------------------------------------------
// <copyright file="ContractContactRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Contact repository.
    /// </summary>
    public class ContractContactRepository : IContractContactRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractContactRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractContactRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractContactModel> GetAll()
        {
            return this.context.contract_contact_t.Select(Mapper.Map<ContractContactModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractContact by ContractContactId
        /// </summary>
		/// <param name="contract_contact_id">The contract_contact_id of the entity.</param>        
        /// <returns>A Contract Contact.</returns>
        public ContractContactModel GetByContractContactId(Int32 contract_contact_id)
        {
            return Mapper.Map<ContractContactModel>(this.context.contract_contact_t.Where(p => p.contract_contact_id == ContractContactId));
        }
       	    public List<ContractContactModel> GetByContractNbBillingNb(String contractNb, String billingNb) {
			throw new NotImplementedException();
		}
		public List<ContractContactModel> GetByContactNb(String contactNb) {
			throw new NotImplementedException();
		}
		public List<ContractContactModel> GetByContractNbBillingNbContactNb(String contractNb, String billingNb, String contactNb) {
			throw new NotImplementedException();
		}
		public ContractContactModel GetByContractNbBillingNbContactNbMsoNbClassification(String contractNb, String billingNb, String contactNb, String msoNb, String classification) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractContactModel model)
        {
            this.context.contract_contact_t.Remove(Mapper.Map<contract_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractContactModel model)
        {
            var record = this.context.contract_contact_t.Where(p => p.contract_contact_id == model.ContractContactId).FirstOrDefault();
            Mapper.Map<ContractContactModel, contract_contact_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractContactModel model)
        {
            this.context.contract_contact_t.Add(Mapper.Map<contract_contact_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}