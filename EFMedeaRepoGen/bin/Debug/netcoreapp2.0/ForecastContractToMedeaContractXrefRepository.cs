
// -----------------------------------------------------------------------
// <copyright file="ForecastContractToMedeaContractXrefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Contract To Medea Contract Xref repository.
    /// </summary>
    public class ForecastContractToMedeaContractXrefRepository : IForecastContractToMedeaContractXrefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastContractToMedeaContractXrefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastContractToMedeaContractXrefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastContractToMedeaContractXrefModel> GetAll()
        {
            return this.context.forecast_contract_to_medea_contract_xref_t.Select(Mapper.Map<ForecastContractToMedeaContractXrefModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastContractToMedeaContractXref by ForecastContractNb
        /// </summary>
		/// <param name="forecast_contract_nb">The forecast_contract_nb of the entity.</param>        
        /// <returns>A Forecast Contract To Medea Contract Xref.</returns>
        public ForecastContractToMedeaContractXrefModel GetByForecastContractNb(String forecast_contract_nb)
        {
            return Mapper.Map<ForecastContractToMedeaContractXrefModel>(this.context.forecast_contract_to_medea_contract_xref_t.Where(p => p.forecast_contract_nb == ForecastContractNb));
        }
       	    public ForecastContractToMedeaContractXrefModel GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<ForecastContractToMedeaContractXrefModel> GetManyByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastContractToMedeaContractXrefModel model)
        {
            this.context.forecast_contract_to_medea_contract_xref_t.Remove(Mapper.Map<forecast_contract_to_medea_contract_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastContractToMedeaContractXrefModel model)
        {
            var record = this.context.forecast_contract_to_medea_contract_xref_t.Where(p => p.forecast_contract_nb == model.ForecastContractNb).FirstOrDefault();
            Mapper.Map<ForecastContractToMedeaContractXrefModel, forecast_contract_to_medea_contract_xref_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastContractToMedeaContractXrefModel model)
        {
            this.context.forecast_contract_to_medea_contract_xref_t.Add(Mapper.Map<forecast_contract_to_medea_contract_xref_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}