
// -----------------------------------------------------------------------
// <copyright file="UserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The User Def repository.
    /// </summary>
    public class UserDefRepository : IUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public UserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<UserDefModel> GetAll()
        {
            return this.context.user_def_t.Select(Mapper.Map<UserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a UserDef by EntityTyp UserDefCd
        /// </summary>
		/// <param name="entity_typ">The entity_typ of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>        
        /// <returns>A User Def.</returns>
        public UserDefModel GetByEntityTypUserDefCd(String entity_typ,String user_def_cd)
        {
            return Mapper.Map<UserDefModel>(this.context.user_def_t.Where(p => p.entity_typ == EntityTyp && p.user_def_cd == UserDefCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(UserDefModel model)
        {
            this.context.user_def_t.Remove(Mapper.Map<user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(UserDefModel model)
        {
            var record = this.context.user_def_t.Where(p => p.entity_typ == model.EntityTyp && p.user_def_cd == model.UserDefCd).FirstOrDefault();
            Mapper.Map<UserDefModel, user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(UserDefModel model)
        {
            this.context.user_def_t.Add(Mapper.Map<user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}