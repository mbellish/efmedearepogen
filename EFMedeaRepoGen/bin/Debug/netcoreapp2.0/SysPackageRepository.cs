
// -----------------------------------------------------------------------
// <copyright file="SysPackageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Package repository.
    /// </summary>
    public class SysPackageRepository : ISysPackageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysPackageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysPackageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysPackageModel> GetAll()
        {
            return this.context.sys_package_t.Select(Mapper.Map<SysPackageModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysPackage by SysPackageId
        /// </summary>
		/// <param name="sys_package_id">The sys_package_id of the entity.</param>        
        /// <returns>A Sys Package.</returns>
        public SysPackageModel GetBySysPackageId(Int32 sys_package_id)
        {
            return Mapper.Map<SysPackageModel>(this.context.sys_package_t.Where(p => p.sys_package_id == SysPackageId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysPackageModel model)
        {
            this.context.sys_package_t.Remove(Mapper.Map<sys_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysPackageModel model)
        {
            var record = this.context.sys_package_t.Where(p => p.sys_package_id == model.SysPackageId).FirstOrDefault();
            Mapper.Map<SysPackageModel, sys_package_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysPackageModel model)
        {
            this.context.sys_package_t.Add(Mapper.Map<sys_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}