
// -----------------------------------------------------------------------
// <copyright file="SalesExecRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sales Exec repository.
    /// </summary>
    public class SalesExecRepository : ISalesExecRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesExecRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SalesExecRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SalesExecModel> GetAll()
        {
            return this.context.sales_exec_t.Select(Mapper.Map<SalesExecModel>).ToList();
        }

		        /// <summary>
        /// Gets a SalesExec by SalesExecId
        /// </summary>
		/// <param name="sales_exec_id">The sales_exec_id of the entity.</param>        
        /// <returns>A Sales Exec.</returns>
        public SalesExecModel GetBySalesExecId(String sales_exec_id)
        {
            return Mapper.Map<SalesExecModel>(this.context.sales_exec_t.Where(p => p.sales_exec_id == SalesExecId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SalesExecModel model)
        {
            this.context.sales_exec_t.Remove(Mapper.Map<sales_exec_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SalesExecModel model)
        {
            var record = this.context.sales_exec_t.Where(p => p.sales_exec_id == model.SalesExecId).FirstOrDefault();
            Mapper.Map<SalesExecModel, sales_exec_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SalesExecModel model)
        {
            this.context.sales_exec_t.Add(Mapper.Map<sales_exec_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}