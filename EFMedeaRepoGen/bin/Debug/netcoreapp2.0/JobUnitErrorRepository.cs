
// -----------------------------------------------------------------------
// <copyright file="JobUnitErrorRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Unit Error repository.
    /// </summary>
    public class JobUnitErrorRepository : IJobUnitErrorRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobUnitErrorRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobUnitErrorRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobUnitErrorModel> GetAll()
        {
            return this.context.job_unit_error_t.Select(Mapper.Map<JobUnitErrorModel>).ToList();
        }

		        /// <summary>
        /// Gets a JobUnitError by JobUnitErrorId
        /// </summary>
		/// <param name="job_unit_error_id">The job_unit_error_id of the entity.</param>        
        /// <returns>A Job Unit Error.</returns>
        public JobUnitErrorModel GetByJobUnitErrorId(Int32 job_unit_error_id)
        {
            return Mapper.Map<JobUnitErrorModel>(this.context.job_unit_error_t.Where(p => p.job_unit_error_id == JobUnitErrorId));
        }
       	    public List<JobUnitErrorModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		public List<JobUnitErrorModel> GetByJobUnitId(Int32 jobUnitId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobUnitErrorModel model)
        {
            this.context.job_unit_error_t.Remove(Mapper.Map<job_unit_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobUnitErrorModel model)
        {
            var record = this.context.job_unit_error_t.Where(p => p.job_unit_error_id == model.JobUnitErrorId).FirstOrDefault();
            Mapper.Map<JobUnitErrorModel, job_unit_error_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobUnitErrorModel model)
        {
            this.context.job_unit_error_t.Add(Mapper.Map<job_unit_error_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}