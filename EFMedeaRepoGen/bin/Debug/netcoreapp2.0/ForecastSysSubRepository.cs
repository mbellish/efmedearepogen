
// -----------------------------------------------------------------------
// <copyright file="ForecastSysSubRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Forecast Sys Sub repository.
    /// </summary>
    public class ForecastSysSubRepository : IForecastSysSubRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastSysSubRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ForecastSysSubRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ForecastSysSubModel> GetAll()
        {
            return this.context.forecast_sys_sub_t.Select(Mapper.Map<ForecastSysSubModel>).ToList();
        }

		        /// <summary>
        /// Gets a ForecastSysSub by ForecastSysSubId
        /// </summary>
		/// <param name="forecast_sys_sub_id">The forecast_sys_sub_id of the entity.</param>        
        /// <returns>A Forecast Sys Sub.</returns>
        public ForecastSysSubModel GetByForecastSysSubId(Int32 forecast_sys_sub_id)
        {
            return Mapper.Map<ForecastSysSubModel>(this.context.forecast_sys_sub_t.Where(p => p.forecast_sys_sub_id == ForecastSysSubId));
        }
       	    public IList<ForecastSysSubModel> GetByForecastIdSysId(Int32 forecastId, String sysId) {
			throw new NotImplementedException();
		}
		public IList<ForecastSysSubModel> GetByForecastLaunchId(Int32 forecastLaunchId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ForecastSysSubModel model)
        {
            this.context.forecast_sys_sub_t.Remove(Mapper.Map<forecast_sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ForecastSysSubModel model)
        {
            var record = this.context.forecast_sys_sub_t.Where(p => p.forecast_sys_sub_id == model.ForecastSysSubId).FirstOrDefault();
            Mapper.Map<ForecastSysSubModel, forecast_sys_sub_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ForecastSysSubModel model)
        {
            this.context.forecast_sys_sub_t.Add(Mapper.Map<forecast_sys_sub_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}