
// -----------------------------------------------------------------------
// <copyright file="SalesExecHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sales Exec Hist repository.
    /// </summary>
    public class SalesExecHistRepository : ISalesExecHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesExecHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SalesExecHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SalesExecHistModel> GetAll()
        {
            return this.context.sales_exec_hist_t.Select(Mapper.Map<SalesExecHistModel>).ToList();
        }

		        /// <summary>
        /// Gets a SalesExecHist by SalesExecId SysId AssignDt
        /// </summary>
		/// <param name="sales_exec_id">The sales_exec_id of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="assign_dt">The assign_dt of the entity.</param>        
        /// <returns>A Sales Exec Hist.</returns>
        public SalesExecHistModel GetBySalesExecIdSysIdAssignDt(String sales_exec_id,String sys_id,DateTime assign_dt)
        {
            return Mapper.Map<SalesExecHistModel>(this.context.sales_exec_hist_t.Where(p => p.sales_exec_id == SalesExecId && p.sys_id == SysId && p.assign_dt == AssignDt));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SalesExecHistModel model)
        {
            this.context.sales_exec_hist_t.Remove(Mapper.Map<sales_exec_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SalesExecHistModel model)
        {
            var record = this.context.sales_exec_hist_t.Where(p => p.sales_exec_id == model.SalesExecId && p.sys_id == model.SysId && p.assign_dt == model.AssignDt).FirstOrDefault();
            Mapper.Map<SalesExecHistModel, sales_exec_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SalesExecHistModel model)
        {
            this.context.sales_exec_hist_t.Add(Mapper.Map<sales_exec_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}