
// -----------------------------------------------------------------------
// <copyright file="CommitmentCdDropDownValidCountriesRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Commitment Cd Drop Down Valid Countries repository.
    /// </summary>
    public class CommitmentCdDropDownValidCountriesRepository : ICommitmentCdDropDownValidCountriesRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitmentCdDropDownValidCountriesRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommitmentCdDropDownValidCountriesRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommitmentCdDropDownValidCountriesModel> GetAll()
        {
            return this.context.commitment_cd_drop_down_valid_countries_t.Select(Mapper.Map<CommitmentCdDropDownValidCountriesModel>).ToList();
        }

		        /// <summary>
        /// Gets a CommitmentCdDropDownValidCountries by CommitmentCdDropDownValidCountryId
        /// </summary>
		/// <param name="commitment_cd_drop_down_valid_country_id">The commitment_cd_drop_down_valid_country_id of the entity.</param>        
        /// <returns>A Commitment Cd Drop Down Valid Countries.</returns>
        public CommitmentCdDropDownValidCountriesModel GetByCommitmentCdDropDownValidCountryId(Int32 commitment_cd_drop_down_valid_country_id)
        {
            return Mapper.Map<CommitmentCdDropDownValidCountriesModel>(this.context.commitment_cd_drop_down_valid_countries_t.Where(p => p.commitment_cd_drop_down_valid_country_id == CommitmentCdDropDownValidCountryId));
        }
       	    public List<CommitmentCdDropDownValidCountriesModel> GetByCommitmentCdDropDownId(Int32 commitmentCdDropDownId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommitmentCdDropDownValidCountriesModel model)
        {
            this.context.commitment_cd_drop_down_valid_countries_t.Remove(Mapper.Map<commitment_cd_drop_down_valid_countries_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommitmentCdDropDownValidCountriesModel model)
        {
            var record = this.context.commitment_cd_drop_down_valid_countries_t.Where(p => p.commitment_cd_drop_down_valid_country_id == model.CommitmentCdDropDownValidCountryId).FirstOrDefault();
            Mapper.Map<CommitmentCdDropDownValidCountriesModel, commitment_cd_drop_down_valid_countries_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommitmentCdDropDownValidCountriesModel model)
        {
            this.context.commitment_cd_drop_down_valid_countries_t.Add(Mapper.Map<commitment_cd_drop_down_valid_countries_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}