
// -----------------------------------------------------------------------
// <copyright file="ContactTitleRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contact Title repository.
    /// </summary>
    public class ContactTitleRepository : IContactTitleRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactTitleRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContactTitleRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContactTitleModel> GetAll()
        {
            return this.context.contact_title_t.Select(Mapper.Map<ContactTitleModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContactTitle by ContactNb TitleCd
        /// </summary>
		/// <param name="contact_nb">The contact_nb of the entity.</param>/// <param name="title_cd">The title_cd of the entity.</param>        
        /// <returns>A Contact Title.</returns>
        public ContactTitleModel GetByContactNbTitleCd(String contact_nb,String title_cd)
        {
            return Mapper.Map<ContactTitleModel>(this.context.contact_title_t.Where(p => p.contact_nb == ContactNb && p.title_cd == TitleCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContactTitleModel model)
        {
            this.context.contact_title_t.Remove(Mapper.Map<contact_title_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContactTitleModel model)
        {
            var record = this.context.contact_title_t.Where(p => p.contact_nb == model.ContactNb && p.title_cd == model.TitleCd).FirstOrDefault();
            Mapper.Map<ContactTitleModel, contact_title_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContactTitleModel model)
        {
            this.context.contact_title_t.Add(Mapper.Map<contact_title_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}