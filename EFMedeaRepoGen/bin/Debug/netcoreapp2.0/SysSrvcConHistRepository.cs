
// -----------------------------------------------------------------------
// <copyright file="SysSrvcConHistRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc Con Hist repository.
    /// </summary>
    public class SysSrvcConHistRepository : ISysSrvcConHistRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcConHistRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcConHistRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcConHistModel> GetAll()
        {
            return this.context.sys_srvc_con_hist_t.Select(Mapper.Map<SysSrvcConHistModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvcConHist by HistoryId
        /// </summary>
		/// <param name="history_id">The history_id of the entity.</param>        
        /// <returns>A Sys Srvc Con Hist.</returns>
        public SysSrvcConHistModel GetByHistoryId(Int32 history_id)
        {
            return Mapper.Map<SysSrvcConHistModel>(this.context.sys_srvc_con_hist_t.Where(p => p.history_id == HistoryId));
        }
       	    public List<SysSrvcConHistModel> GetByProgSrvcNmSysIdChangeDt(String progSrvcNm, String sysId, DateTime changeDt) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcConHistModel model)
        {
            this.context.sys_srvc_con_hist_t.Remove(Mapper.Map<sys_srvc_con_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcConHistModel model)
        {
            var record = this.context.sys_srvc_con_hist_t.Where(p => p.history_id == model.HistoryId).FirstOrDefault();
            Mapper.Map<SysSrvcConHistModel, sys_srvc_con_hist_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcConHistModel model)
        {
            this.context.sys_srvc_con_hist_t.Add(Mapper.Map<sys_srvc_con_hist_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}