
// -----------------------------------------------------------------------
// <copyright file="SrvcPackageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Srvc Package repository.
    /// </summary>
    public class SrvcPackageRepository : ISrvcPackageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SrvcPackageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SrvcPackageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SrvcPackageModel> GetAll()
        {
            return this.context.srvc_package_t.Select(Mapper.Map<SrvcPackageModel>).ToList();
        }

		        /// <summary>
        /// Gets a SrvcPackage by SrvcPackageId
        /// </summary>
		/// <param name="srvc_package_id">The srvc_package_id of the entity.</param>        
        /// <returns>A Srvc Package.</returns>
        public SrvcPackageModel GetBySrvcPackageId(Int32 srvc_package_id)
        {
            return Mapper.Map<SrvcPackageModel>(this.context.srvc_package_t.Where(p => p.srvc_package_id == SrvcPackageId));
        }
       	    public List<SrvcPackageModel> GetByPackageProgSrvcNm(String packageProgSrvcNm) {
			throw new NotImplementedException();
		}
		public SrvcPackageModel GetByPackageProgSrvcNmProgSrvcNm(String packageProgSrvcNm, String progSrvcNm) {
			throw new NotImplementedException();
		}
		public List<SrvcPackageModel> GetByProgSrvcNm(String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SrvcPackageModel model)
        {
            this.context.srvc_package_t.Remove(Mapper.Map<srvc_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SrvcPackageModel model)
        {
            var record = this.context.srvc_package_t.Where(p => p.srvc_package_id == model.SrvcPackageId).FirstOrDefault();
            Mapper.Map<SrvcPackageModel, srvc_package_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SrvcPackageModel model)
        {
            this.context.srvc_package_t.Add(Mapper.Map<srvc_package_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}