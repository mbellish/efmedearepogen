
// -----------------------------------------------------------------------
// <copyright file="ContractCarriageRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Carriage repository.
    /// </summary>
    public class ContractCarriageRepository : IContractCarriageRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCarriageRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCarriageRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCarriageModel> GetAll()
        {
            return this.context.contract_carriage_t.Select(Mapper.Map<ContractCarriageModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCarriage by ContractNb CarriageId
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="carriage_id">The carriage_id of the entity.</param>        
        /// <returns>A Contract Carriage.</returns>
        public ContractCarriageModel GetByContractNbCarriageId(String contract_nb,String carriage_id)
        {
            return Mapper.Map<ContractCarriageModel>(this.context.contract_carriage_t.Where(p => p.contract_nb == ContractNb && p.carriage_id == CarriageId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCarriageModel model)
        {
            this.context.contract_carriage_t.Remove(Mapper.Map<contract_carriage_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCarriageModel model)
        {
            var record = this.context.contract_carriage_t.Where(p => p.contract_nb == model.ContractNb && p.carriage_id == model.CarriageId).FirstOrDefault();
            Mapper.Map<ContractCarriageModel, contract_carriage_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCarriageModel model)
        {
            this.context.contract_carriage_t.Add(Mapper.Map<contract_carriage_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}