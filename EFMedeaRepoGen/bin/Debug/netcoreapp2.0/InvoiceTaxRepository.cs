
// -----------------------------------------------------------------------
// <copyright file="InvoiceTaxRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Tax repository.
    /// </summary>
    public class InvoiceTaxRepository : IInvoiceTaxRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceTaxRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceTaxRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceTaxModel> GetAll()
        {
            return this.context.invoice_tax_t.Select(Mapper.Map<InvoiceTaxModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceTax by InvoiceNb InvoiceRecNb SysInvoiceRecNb TaxCd
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>/// <param name="sys_invoice_rec_nb">The sys_invoice_rec_nb of the entity.</param>/// <param name="tax_cd">The tax_cd of the entity.</param>        
        /// <returns>A Invoice Tax.</returns>
        public InvoiceTaxModel GetByInvoiceNbInvoiceRecNbSysInvoiceRecNbTaxCd(String invoice_nb,String invoice_rec_nb,String sys_invoice_rec_nb,String tax_cd)
        {
            return Mapper.Map<InvoiceTaxModel>(this.context.invoice_tax_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb && p.sys_invoice_rec_nb == SysInvoiceRecNb && p.tax_cd == TaxCd));
        }
       	    public List<InvoiceTaxModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceTaxModel> GetByInvoiceNbSysInvoiceRecNb(String invoiceNb, String sysInvoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceTaxModel model)
        {
            this.context.invoice_tax_t.Remove(Mapper.Map<invoice_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceTaxModel model)
        {
            var record = this.context.invoice_tax_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb && p.sys_invoice_rec_nb == model.SysInvoiceRecNb && p.tax_cd == model.TaxCd).FirstOrDefault();
            Mapper.Map<InvoiceTaxModel, invoice_tax_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceTaxModel model)
        {
            this.context.invoice_tax_t.Add(Mapper.Map<invoice_tax_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}