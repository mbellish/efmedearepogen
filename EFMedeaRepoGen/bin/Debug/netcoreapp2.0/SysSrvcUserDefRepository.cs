
// -----------------------------------------------------------------------
// <copyright file="SysSrvcUserDefRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Sys Srvc User Def repository.
    /// </summary>
    public class SysSrvcUserDefRepository : ISysSrvcUserDefRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SysSrvcUserDefRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SysSrvcUserDefRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SysSrvcUserDefModel> GetAll()
        {
            return this.context.sys_srvc_user_def_t.Select(Mapper.Map<SysSrvcUserDefModel>).ToList();
        }

		        /// <summary>
        /// Gets a SysSrvcUserDef by ProgSrvcNm SysId UserDefCd RowNb
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="sys_id">The sys_id of the entity.</param>/// <param name="user_def_cd">The user_def_cd of the entity.</param>/// <param name="row_nb">The row_nb of the entity.</param>        
        /// <returns>A Sys Srvc User Def.</returns>
        public SysSrvcUserDefModel GetByProgSrvcNmSysIdUserDefCdRowNb(String prog_srvc_nm,String sys_id,String user_def_cd,Int32 row_nb)
        {
            return Mapper.Map<SysSrvcUserDefModel>(this.context.sys_srvc_user_def_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.sys_id == SysId && p.user_def_cd == UserDefCd && p.row_nb == RowNb));
        }
       	    public SysSrvcUserDefModel GetByRowNbProgSrvcNmSysIdUserDefCd(Int32 rowNb, String progSrvcNm, String sysId, String userDefCd) {
			throw new NotImplementedException();
		}
		public List<SysSrvcUserDefModel> GetBySysIdProgSrvcNm(String sysId, String progSrvcNm) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SysSrvcUserDefModel model)
        {
            this.context.sys_srvc_user_def_t.Remove(Mapper.Map<sys_srvc_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SysSrvcUserDefModel model)
        {
            var record = this.context.sys_srvc_user_def_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.sys_id == model.SysId && p.user_def_cd == model.UserDefCd && p.row_nb == model.RowNb).FirstOrDefault();
            Mapper.Map<SysSrvcUserDefModel, sys_srvc_user_def_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SysSrvcUserDefModel model)
        {
            this.context.sys_srvc_user_def_t.Add(Mapper.Map<sys_srvc_user_def_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}