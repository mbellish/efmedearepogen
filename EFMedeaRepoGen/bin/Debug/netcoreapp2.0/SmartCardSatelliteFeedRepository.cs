
// -----------------------------------------------------------------------
// <copyright file="SmartCardSatelliteFeedRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Smart Card Satellite Feed repository.
    /// </summary>
    public class SmartCardSatelliteFeedRepository : ISmartCardSatelliteFeedRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartCardSatelliteFeedRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public SmartCardSatelliteFeedRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<SmartCardSatelliteFeedModel> GetAll()
        {
            return this.context.smart_card_satellite_feed_t.Select(Mapper.Map<SmartCardSatelliteFeedModel>).ToList();
        }

		        /// <summary>
        /// Gets a SmartCardSatelliteFeed by SmartCardId
        /// </summary>
		/// <param name="smart_card_id">The smart_card_id of the entity.</param>        
        /// <returns>A Smart Card Satellite Feed.</returns>
        public SmartCardSatelliteFeedModel GetBySmartCardId(Int32 smart_card_id)
        {
            return Mapper.Map<SmartCardSatelliteFeedModel>(this.context.smart_card_satellite_feed_t.Where(p => p.smart_card_id == SmartCardId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(SmartCardSatelliteFeedModel model)
        {
            this.context.smart_card_satellite_feed_t.Remove(Mapper.Map<smart_card_satellite_feed_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(SmartCardSatelliteFeedModel model)
        {
            var record = this.context.smart_card_satellite_feed_t.Where(p => p.smart_card_id == model.SmartCardId).FirstOrDefault();
            Mapper.Map<SmartCardSatelliteFeedModel, smart_card_satellite_feed_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(SmartCardSatelliteFeedModel model)
        {
            this.context.smart_card_satellite_feed_t.Add(Mapper.Map<smart_card_satellite_feed_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}