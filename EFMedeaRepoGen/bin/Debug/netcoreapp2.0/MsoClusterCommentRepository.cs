
// -----------------------------------------------------------------------
// <copyright file="MsoClusterCommentRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Cluster Comment repository.
    /// </summary>
    public class MsoClusterCommentRepository : IMsoClusterCommentRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoClusterCommentRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoClusterCommentRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoClusterCommentModel> GetAll()
        {
            return this.context.mso_cluster_comment_t.Select(Mapper.Map<MsoClusterCommentModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoClusterComment by CommentNb MsoNb ClusterNb
        /// </summary>
		/// <param name="comment_nb">The comment_nb of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>/// <param name="cluster_nb">The cluster_nb of the entity.</param>        
        /// <returns>A Mso Cluster Comment.</returns>
        public MsoClusterCommentModel GetByCommentNbMsoNbClusterNb(String comment_nb,String mso_nb,String cluster_nb)
        {
            return Mapper.Map<MsoClusterCommentModel>(this.context.mso_cluster_comment_t.Where(p => p.comment_nb == CommentNb && p.mso_nb == MsoNb && p.cluster_nb == ClusterNb));
        }
       	    public List<MsoClusterCommentModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		public List<MsoClusterCommentModel> GetByMsoNbClusterNb(String msoNb, String clusterNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoClusterCommentModel model)
        {
            this.context.mso_cluster_comment_t.Remove(Mapper.Map<mso_cluster_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoClusterCommentModel model)
        {
            var record = this.context.mso_cluster_comment_t.Where(p => p.comment_nb == model.CommentNb && p.mso_nb == model.MsoNb && p.cluster_nb == model.ClusterNb).FirstOrDefault();
            Mapper.Map<MsoClusterCommentModel, mso_cluster_comment_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoClusterCommentModel model)
        {
            this.context.mso_cluster_comment_t.Add(Mapper.Map<mso_cluster_comment_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}