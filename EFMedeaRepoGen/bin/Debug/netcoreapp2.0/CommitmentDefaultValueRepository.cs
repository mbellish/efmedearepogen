
// -----------------------------------------------------------------------
// <copyright file="CommitmentDefaultValueRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Commitment Default Value repository.
    /// </summary>
    public class CommitmentDefaultValueRepository : ICommitmentDefaultValueRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitmentDefaultValueRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public CommitmentDefaultValueRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<CommitmentDefaultValueModel> GetAll()
        {
            return this.context.commitment_default_value_t.Select(Mapper.Map<CommitmentDefaultValueModel>).ToList();
        }

		        /// <summary>
        /// Gets a CommitmentDefaultValue by CommitmentDefaultValueId
        /// </summary>
		/// <param name="commitment_default_value_id">The commitment_default_value_id of the entity.</param>        
        /// <returns>A Commitment Default Value.</returns>
        public CommitmentDefaultValueModel GetByCommitmentDefaultValueId(Int32 commitment_default_value_id)
        {
            return Mapper.Map<CommitmentDefaultValueModel>(this.context.commitment_default_value_t.Where(p => p.commitment_default_value_id == CommitmentDefaultValueId));
        }
       	    public List<CommitmentDefaultValueModel> GetByCommitmentCd(String commitmentCd) {
			throw new NotImplementedException();
		}
		public List<CommitmentDefaultValueModel> GetByCountryCd(String countryCd) {
			throw new NotImplementedException();
		}
		public List<CommitmentDefaultValueModel> GetByDefaultDropDownValueId(Nullable<Int32> defaultDropDownValueId) {
			throw new NotImplementedException();
		}
		public List<CommitmentDefaultValueModel> GetByDropDownValueId(Nullable<Int32> dropDownValueId) {
			throw new NotImplementedException();
		}
		public List<CommitmentDefaultValueModel> GetBySalesRegion(String salesRegion) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(CommitmentDefaultValueModel model)
        {
            this.context.commitment_default_value_t.Remove(Mapper.Map<commitment_default_value_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(CommitmentDefaultValueModel model)
        {
            var record = this.context.commitment_default_value_t.Where(p => p.commitment_default_value_id == model.CommitmentDefaultValueId).FirstOrDefault();
            Mapper.Map<CommitmentDefaultValueModel, commitment_default_value_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(CommitmentDefaultValueModel model)
        {
            this.context.commitment_default_value_t.Add(Mapper.Map<commitment_default_value_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}