
// -----------------------------------------------------------------------
// <copyright file="InvoiceDiscRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Disc repository.
    /// </summary>
    public class InvoiceDiscRepository : IInvoiceDiscRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceDiscRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceDiscRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceDiscModel> GetAll()
        {
            return this.context.invoice_disc_t.Select(Mapper.Map<InvoiceDiscModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceDisc by InvoiceNb InvoiceRecNb InvoiceDiscNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>/// <param name="invoice_rec_nb">The invoice_rec_nb of the entity.</param>/// <param name="invoice_disc_nb">The invoice_disc_nb of the entity.</param>        
        /// <returns>A Invoice Disc.</returns>
        public InvoiceDiscModel GetByInvoiceNbInvoiceRecNbInvoiceDiscNb(String invoice_nb,String invoice_rec_nb,String invoice_disc_nb)
        {
            return Mapper.Map<InvoiceDiscModel>(this.context.invoice_disc_t.Where(p => p.invoice_nb == InvoiceNb && p.invoice_rec_nb == InvoiceRecNb && p.invoice_disc_nb == InvoiceDiscNb));
        }
       	    public List<InvoiceDiscModel> GetByInvoiceNb(String invoiceNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceDiscModel> GetByInvoiceNbInvoiceRecNb(String invoiceNb, String invoiceRecNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceDiscModel model)
        {
            this.context.invoice_disc_t.Remove(Mapper.Map<invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceDiscModel model)
        {
            var record = this.context.invoice_disc_t.Where(p => p.invoice_nb == model.InvoiceNb && p.invoice_rec_nb == model.InvoiceRecNb && p.invoice_disc_nb == model.InvoiceDiscNb).FirstOrDefault();
            Mapper.Map<InvoiceDiscModel, invoice_disc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceDiscModel model)
        {
            this.context.invoice_disc_t.Add(Mapper.Map<invoice_disc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}