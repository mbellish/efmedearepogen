
// -----------------------------------------------------------------------
// <copyright file="VolumCommitRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Volum Commit repository.
    /// </summary>
    public class VolumCommitRepository : IVolumCommitRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="VolumCommitRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public VolumCommitRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<VolumCommitModel> GetAll()
        {
            return this.context.volum_commit_t.Select(Mapper.Map<VolumCommitModel>).ToList();
        }

		        /// <summary>
        /// Gets a VolumCommit by ContractNb StartDt StartSubCnt SubCd
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="start_dt">The start_dt of the entity.</param>/// <param name="start_sub_cnt">The start_sub_cnt of the entity.</param>/// <param name="sub_cd">The sub_cd of the entity.</param>        
        /// <returns>A Volum Commit.</returns>
        public VolumCommitModel GetByContractNbStartDtStartSubCntSubCd(String contract_nb,DateTime start_dt,Int32 start_sub_cnt,String sub_cd)
        {
            return Mapper.Map<VolumCommitModel>(this.context.volum_commit_t.Where(p => p.contract_nb == ContractNb && p.start_dt == StartDt && p.start_sub_cnt == StartSubCnt && p.sub_cd == SubCd));
        }
       	    public List<VolumCommitModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(VolumCommitModel model)
        {
            this.context.volum_commit_t.Remove(Mapper.Map<volum_commit_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(VolumCommitModel model)
        {
            var record = this.context.volum_commit_t.Where(p => p.contract_nb == model.ContractNb && p.start_dt == model.StartDt && p.start_sub_cnt == model.StartSubCnt && p.sub_cd == model.SubCd).FirstOrDefault();
            Mapper.Map<VolumCommitModel, volum_commit_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(VolumCommitModel model)
        {
            this.context.volum_commit_t.Add(Mapper.Map<volum_commit_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}