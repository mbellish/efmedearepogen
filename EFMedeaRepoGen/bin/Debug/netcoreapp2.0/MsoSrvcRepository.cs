
// -----------------------------------------------------------------------
// <copyright file="MsoSrvcRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Mso Srvc repository.
    /// </summary>
    public class MsoSrvcRepository : IMsoSrvcRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsoSrvcRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public MsoSrvcRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<MsoSrvcModel> GetAll()
        {
            return this.context.mso_srvc_t.Select(Mapper.Map<MsoSrvcModel>).ToList();
        }

		        /// <summary>
        /// Gets a MsoSrvc by ProgSrvcNm MsoNb
        /// </summary>
		/// <param name="prog_srvc_nm">The prog_srvc_nm of the entity.</param>/// <param name="mso_nb">The mso_nb of the entity.</param>        
        /// <returns>A Mso Srvc.</returns>
        public MsoSrvcModel GetByProgSrvcNmMsoNb(String prog_srvc_nm,String mso_nb)
        {
            return Mapper.Map<MsoSrvcModel>(this.context.mso_srvc_t.Where(p => p.prog_srvc_nm == ProgSrvcNm && p.mso_nb == MsoNb));
        }
       	    public List<MsoSrvcModel> GetByContract(String contractNb) {
			throw new NotImplementedException();
		}
		public List<MsoSrvcModel> GetByContractAndBillingParty(String contractNb, String billingNb) {
			throw new NotImplementedException();
		}
		public List<MsoSrvcModel> GetByMsoNb(String msoNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(MsoSrvcModel model)
        {
            this.context.mso_srvc_t.Remove(Mapper.Map<mso_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(MsoSrvcModel model)
        {
            var record = this.context.mso_srvc_t.Where(p => p.prog_srvc_nm == model.ProgSrvcNm && p.mso_nb == model.MsoNb).FirstOrDefault();
            Mapper.Map<MsoSrvcModel, mso_srvc_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(MsoSrvcModel model)
        {
            this.context.mso_srvc_t.Add(Mapper.Map<mso_srvc_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}