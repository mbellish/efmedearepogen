
// -----------------------------------------------------------------------
// <copyright file="JobInvoiceMappingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Job Invoice Mapping repository.
    /// </summary>
    public class JobInvoiceMappingRepository : IJobInvoiceMappingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobInvoiceMappingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public JobInvoiceMappingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<JobInvoiceMappingModel> GetAll()
        {
            return this.context.job_invoice_mapping_t.Select(Mapper.Map<JobInvoiceMappingModel>).ToList();
        }

			    public List<JobInvoiceMappingModel> GetByJobId(Int32 jobId) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(JobInvoiceMappingModel model)
        {
            this.context.job_invoice_mapping_t.Remove(Mapper.Map<job_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(JobInvoiceMappingModel model)
        {
            var record = this.context.job_invoice_mapping_t.Where(p => ).FirstOrDefault();
            Mapper.Map<JobInvoiceMappingModel, job_invoice_mapping_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(JobInvoiceMappingModel model)
        {
            this.context.job_invoice_mapping_t.Add(Mapper.Map<job_invoice_mapping_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}