
// -----------------------------------------------------------------------
// <copyright file="ArgoreportCustomGroupRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Argoreport Custom Group repository.
    /// </summary>
    public class ArgoreportCustomGroupRepository : IArgoreportCustomGroupRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgoreportCustomGroupRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ArgoreportCustomGroupRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ArgoreportCustomGroupModel> GetAll()
        {
            return this.context.argoreport_custom_group_t.Select(Mapper.Map<ArgoreportCustomGroupModel>).ToList();
        }

		        /// <summary>
        /// Gets a ArgoreportCustomGroup by ArgoreportCustomId GroupId
        /// </summary>
		/// <param name="argoreport_custom_id">The argoreport_custom_id of the entity.</param>/// <param name="group_id">The group_id of the entity.</param>        
        /// <returns>A Argoreport Custom Group.</returns>
        public ArgoreportCustomGroupModel GetByArgoreportCustomIdGroupId(String argoreport_custom_id,String group_id)
        {
            return Mapper.Map<ArgoreportCustomGroupModel>(this.context.argoreport_custom_group_t.Where(p => p.argoreport_custom_id == ArgoreportCustomId && p.group_id == GroupId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ArgoreportCustomGroupModel model)
        {
            this.context.argoreport_custom_group_t.Remove(Mapper.Map<argoreport_custom_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ArgoreportCustomGroupModel model)
        {
            var record = this.context.argoreport_custom_group_t.Where(p => p.argoreport_custom_id == model.ArgoreportCustomId && p.group_id == model.GroupId).FirstOrDefault();
            Mapper.Map<ArgoreportCustomGroupModel, argoreport_custom_group_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ArgoreportCustomGroupModel model)
        {
            this.context.argoreport_custom_group_t.Add(Mapper.Map<argoreport_custom_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}