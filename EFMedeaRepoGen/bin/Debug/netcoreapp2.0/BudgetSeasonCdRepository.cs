
// -----------------------------------------------------------------------
// <copyright file="BudgetSeasonCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Budget Season Cd repository.
    /// </summary>
    public class BudgetSeasonCdRepository : IBudgetSeasonCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BudgetSeasonCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BudgetSeasonCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BudgetSeasonCdModel> GetAll()
        {
            return this.context.budget_season_cd_t.Select(Mapper.Map<BudgetSeasonCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a BudgetSeasonCd by BudgetSeasonCd
        /// </summary>
		/// <param name="budget_season_cd">The budget_season_cd of the entity.</param>        
        /// <returns>A Budget Season Cd.</returns>
        public BudgetSeasonCdModel GetByBudgetSeasonCd(Int32 budget_season_cd)
        {
            return Mapper.Map<BudgetSeasonCdModel>(this.context.budget_season_cd_t.Where(p => p.budget_season_cd == BudgetSeasonCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BudgetSeasonCdModel model)
        {
            this.context.budget_season_cd_t.Remove(Mapper.Map<budget_season_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BudgetSeasonCdModel model)
        {
            var record = this.context.budget_season_cd_t.Where(p => p.budget_season_cd == model.BudgetSeasonCd).FirstOrDefault();
            Mapper.Map<BudgetSeasonCdModel, budget_season_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BudgetSeasonCdModel model)
        {
            this.context.budget_season_cd_t.Add(Mapper.Map<budget_season_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}