
// -----------------------------------------------------------------------
// <copyright file="InvoiceRecLogRevenueAccrueLatestRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice Rec Log Revenue Accrue Latest repository.
    /// </summary>
    public class InvoiceRecLogRevenueAccrueLatestRepository : IInvoiceRecLogRevenueAccrueLatestRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRecLogRevenueAccrueLatestRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRecLogRevenueAccrueLatestRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceRecLogRevenueAccrueLatestModel> GetAll()
        {
            return this.context.invoice_rec_log_revenue_accrue_latest_t.Select(Mapper.Map<InvoiceRecLogRevenueAccrueLatestModel>).ToList();
        }

		        /// <summary>
        /// Gets a InvoiceRecLogRevenueAccrueLatest by InvoiceRecLogId
        /// </summary>
		/// <param name="invoice_rec_log_id">The invoice_rec_log_id of the entity.</param>        
        /// <returns>A Invoice Rec Log Revenue Accrue Latest.</returns>
        public InvoiceRecLogRevenueAccrueLatestModel GetByInvoiceRecLogId(Int32 invoice_rec_log_id)
        {
            return Mapper.Map<InvoiceRecLogRevenueAccrueLatestModel>(this.context.invoice_rec_log_revenue_accrue_latest_t.Where(p => p.invoice_rec_log_id == InvoiceRecLogId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceRecLogRevenueAccrueLatestModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_latest_t.Remove(Mapper.Map<invoice_rec_log_revenue_accrue_latest_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceRecLogRevenueAccrueLatestModel model)
        {
            var record = this.context.invoice_rec_log_revenue_accrue_latest_t.Where(p => p.invoice_rec_log_id == model.InvoiceRecLogId).FirstOrDefault();
            Mapper.Map<InvoiceRecLogRevenueAccrueLatestModel, invoice_rec_log_revenue_accrue_latest_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceRecLogRevenueAccrueLatestModel model)
        {
            this.context.invoice_rec_log_revenue_accrue_latest_t.Add(Mapper.Map<invoice_rec_log_revenue_accrue_latest_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}