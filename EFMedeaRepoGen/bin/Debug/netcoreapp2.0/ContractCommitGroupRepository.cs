
// -----------------------------------------------------------------------
// <copyright file="ContractCommitGroupRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Contract Commit Group repository.
    /// </summary>
    public class ContractCommitGroupRepository : IContractCommitGroupRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractCommitGroupRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public ContractCommitGroupRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<ContractCommitGroupModel> GetAll()
        {
            return this.context.contract_commit_group_t.Select(Mapper.Map<ContractCommitGroupModel>).ToList();
        }

		        /// <summary>
        /// Gets a ContractCommitGroup by CommitGroupNb
        /// </summary>
		/// <param name="commit_group_nb">The commit_group_nb of the entity.</param>        
        /// <returns>A Contract Commit Group.</returns>
        public ContractCommitGroupModel GetByCommitGroupNb(String commit_group_nb)
        {
            return Mapper.Map<ContractCommitGroupModel>(this.context.contract_commit_group_t.Where(p => p.commit_group_nb == CommitGroupNb));
        }
       	    public List<ContractCommitGroupModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(ContractCommitGroupModel model)
        {
            this.context.contract_commit_group_t.Remove(Mapper.Map<contract_commit_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(ContractCommitGroupModel model)
        {
            var record = this.context.contract_commit_group_t.Where(p => p.commit_group_nb == model.CommitGroupNb).FirstOrDefault();
            Mapper.Map<ContractCommitGroupModel, contract_commit_group_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(ContractCommitGroupModel model)
        {
            this.context.contract_commit_group_t.Add(Mapper.Map<contract_commit_group_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}