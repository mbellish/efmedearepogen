
// -----------------------------------------------------------------------
// <copyright file="GroupAdjustRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Group Adjust repository.
    /// </summary>
    public class GroupAdjustRepository : IGroupAdjustRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupAdjustRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public GroupAdjustRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<GroupAdjustModel> GetAll()
        {
            return this.context.group_adjust_t.Select(Mapper.Map<GroupAdjustModel>).ToList();
        }

		        /// <summary>
        /// Gets a GroupAdjust by GroupId AdjustCd
        /// </summary>
		/// <param name="group_id">The group_id of the entity.</param>/// <param name="adjust_cd">The adjust_cd of the entity.</param>        
        /// <returns>A Group Adjust.</returns>
        public GroupAdjustModel GetByGroupIdAdjustCd(String group_id,String adjust_cd)
        {
            return Mapper.Map<GroupAdjustModel>(this.context.group_adjust_t.Where(p => p.group_id == GroupId && p.adjust_cd == AdjustCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(GroupAdjustModel model)
        {
            this.context.group_adjust_t.Remove(Mapper.Map<group_adjust_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(GroupAdjustModel model)
        {
            var record = this.context.group_adjust_t.Where(p => p.group_id == model.GroupId && p.adjust_cd == model.AdjustCd).FirstOrDefault();
            Mapper.Map<GroupAdjustModel, group_adjust_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(GroupAdjustModel model)
        {
            this.context.group_adjust_t.Add(Mapper.Map<group_adjust_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}