
// -----------------------------------------------------------------------
// <copyright file="OptionInvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Option Invoice repository.
    /// </summary>
    public class OptionInvoiceRepository : IOptionInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionInvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public OptionInvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<OptionInvoiceModel> GetAll()
        {
            return this.context.option_invoice_t.Select(Mapper.Map<OptionInvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a OptionInvoice by OptionInvoiceNb
        /// </summary>
		/// <param name="option_invoice_nb">The option_invoice_nb of the entity.</param>        
        /// <returns>A Option Invoice.</returns>
        public OptionInvoiceModel GetByOptionInvoiceNb(String option_invoice_nb)
        {
            return Mapper.Map<OptionInvoiceModel>(this.context.option_invoice_t.Where(p => p.option_invoice_nb == OptionInvoiceNb));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(OptionInvoiceModel model)
        {
            this.context.option_invoice_t.Remove(Mapper.Map<option_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(OptionInvoiceModel model)
        {
            var record = this.context.option_invoice_t.Where(p => p.option_invoice_nb == model.OptionInvoiceNb).FirstOrDefault();
            Mapper.Map<OptionInvoiceModel, option_invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(OptionInvoiceModel model)
        {
            this.context.option_invoice_t.Add(Mapper.Map<option_invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}