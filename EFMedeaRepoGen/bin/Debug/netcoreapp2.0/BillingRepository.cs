
// -----------------------------------------------------------------------
// <copyright file="BillingRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Billing repository.
    /// </summary>
    public class BillingRepository : IBillingRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillingRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public BillingRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<BillingModel> GetAll()
        {
            return this.context.billing_t.Select(Mapper.Map<BillingModel>).ToList();
        }

		        /// <summary>
        /// Gets a Billing by ContractNb BillingNb
        /// </summary>
		/// <param name="contract_nb">The contract_nb of the entity.</param>/// <param name="billing_nb">The billing_nb of the entity.</param>        
        /// <returns>A Billing.</returns>
        public BillingModel GetByContractNbBillingNb(String contract_nb,String billing_nb)
        {
            return Mapper.Map<BillingModel>(this.context.billing_t.Where(p => p.contract_nb == ContractNb && p.billing_nb == BillingNb));
        }
       	    public List<BillingModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(BillingModel model)
        {
            this.context.billing_t.Remove(Mapper.Map<billing_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(BillingModel model)
        {
            var record = this.context.billing_t.Where(p => p.contract_nb == model.ContractNb && p.billing_nb == model.BillingNb).FirstOrDefault();
            Mapper.Map<BillingModel, billing_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(BillingModel model)
        {
            this.context.billing_t.Add(Mapper.Map<billing_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}