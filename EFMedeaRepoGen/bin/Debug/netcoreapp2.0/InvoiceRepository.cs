
// -----------------------------------------------------------------------
// <copyright file="InvoiceRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Invoice repository.
    /// </summary>
    public class InvoiceRepository : IInvoiceRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public InvoiceRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<InvoiceModel> GetAll()
        {
            return this.context.invoice_t.Select(Mapper.Map<InvoiceModel>).ToList();
        }

		        /// <summary>
        /// Gets a Invoice by InvoiceNb
        /// </summary>
		/// <param name="invoice_nb">The invoice_nb of the entity.</param>        
        /// <returns>A Invoice.</returns>
        public InvoiceModel GetByInvoiceNb(String invoice_nb)
        {
            return Mapper.Map<InvoiceModel>(this.context.invoice_t.Where(p => p.invoice_nb == InvoiceNb));
        }
       	    public List<InvoiceModel> GetByContractNb(String contractNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetByContractNbBillingNb(String contractNb, String billingNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetByEndDt(DateTime endDt) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetByContractNbTransCdManualInvFlAccInvFl(String contractNb, String transCd, String manualInvFl, String accInvFl) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetByOriginatingInvoiceNb(String originatingInvoiceNb) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetInvoicesByContractNbMonthYear(String contractNb, Int32 month, Int32 year) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetInvoicesByMonthYearRange(Int32 startMonth, Int32 startYear, Int32 endMonth, Int32 endYear) {
			throw new NotImplementedException();
		}
		public List<InvoiceModel> GetInvoicesAndAccruals() {
			throw new NotImplementedException();
		}
		public Void DeleteByInvoiceNb(InvoiceModel entity) {
			throw new NotImplementedException();
		}
		        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(InvoiceModel model)
        {
            this.context.invoice_t.Remove(Mapper.Map<invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(InvoiceModel model)
        {
            var record = this.context.invoice_t.Where(p => p.invoice_nb == model.InvoiceNb).FirstOrDefault();
            Mapper.Map<InvoiceModel, invoice_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(InvoiceModel model)
        {
            this.context.invoice_t.Add(Mapper.Map<invoice_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}