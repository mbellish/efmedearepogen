
// -----------------------------------------------------------------------
// <copyright file="AffilCdRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Affil Cd repository.
    /// </summary>
    public class AffilCdRepository : IAffilCdRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AffilCdRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public AffilCdRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<AffilCdModel> GetAll()
        {
            return this.context.affil_cd_t.Select(Mapper.Map<AffilCdModel>).ToList();
        }

		        /// <summary>
        /// Gets a AffilCd by AffilCd
        /// </summary>
		/// <param name="affil_cd">The affil_cd of the entity.</param>        
        /// <returns>A Affil Cd.</returns>
        public AffilCdModel GetByAffilCd(String affil_cd)
        {
            return Mapper.Map<AffilCdModel>(this.context.affil_cd_t.Where(p => p.affil_cd == AffilCd));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(AffilCdModel model)
        {
            this.context.affil_cd_t.Remove(Mapper.Map<affil_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(AffilCdModel model)
        {
            var record = this.context.affil_cd_t.Where(p => p.affil_cd == model.AffilCd).FirstOrDefault();
            Mapper.Map<AffilCdModel, affil_cd_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(AffilCdModel model)
        {
            this.context.affil_cd_t.Add(Mapper.Map<affil_cd_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}