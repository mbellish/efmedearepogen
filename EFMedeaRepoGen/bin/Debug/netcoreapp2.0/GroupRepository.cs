
// -----------------------------------------------------------------------
// <copyright file="GroupRepository.cs" company="Operative">
// Copyright (c) Operative. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Operative.Medea.Core.Repository.EF
{
	using System;
	using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Operative.Medea.Core.Model;
    using Operative.Medea.Core.Repository.Abstract;

    /// <summary>
    /// The Group repository.
    /// </summary>
    public class GroupRepository : IGroupRepository    {
        /// <summary>
        /// The adjustService.
        /// </summary>
        private readonly MedeaDataModel context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupRepository"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        public GroupRepository(MedeaDataModel context)
        {
            this.context = context;
        }

		
        /// <summary>
        /// Gets a list of all adjust
        /// </summary>
        /// <returns>A list of all adjust</returns>
        public List<GroupModel> GetAll()
        {
            return this.context.group_t.Select(Mapper.Map<GroupModel>).ToList();
        }

		        /// <summary>
        /// Gets a Group by GroupId
        /// </summary>
		/// <param name="group_id">The group_id of the entity.</param>        
        /// <returns>A Group.</returns>
        public GroupModel GetByGroupId(String group_id)
        {
            return Mapper.Map<GroupModel>(this.context.group_t.Where(p => p.group_id == GroupId));
        }
       	            /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="model">The Adjust code to delete</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Delete(GroupModel model)
        {
            this.context.group_t.Remove(Mapper.Map<group_t>(model));
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">The Adjust code to update</param>
        /// <returns>
        /// The model<see cref="bool"/>.
        /// </returns>
        public void Update(GroupModel model)
        {
            var record = this.context.group_t.Where(p => p.group_id == model.GroupId).FirstOrDefault();
            Mapper.Map<GroupModel, group_t>(model, record);
            this.context.SaveChanges();
            // return true;
        }

        /// <summary>
        /// Inserts a Adjust Code into the database.
        /// </summary>
        /// <param name="model">The Adjust Code to insert.</param>
        /// <returns>
        /// The model <see cref="bool"/>.
        /// </returns>
        public void Insert(GroupModel model)
        {
            this.context.group_t.Add(Mapper.Map<group_t>(model));
            this.context.SaveChanges();
            // return true;
        }
	}
}