﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace EFMedeaRepoGen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Gin the code!");
            foreach (string f in Directory.EnumerateFiles(".", "*.cs"))
            {
                File.Delete(f);
            }
            var interfaces = System.Reflection.Assembly.LoadFile(@"D:\devel\Sintec.Medea.Core\Sintec.Medea.Core\Operative.Medea.Core.Repository.EF\bin\Debug\Operative.Medea.Core.Repository.Abstract.dll");
            foreach (var type in System.Reflection.Assembly.LoadFile(@"D:\devel\Sintec.Medea.Core\Sintec.Medea.Core\Operative.Medea.Core.Repository.EF\bin\Debug\Operative.Medea.Core.Repository.EF.dll").GetTypes().Where(p => p.Name.EndsWith("_t")))
            {
                string interfaceName = "Operative.Medea.Core.Repository.Abstract.I" + RepoTemplate.PascalCase(type.Name) + "Repository";
                var itype = interfaces.GetType(interfaceName);
                if (itype == null)
                    continue;
                RepoTemplate template = new RepoTemplate(type, itype);
                var context = template.TransformText();
                System.IO.File.WriteAllText(RepoTemplate.PascalCase(type.Name) + "Repository.cs", context);
            }
        }
    }
}
